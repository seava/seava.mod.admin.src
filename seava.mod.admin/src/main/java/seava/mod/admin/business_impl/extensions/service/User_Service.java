/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.business_impl.extensions.service;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.springframework.transaction.annotation.Transactional;

import seava.lib.j4e.api.Constants;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.web.security.IPasswordValidator;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.mod.admin.business_api.generated.spi.IUserService;
import seava.mod.admin.domain.generated.model.User;

/**
 * Service class implementation for {@link User} domain entity. <br>
 * Contains repository functionality with finder methods as well as specific
 * business functionality
 */
public class User_Service extends
		seava.mod.admin.business_impl.generated.service.User_Service implements
		IUserService {

	@Override
	protected void preInsert(User e) throws ManagedException {
		if (e.getPassword() == null || "".equals(e.getPassword())) {
			e.setPassword(this.encryptPassword(e.getLoginName()));
		}
	}

	/**
	 * 
	 */
	@Override
	@Transactional
	public void doChangePassword(String userId, String newPassword)
			throws ManagedException {

		User u = this.findById(userId);
		if (!u.getClientId().equals(Session.user.get().getClientId())) {
			throw new ManagedException(ErrorCodeCommons.CLIENT_MISMATCH,
					"Acces to a different client is not allowed!");
		}
		try {
			this.getApplicationContext().getBean(IPasswordValidator.class)
					.validate(newPassword);
		} catch (Exception e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"Pasword validation failed.", e);
		}
		u.setPassword(this.encryptPassword(newPassword));
		this.getEntityManager().merge(u);

	}

	/**
	 * 
	 * @param password
	 * @return
	 * @throws ManagedException
	 */
	private String encryptPassword(String password) throws ManagedException {
		MessageDigest messageDigest;
		try {
			messageDigest = MessageDigest
					.getInstance(Constants.DEFAULT_ENCRYPTION_ALGORITHM);
		} catch (NoSuchAlgorithmException e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					"No `MD5` algorithm found.", e);
		}
		messageDigest.update(password.getBytes(), 0, password.length());
		String hashedPass = new BigInteger(1, messageDigest.digest())
				.toString(16);
		if (hashedPass.length() < 32) {
			hashedPass = "0" + hashedPass;
		}
		return hashedPass;
	}

}
