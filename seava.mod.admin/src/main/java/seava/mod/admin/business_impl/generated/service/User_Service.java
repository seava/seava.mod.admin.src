/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.business_impl.generated.service;

import javax.persistence.EntityManager;
import seava.lib.j4e.business.service.entity.AbstractEntityService;
import seava.mod.admin.domain.generated.model.User;

/**
 * Service class implementation for {@link User} domain
 * entity. <br>
 * Contains repository functionality with finder methods as well as specific business functionality
 */
public class User_Service extends AbstractEntityService<User> {
  
  public User_Service() {
    super();
  }
  
  public User_Service(EntityManager em) {
    super();
    this.setEntityManager(em);
  }
  
  @Override
  public Class<User> getEntityClass() {
    return User.class;
  }
}
