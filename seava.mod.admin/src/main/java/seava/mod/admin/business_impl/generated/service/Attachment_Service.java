/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.business_impl.generated.service;

import javax.persistence.EntityManager;
import seava.lib.j4e.business.service.entity.AbstractEntityService;
import seava.mod.admin.domain.generated.model.Attachment;

/**
 * Service class implementation for {@link Attachment} domain
 * entity. <br>
 * Contains repository functionality with finder methods as well as specific business functionality
 */
public class Attachment_Service extends AbstractEntityService<Attachment> {
  
  public Attachment_Service() {
    super();
  }
  
  public Attachment_Service(EntityManager em) {
    super();
    this.setEntityManager(em);
  }
  
  @Override
  public Class<Attachment> getEntityClass() {
    return Attachment.class;
  }
}
