package seava.mod.admin.business_impl.extensions.delegate;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collection;

import seava.lib.j4e.api.Constants;
import seava.lib.j4e.api.base.descriptor.IImportDataPackage;
import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.IClient;
import seava.lib.j4e.api.base.session.IUser;
import seava.lib.j4e.api.base.session.IWorkspace;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.business.service.IImportDataPackageService;
import seava.lib.j4e.api.business.service.IInitializeClientService;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.base.session.AppClient;
import seava.lib.j4e.base.session.AppUser;
import seava.lib.j4e.base.session.AppWorkspace;
import seava.lib.j4e.business.service.AbstractBusinessDelegate;
import seava.mod.admin.domain.generated.model.Role;
import seava.mod.admin.domain.generated.model.User;

public class InitializeClient_Bd extends AbstractBusinessDelegate implements
		IInitializeClientService {

	@Override
	public void execute(String clientId, String clientCode, String userCode,
			String userName, String loginName, String password,
			IImportDataPackage dataPackage) throws ManagedException {

		IUser su = Session.user.get();
		IClient c = new AppClient(clientId, clientCode, clientCode);
		IWorkspace ws = new AppWorkspace("");
		IUser newUser = new AppUser(su.getCode(), su.getName(),
				su.getLoginName(), "", c, su.getSettings(), su.getProfile(),
				ws, false);

		try {
			Session.user.set(newUser);

			Role radmin = new Role();
			radmin.setCode(Constants.ROLE_ADMIN_CODE);
			radmin.setName(Constants.ROLE_ADMIN_NAME);
			radmin.setDescription(Constants.ROLE_ADMIN_DESC);
			radmin.setActive(true);
			this.getEntityManager().persist(radmin);

			Role ruser = new Role();
			ruser.setCode(Constants.ROLE_USER_CODE);
			ruser.setName(Constants.ROLE_USER_NAME);
			ruser.setDescription(Constants.ROLE_USER_DESC);
			ruser.setActive(true);
			this.getEntityManager().persist(ruser);

			Collection<Role> roles = new ArrayList<Role>();
			roles.add(radmin);
			roles.add(ruser);

			User u = new User();

			u.setCode(userCode);
			u.setName(userName);
			u.setLoginName(loginName);
			u.setLocked(false);
			u.setActive(true);
			MessageDigest messageDigest;

			try {
				messageDigest = MessageDigest
						.getInstance(Constants.DEFAULT_ENCRYPTION_ALGORITHM);
			} catch (NoSuchAlgorithmException e) {
				throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
						"No MD5 algrorithm available", e);
			}
			messageDigest.update(password.getBytes(), 0, password.length());
			String hashedPass = new BigInteger(1, messageDigest.digest())
					.toString(16);
			if (hashedPass.length() < 32) {
				hashedPass = "0" + hashedPass;
			}

			u.setPassword(hashedPass);
			u.setRoles(roles);
			this.getEntityManager().persist(u);

			if (dataPackage != null) {
				this.getApplicationContext()
						.getBean(IImportDataPackageService.class)
						.doExecute(dataPackage);
			}
		} catch (Exception e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage(), e);
		} finally {
			Session.user.set(su);
		}

	}

}
