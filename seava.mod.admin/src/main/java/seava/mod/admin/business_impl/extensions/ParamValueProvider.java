package seava.mod.admin.business_impl.extensions;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.presenter.service.ISysParamValueProvider;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.business.service.AbstractBusinessBaseService;

import seava.mod.admin.domain.generated.model.ParamValue;

public class ParamValueProvider extends AbstractBusinessBaseService implements
		ISysParamValueProvider {

	/**
	 * Return parameter vales for current client valid at the specified date.
	 */
	@Override
	public Map<String, String> getParamValues(Date validAt)
			throws ManagedException {

		try {
			DataSource dataSource = this.getApplicationContext().getBean(
					DataSource.class);
			JdbcTemplate tpl = new JdbcTemplate(dataSource);
			tpl.queryForList("select 1 from " + ParamValue.TABLE_NAME
					+ "  where 1=0");

		} catch (Exception e) {
			throw new ManagedException(ErrorCodeCommons.RUNTIME_ERROR,
					e.getMessage());
		}

		Date _validAt = validAt;
		if (_validAt == null) {
			_validAt = new Date();
		}

		List<ParamValue> values = this
				.getEntityManager()
				.createQuery(
						"select e from "
								+ ParamValue.ALIAS
								+ " e where :validAt between e.validFrom and e.validTo order by e.validFrom",
						ParamValue.class).setParameter("validAt", _validAt)
				.getResultList();
		Map<String, String> result = new HashMap<String, String>();
		for (ParamValue v : values) {
			result.put(v.getSysParam(), v.getValue());
		}
		return result;
	}
}
