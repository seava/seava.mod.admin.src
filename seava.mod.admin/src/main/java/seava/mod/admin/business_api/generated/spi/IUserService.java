/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.business_api.generated.spi;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.mod.admin.domain.generated.model.User;

/**
 * Interface to expose business functions specific for {@link User} domain
 * entity.
 */
public interface IUserService extends IEntityService<User> {
  
  public void doChangePassword(String userId, String newPassword)
    throws ManagedException;
}
