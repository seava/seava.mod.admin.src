/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import seava.mod.abstracts.domain.generated.model.AbstractAuditable;

@Entity(name=ParamValue.ALIAS)
@Table(name=ParamValue.TABLE_NAME)
public class ParamValue extends AbstractAuditable {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_ParamValue";
  
  public static final String TABLE_NAME = "AD_PARAMVAL";
      
  
  @Column(name="SYSPARAM", nullable=false)
  private String sysParam;
  
  @Column(name="VALUE")
  private String value;
  
  @Column(name="VALIDFROM", nullable=false)
  @Temporal(TemporalType.DATE)
  private Date validFrom;
  
  @Column(name="VALIDTO", nullable=false)
  @Temporal(TemporalType.DATE)
  private Date validTo;
  
  public String getSysParam() {
    return this.sysParam;
  }
  
  public void setSysParam(String sysParam) {
    this.sysParam = sysParam;
  }
  
  public String getValue() {
    return this.value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
  
  public Date getValidFrom() {
    return this.validFrom;
  }
  
  public void setValidFrom(Date validFrom) {
    this.validFrom = validFrom;
  }
  
  public Date getValidTo() {
    return this.validTo;
  }
  
  public void setValidTo(Date validTo) {
    this.validTo = validTo;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
    if (this.validFrom == null) {
      this.validFrom = new Date();
    }
    if (this.validTo == null) {
      this.validTo = new Date();
    }
  }
}
