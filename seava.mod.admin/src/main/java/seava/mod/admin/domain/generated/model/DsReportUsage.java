/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import seava.mod.abstracts.domain.generated.model.AbstractAuditable;
import seava.mod.admin.domain.generated.model.DsReport;

@Entity(name=DsReportUsage.ALIAS)
@Table(name=DsReportUsage.TABLE_NAME)
public class DsReportUsage extends AbstractAuditable {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_DsReportUsage";
  
  public static final String TABLE_NAME = "AD_RPT_DS_USAGE";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=DsReport.class)
  @JoinColumn(name="DSREPORT_ID", referencedColumnName="ID")
  private DsReport dsReport;
  
  @Column(name="FRAMENAME")
  private String frameName;
  
  @Column(name="TOOLBARKEY")
  private String toolbarKey;
  
  @Column(name="DCKEY")
  private String dcKey;
  
  @Column(name="SEQUENCENO")
  private Integer sequenceNo;
  
  public DsReport getDsReport() {
    return this.dsReport;
  }
  
  public void setDsReport(DsReport dsReport) {
    this.dsReport = dsReport;
  }
  
  public String getFrameName() {
    return this.frameName;
  }
  
  public void setFrameName(String frameName) {
    this.frameName = frameName;
  }
  
  public String getToolbarKey() {
    return this.toolbarKey;
  }
  
  public void setToolbarKey(String toolbarKey) {
    this.toolbarKey = toolbarKey;
  }
  
  public String getDcKey() {
    return this.dcKey;
  }
  
  public void setDcKey(String dcKey) {
    this.dcKey = dcKey;
  }
  
  public Integer getSequenceNo() {
    return this.sequenceNo;
  }
  
  public void setSequenceNo(Integer sequenceNo) {
    this.sequenceNo = sequenceNo;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
