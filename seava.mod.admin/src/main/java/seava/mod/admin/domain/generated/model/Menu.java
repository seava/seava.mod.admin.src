/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractType;
import seava.mod.admin.domain.generated.model.Role;

@Entity(name=Menu.ALIAS)
@Table(name=Menu.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=Menu.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+Menu.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class Menu extends AbstractType {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_Menu";
  
  public static final String TABLE_NAME = "AD_MENU";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "Menu.findByName";
      
  
  @Column(name="SEQUENCENO", nullable=false)
  private Integer sequenceNo;
  
  @Column(name="TITLE", nullable=false)
  private String title;
  
  @Column(name="TAG")
  private String tag;
  
  @ManyToMany(mappedBy="menus")
  private Collection<Role> roles;
  
  public Integer getSequenceNo() {
    return this.sequenceNo;
  }
  
  public void setSequenceNo(Integer sequenceNo) {
    this.sequenceNo = sequenceNo;
  }
  
  public String getTitle() {
    return this.title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getTag() {
    return this.tag;
  }
  
  public void setTag(String tag) {
    this.tag = tag;
  }
  
  public Collection<Role> getRoles() {
    return this.roles;
  }
  
  public void setRoles(Collection<Role> roles) {
    this.roles = roles;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
    if (this.sequenceNo == null) {
      this.sequenceNo = 0;
    }
  }
}
