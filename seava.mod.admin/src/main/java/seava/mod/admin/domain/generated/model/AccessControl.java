/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import java.util.ArrayList;
import java.util.Collection;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.annotations.CascadeOnDelete;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractType;
import seava.mod.admin.domain.generated.model.AccessControlDs;
import seava.mod.admin.domain.generated.model.Role;

@Entity(name=AccessControl.ALIAS)
@Table(name=AccessControl.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=AccessControl.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+AccessControl.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class AccessControl extends AbstractType {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_AccessControl";
  
  public static final String TABLE_NAME = "AD_ACL";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "AccessControl.findByName";
      
  
  @OneToMany(fetch=FetchType.LAZY, targetEntity=AccessControlDs.class, mappedBy="accessControl"
  ,cascade=CascadeType.ALL)
  @CascadeOnDelete
  private Collection<AccessControlDs> dsRules;
  
  @ManyToMany(mappedBy="accessControls")
  private Collection<Role> roles;
  
  public Collection<AccessControlDs> getDsRules() {
    return this.dsRules;
  }
  
  public void setDsRules(Collection<AccessControlDs> dsRules) {
    this.dsRules = dsRules;
  }
  
  public void addToDsRules(AccessControlDs e) {
    if (this.dsRules == null) {
      this.dsRules = new ArrayList<AccessControlDs>();
    }
    e.setAccessControl(this);
    this.dsRules.add(e);
  }
  
  public Collection<Role> getRoles() {
    return this.roles;
  }
  
  public void setRoles(Collection<Role> roles) {
    this.roles = roles;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
