/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeWithCode;
import seava.mod.admin.domain.generated.model.ReportServer;

@Entity(name=Report.ALIAS)
@Table(name=Report.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=Report.NQ_FIND_BY_CODE,
    query="SELECT e FROM "+Report.ALIAS+" e WHERE e.clientId = :clientId and e.code = :code",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  ),
  @NamedQuery(
    name=Report.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+Report.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class Report extends AbstractTypeWithCode {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_Report";
  
  public static final String TABLE_NAME = "AD_RPT";
  /**
   * Named query find by unique key: Code.
   */
  public static final String NQ_FIND_BY_CODE = "Report.findByCode";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "Report.findByName";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=ReportServer.class)
  @JoinColumn(name="REPORTSERVER_ID", referencedColumnName="ID")
  private ReportServer reportServer;
  
  @Column(name="CONTEXTPATH")
  private String contextPath;
  
  public ReportServer getReportServer() {
    return this.reportServer;
  }
  
  public void setReportServer(ReportServer reportServer) {
    this.reportServer = reportServer;
  }
  
  public String getContextPath() {
    return this.contextPath;
  }
  
  public void setContextPath(String contextPath) {
    this.contextPath = contextPath;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
