/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import seava.mod.abstracts.domain.generated.model.AbstractAuditable;

@Entity(name=TargetRule.ALIAS)
@Table(name=TargetRule.TABLE_NAME)
public class TargetRule extends AbstractAuditable {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_TargetRule";
  
  public static final String TABLE_NAME = "AD_TARGET_RULE";
      
  
  @Column(name="SOURCEREFID", nullable=false)
  private String sourceRefId;
  
  @Column(name="TARGETALIAS", nullable=false)
  private String targetAlias;
  
  @Column(name="TARGETTYPE", nullable=false)
  private String targetType;
  
  public String getSourceRefId() {
    return this.sourceRefId;
  }
  
  public void setSourceRefId(String sourceRefId) {
    this.sourceRefId = sourceRefId;
  }
  
  public String getTargetAlias() {
    return this.targetAlias;
  }
  
  public void setTargetAlias(String targetAlias) {
    this.targetAlias = targetAlias;
  }
  
  public String getTargetType() {
    return this.targetType;
  }
  
  public void setTargetType(String targetType) {
    this.targetType = targetType;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
