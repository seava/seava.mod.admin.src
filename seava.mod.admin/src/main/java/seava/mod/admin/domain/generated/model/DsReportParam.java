/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import seava.mod.abstracts.domain.generated.model.AbstractAuditable;
import seava.mod.admin.domain.generated.model.DsReport;
import seava.mod.admin.domain.generated.model.ReportParam;

@Entity(name=DsReportParam.ALIAS)
@Table(name=DsReportParam.TABLE_NAME)
public class DsReportParam extends AbstractAuditable {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_DsReportParam";
  
  public static final String TABLE_NAME = "AD_RPT_DS_PARAM";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=DsReport.class)
  @JoinColumn(name="DSREPORT_ID", referencedColumnName="ID")
  private DsReport dsReport;
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=ReportParam.class)
  @JoinColumn(name="REPORTPARAM_ID", referencedColumnName="ID")
  private ReportParam reportParam;
  
  @Column(name="DSFIELD")
  private String dsField;
  
  @Column(name="STATICVALUE")
  private String staticValue;
  
  public DsReport getDsReport() {
    return this.dsReport;
  }
  
  public void setDsReport(DsReport dsReport) {
    this.dsReport = dsReport;
  }
  
  public ReportParam getReportParam() {
    return this.reportParam;
  }
  
  public void setReportParam(ReportParam reportParam) {
    this.reportParam = reportParam;
  }
  
  public String getDsField() {
    return this.dsField;
  }
  
  public void setDsField(String dsField) {
    this.dsField = dsField;
  }
  
  public String getStaticValue() {
    return this.staticValue;
  }
  
  public void setStaticValue(String staticValue) {
    this.staticValue = staticValue;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
