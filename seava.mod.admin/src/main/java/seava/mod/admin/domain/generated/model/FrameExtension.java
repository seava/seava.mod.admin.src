/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractAuditable;

@Entity(name=FrameExtension.ALIAS)
@Table(name=FrameExtension.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=FrameExtension.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+FrameExtension.ALIAS+" e WHERE e.clientId = :clientId and e.frame = :frame and e.fileLocation = :fileLocation",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class FrameExtension extends AbstractAuditable {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_FrameExtension";
  
  public static final String TABLE_NAME = "AD_FRAME_EXT";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "FrameExtension.findByName";
      
  
  @Column(name="FRAME", nullable=false)
  private String frame;
  
  @Column(name="SEQUENCENO", nullable=false)
  private Integer sequenceNo;
  
  @Column(name="FILELOCATION", nullable=false)
  private String fileLocation;
  
  @Column(name="RELATIVEPATH", nullable=false)
  private Boolean relativePath;
  
  @Column(name="ACTIVE", nullable=false)
  private Boolean active;
  
  public String getFrame() {
    return this.frame;
  }
  
  public void setFrame(String frame) {
    this.frame = frame;
  }
  
  public Integer getSequenceNo() {
    return this.sequenceNo;
  }
  
  public void setSequenceNo(Integer sequenceNo) {
    this.sequenceNo = sequenceNo;
  }
  
  public String getFileLocation() {
    return this.fileLocation;
  }
  
  public void setFileLocation(String fileLocation) {
    this.fileLocation = fileLocation;
  }
  
  public Boolean getRelativePath() {
    return this.relativePath;
  }
  
  public void setRelativePath(Boolean relativePath) {
    this.relativePath = relativePath;
  }
  
  public Boolean getActive() {
    return this.active;
  }
  
  public void setActive(Boolean active) {
    this.active = active;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
    if (this.sequenceNo == null) {
      this.sequenceNo = 0;
    }
    if (this.relativePath == null) {
      this.relativePath = false;
    }
    if (this.active == null) {
      this.active = false;
    }
  }
}
