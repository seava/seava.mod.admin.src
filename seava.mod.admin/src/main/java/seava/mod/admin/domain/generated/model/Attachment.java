/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import seava.mod.abstracts.domain.generated.model.AbstractAuditable;
import seava.mod.admin.domain.generated.model.AttachmentType;

@Entity(name=Attachment.ALIAS)
@Table(name=Attachment.TABLE_NAME)
public class Attachment extends AbstractAuditable {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_Attachment";
  
  public static final String TABLE_NAME = "AD_ATCH";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=AttachmentType.class)
  @JoinColumn(name="TYPE_ID", referencedColumnName="ID")
  private AttachmentType type;
  
  @Column(name="NAME", nullable=false)
  private String name;
  
  @Column(name="FILENAME")
  private String fileName;
  
  @Column(name="LOCATION")
  private String location;
  
  @Column(name="CONTENTTYPE")
  private String contentType;
  
  @Transient
  private String url;
  
  @Column(name="TARGETREFID", nullable=false)
  private String targetRefid;
  
  @Column(name="TARGETALIAS", nullable=false)
  private String targetAlias;
  
  @Column(name="TARGETTYPE")
  private String targetType;
  
  public AttachmentType getType() {
    return this.type;
  }
  
  public void setType(AttachmentType type) {
    this.type = type;
  }
  
  public String getName() {
    return this.name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getFileName() {
    return this.fileName;
  }
  
  public void setFileName(String fileName) {
    this.fileName = fileName;
  }
  
  public String getLocation() {
    return this.location;
  }
  
  public void setLocation(String location) {
    this.location = location;
  }
  
  public String getContentType() {
    return this.contentType;
  }
  
  public void setContentType(String contentType) {
    this.contentType = contentType;
  }
  
  @Transient
  public String getUrl() {
    
        if( this.location != null && !"".equals(this.location) ) {
            if( this.location.startsWith("http")) {
                return this.location;
              } else {
                return this.type.getBaseUrl() + "/" + this.location;
              }
          } else {
            return this.type.getBaseUrl() + "/" + this.getId() + "." + this.contentType;
          }
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  public String getTargetRefid() {
    return this.targetRefid;
  }
  
  public void setTargetRefid(String targetRefid) {
    this.targetRefid = targetRefid;
  }
  
  public String getTargetAlias() {
    return this.targetAlias;
  }
  
  public void setTargetAlias(String targetAlias) {
    this.targetAlias = targetAlias;
  }
  
  public String getTargetType() {
    return this.targetType;
  }
  
  public void setTargetType(String targetType) {
    this.targetType = targetType;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
