/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractType;

@Entity(name=ReportServer.ALIAS)
@Table(name=ReportServer.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=ReportServer.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+ReportServer.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class ReportServer extends AbstractType {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_ReportServer";
  
  public static final String TABLE_NAME = "AD_RPT_SRV";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "ReportServer.findByName";
      
  
  @Column(name="URL")
  private String url;
  
  @Column(name="QUERY_BLD_CLASS")
  private String queryBuilderClass;
  
  public String getUrl() {
    return this.url;
  }
  
  public void setUrl(String url) {
    this.url = url;
  }
  
  public String getQueryBuilderClass() {
    return this.queryBuilderClass;
  }
  
  public void setQueryBuilderClass(String queryBuilderClass) {
    this.queryBuilderClass = queryBuilderClass;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
