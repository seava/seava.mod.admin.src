/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractType;
import seava.mod.admin.domain.generated.model.Report;

@Entity(name=ReportParam.ALIAS)
@Table(name=ReportParam.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=ReportParam.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+ReportParam.ALIAS+" e WHERE e.clientId = :clientId and e.report = :report and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
  ,@NamedQuery(name=ReportParam.NQ_FIND_BY_NAME_PRIMITIVE, query="SELECT e FROM "+ReportParam.ALIAS+" e WHERE e.clientId = :clientId and e.report.id = :reportId and e.name = :name", hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE))
})
public class ReportParam extends AbstractType {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_ReportParam";
  
  public static final String TABLE_NAME = "AD_RPT_PRM";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "ReportParam.findByName";
  /**
   * Named query find by unique key: Name using the ID field for references.
   */
  public static final String NQ_FIND_BY_NAME_PRIMITIVE = "ReportParam.findByName_PRIMITIVE";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=Report.class)
  @JoinColumn(name="REPORT_ID", referencedColumnName="ID")
  private Report report;
  
  @Column(name="TITLE", nullable=false)
  private String title;
  
  @Column(name="SEQUENCENO", nullable=false)
  private Integer sequenceNo;
  
  @Column(name="MANDATORY", nullable=false)
  private Boolean mandatory;
  
  @Column(name="NOEDIT", nullable=false)
  private Boolean noEdit;
  
  @Column(name="DATATYPE", nullable=false)
  private String dataType;
  
  @Column(name="DEFAULTVALUE")
  private String defaultValue;
  
  @Column(name="LISTOFVALUES")
  private String listOfValues;
  
  public Report getReport() {
    return this.report;
  }
  
  public void setReport(Report report) {
    this.report = report;
  }
  
  public String getTitle() {
    return this.title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public Integer getSequenceNo() {
    return this.sequenceNo;
  }
  
  public void setSequenceNo(Integer sequenceNo) {
    this.sequenceNo = sequenceNo;
  }
  
  public Boolean getMandatory() {
    return this.mandatory;
  }
  
  public void setMandatory(Boolean mandatory) {
    this.mandatory = mandatory;
  }
  
  public Boolean getNoEdit() {
    return this.noEdit;
  }
  
  public void setNoEdit(Boolean noEdit) {
    this.noEdit = noEdit;
  }
  
  public String getDataType() {
    return this.dataType;
  }
  
  public void setDataType(String dataType) {
    this.dataType = dataType;
  }
  
  public String getDefaultValue() {
    return this.defaultValue;
  }
  
  public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
  }
  
  public String getListOfValues() {
    return this.listOfValues;
  }
  
  public void setListOfValues(String listOfValues) {
    this.listOfValues = listOfValues;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
    if (this.sequenceNo == null) {
      this.sequenceNo = 0;
    }
    if (this.mandatory == null) {
      this.mandatory = false;
    }
    if (this.noEdit == null) {
      this.noEdit = false;
    }
  }
}
