/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractType;

@Entity(name=AttachmentType.ALIAS)
@Table(name=AttachmentType.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=AttachmentType.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+AttachmentType.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class AttachmentType extends AbstractType {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_AttachmentType";
  
  public static final String TABLE_NAME = "AD_ATCH_TYPE";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "AttachmentType.findByName";
      
  
  @Column(name="CATEGORY", nullable=false)
  private String category;
  
  @Column(name="UPLOADPATH")
  private String uploadPath;
  
  @Column(name="BASEURL", nullable=false)
  private String baseUrl;
  
  public String getCategory() {
    return this.category;
  }
  
  public void setCategory(String category) {
    this.category = category;
  }
  
  public String getUploadPath() {
    return this.uploadPath;
  }
  
  public void setUploadPath(String uploadPath) {
    this.uploadPath = uploadPath;
  }
  
  public String getBaseUrl() {
    return this.baseUrl;
  }
  
  public void setBaseUrl(String baseUrl) {
    this.baseUrl = baseUrl;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
