/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeWithCode;
import seava.mod.admin.domain.generated.model.AccessControl;
import seava.mod.admin.domain.generated.model.Menu;
import seava.mod.admin.domain.generated.model.MenuItem;
import seava.mod.admin.domain.generated.model.User;

@Entity(name=Role.ALIAS)
@Table(name=Role.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=Role.NQ_FIND_BY_CODE,
    query="SELECT e FROM "+Role.ALIAS+" e WHERE e.clientId = :clientId and e.code = :code",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  ),
  @NamedQuery(
    name=Role.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+Role.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class Role extends AbstractTypeWithCode {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_Role";
  
  public static final String TABLE_NAME = "AD_ROLE";
  /**
   * Named query find by unique key: Code.
   */
  public static final String NQ_FIND_BY_CODE = "Role.findByCode";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "Role.findByName";
      
  
  @ManyToMany(mappedBy="roles")
  private Collection<User> users;
  
  @ManyToMany
  @JoinTable(name="AD_ROLE_ACL", joinColumns={@JoinColumn(name="ROLES_ID")}, inverseJoinColumns={@JoinColumn(name="ACL_ID")})
  private Collection<AccessControl> accessControls;
  
  @ManyToMany
  @JoinTable(name="AD_ROLE_MENU", joinColumns={@JoinColumn(name="ROLE_ID")}, inverseJoinColumns={@JoinColumn(name="MENU_ID")})
  private Collection<Menu> menus;
  
  @ManyToMany
  @JoinTable(name="AD_ROLE_MENUITEM", joinColumns={@JoinColumn(name="ROLE_ID")}, inverseJoinColumns={@JoinColumn(name="MENUITEM_ID")})
  private Collection<MenuItem> menuItems;
  
  public Collection<User> getUsers() {
    return this.users;
  }
  
  public void setUsers(Collection<User> users) {
    this.users = users;
  }
  
  public Collection<AccessControl> getAccessControls() {
    return this.accessControls;
  }
  
  public void setAccessControls(Collection<AccessControl> accessControls) {
    this.accessControls = accessControls;
  }
  
  public Collection<Menu> getMenus() {
    return this.menus;
  }
  
  public void setMenus(Collection<Menu> menus) {
    this.menus = menus;
  }
  
  public Collection<MenuItem> getMenuItems() {
    return this.menuItems;
  }
  
  public void setMenuItems(Collection<MenuItem> menuItems) {
    this.menuItems = menuItems;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
