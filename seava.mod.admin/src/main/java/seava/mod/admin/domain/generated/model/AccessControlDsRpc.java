/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractAuditable;
import seava.mod.admin.domain.generated.model.AccessControl;

@Entity(name=AccessControlDsRpc.ALIAS)
@Table(name=AccessControlDsRpc.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=AccessControlDsRpc.NQ_FIND_BY_SRVMTD,
    query="SELECT e FROM "+AccessControlDsRpc.ALIAS+" e WHERE e.clientId = :clientId and e.accessControl = :accessControl and e.dsName = :dsName and e.serviceMethod = :serviceMethod",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
  ,@NamedQuery(name=AccessControlDsRpc.NQ_FIND_BY_SRVMTD_PRIMITIVE, query="SELECT e FROM "+AccessControlDsRpc.ALIAS+" e WHERE e.clientId = :clientId and e.accessControl.id = :accessControlId and e.dsName = :dsName and e.serviceMethod = :serviceMethod", hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE))
})
public class AccessControlDsRpc extends AbstractAuditable {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_AccessControlDsRpc";
  
  public static final String TABLE_NAME = "AD_ACL_DS_RPC";
  /**
   * Named query find by unique key: Srvmtd.
   */
  public static final String NQ_FIND_BY_SRVMTD = "AccessControlDsRpc.findBySrvmtd";
  /**
   * Named query find by unique key: Srvmtd using the ID field for references.
   */
  public static final String NQ_FIND_BY_SRVMTD_PRIMITIVE = "AccessControlDsRpc.findBySrvmtd_PRIMITIVE";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=AccessControl.class)
  @JoinColumn(name="ACCESSCONTROL_ID", referencedColumnName="ID")
  private AccessControl accessControl;
  
  @Column(name="DSNAME", nullable=false)
  private String dsName;
  
  @Column(name="SERVICEMETHOD", nullable=false)
  private String serviceMethod;
  
  public AccessControl getAccessControl() {
    return this.accessControl;
  }
  
  public void setAccessControl(AccessControl accessControl) {
    this.accessControl = accessControl;
  }
  
  public String getDsName() {
    return this.dsName;
  }
  
  public void setDsName(String dsName) {
    this.dsName = dsName;
  }
  
  public String getServiceMethod() {
    return this.serviceMethod;
  }
  
  public void setServiceMethod(String serviceMethod) {
    this.serviceMethod = serviceMethod;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
