/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractType;

@Entity(name=ViewState.ALIAS)
@Table(name=ViewState.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=ViewState.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+ViewState.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class ViewState extends AbstractType {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_ViewState";
  
  public static final String TABLE_NAME = "AD_VIEW_STATE";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "ViewState.findByName";
      
  
  @Column(name="CMP", nullable=false)
  private String cmp;
  
  @Column(name="CMPTYPE", nullable=false)
  private String cmpType;
  
  @Column(name="VALUE")
  private String value;
  
  public String getCmp() {
    return this.cmp;
  }
  
  public void setCmp(String cmp) {
    this.cmp = cmp;
  }
  
  public String getCmpType() {
    return this.cmpType;
  }
  
  public void setCmpType(String cmpType) {
    this.cmpType = cmpType;
  }
  
  public String getValue() {
    return this.value;
  }
  
  public void setValue(String value) {
    this.value = value;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
