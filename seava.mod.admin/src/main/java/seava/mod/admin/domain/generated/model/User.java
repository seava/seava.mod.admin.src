/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeWithCode;
import seava.mod.admin.domain.generated.model.Role;
import seava.mod.admin.domain.generated.model.UserGroup;
import seava.mod.system.domain.generated.model.DateFormat;

@Entity(name=User.ALIAS)
@Table(name=User.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=User.NQ_FIND_BY_LOGIN,
    query="SELECT e FROM "+User.ALIAS+" e WHERE e.clientId = :clientId and e.loginName = :loginName",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  ),
  @NamedQuery(
    name=User.NQ_FIND_BY_CODE,
    query="SELECT e FROM "+User.ALIAS+" e WHERE e.clientId = :clientId and e.code = :code",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  ),
  @NamedQuery(
    name=User.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+User.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class User extends AbstractTypeWithCode {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_User";
  
  public static final String TABLE_NAME = "AD_USR";
  /**
   * Named query find by unique key: Login.
   */
  public static final String NQ_FIND_BY_LOGIN = "User.findByLogin";
  /**
   * Named query find by unique key: Code.
   */
  public static final String NQ_FIND_BY_CODE = "User.findByCode";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "User.findByName";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=DateFormat.class)
  @JoinColumn(name="DATEFORMAT_ID", referencedColumnName="ID")
  private DateFormat dateFormat;
  
  @Column(name="LOGINNAME", nullable=false)
  private String loginName;
  
  @Column(name="PASSWORD", nullable=false)
  private String password;
  
  @Column(name="EMAIL")
  private String email;
  
  @Column(name="LOCKED", nullable=false)
  private Boolean locked;
  
  @Column(name="DECIMALSEPARATOR")
  private String decimalSeparator;
  
  @Column(name="THOUSANDSEPARATOR")
  private String thousandSeparator;
  
  @ManyToMany
  @JoinTable(name="AD_USR_ROLE", joinColumns={@JoinColumn(name="USER_ID")}, inverseJoinColumns={@JoinColumn(name="ROLE_ID")})
  private Collection<Role> roles;
  
  @ManyToMany
  @JoinTable(name="AD_USR_USRGRP", joinColumns={@JoinColumn(name="USERS_ID")}, inverseJoinColumns={@JoinColumn(name="GROUP_ID")})
  private Collection<UserGroup> groups;
  
  public DateFormat getDateFormat() {
    return this.dateFormat;
  }
  
  public void setDateFormat(DateFormat dateFormat) {
    this.dateFormat = dateFormat;
  }
  
  public String getLoginName() {
    return this.loginName;
  }
  
  public void setLoginName(String loginName) {
    this.loginName = loginName;
  }
  
  public String getPassword() {
    return this.password;
  }
  
  public void setPassword(String password) {
    this.password = password;
  }
  
  public String getEmail() {
    return this.email;
  }
  
  public void setEmail(String email) {
    this.email = email;
  }
  
  public Boolean getLocked() {
    return this.locked;
  }
  
  public void setLocked(Boolean locked) {
    this.locked = locked;
  }
  
  public String getDecimalSeparator() {
    return this.decimalSeparator;
  }
  
  public void setDecimalSeparator(String decimalSeparator) {
    this.decimalSeparator = decimalSeparator;
  }
  
  public String getThousandSeparator() {
    return this.thousandSeparator;
  }
  
  public void setThousandSeparator(String thousandSeparator) {
    this.thousandSeparator = thousandSeparator;
  }
  
  public Collection<Role> getRoles() {
    return this.roles;
  }
  
  public void setRoles(Collection<Role> roles) {
    this.roles = roles;
  }
  
  public Collection<UserGroup> getGroups() {
    return this.groups;
  }
  
  public void setGroups(Collection<UserGroup> groups) {
    this.groups = groups;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
    if (this.locked == null) {
      this.locked = false;
    }
  }
}
