/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import java.util.Collection;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import javax.persistence.Transient;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractType;
import seava.mod.admin.domain.generated.model.Menu;
import seava.mod.admin.domain.generated.model.MenuItem;
import seava.mod.admin.domain.generated.model.Role;

@Entity(name=MenuItem.ALIAS)
@Table(name=MenuItem.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=MenuItem.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+MenuItem.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class MenuItem extends AbstractType {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_MenuItem";
  
  public static final String TABLE_NAME = "AD_MENU_ITEM";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "MenuItem.findByName";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=MenuItem.class)
  @JoinColumn(name="MENUITEM_ID", referencedColumnName="ID")
  private MenuItem menuItem;
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=Menu.class)
  @JoinColumn(name="MENU_ID", referencedColumnName="ID")
  private Menu menu;
  
  @Column(name="SEQUENCENO")
  private Integer sequenceNo;
  
  @Column(name="TITLE", nullable=false)
  private String title;
  
  @Column(name="FRAME")
  private String frame;
  
  @Column(name="ICONURL")
  private String iconUrl;
  
  @Column(name="SEPARATORBEFORE")
  private Boolean separatorBefore;
  
  @Column(name="SEPARATORAFTER")
  private Boolean separatorAfter;
  
  @Transient
  private Boolean leafNode;
  
  @ManyToMany(mappedBy="menuItems")
  private Collection<Role> roles;
  
  public MenuItem getMenuItem() {
    return this.menuItem;
  }
  
  public void setMenuItem(MenuItem menuItem) {
    this.menuItem = menuItem;
  }
  
  public Menu getMenu() {
    return this.menu;
  }
  
  public void setMenu(Menu menu) {
    this.menu = menu;
  }
  
  public Integer getSequenceNo() {
    return this.sequenceNo;
  }
  
  public void setSequenceNo(Integer sequenceNo) {
    this.sequenceNo = sequenceNo;
  }
  
  public String getTitle() {
    return this.title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getFrame() {
    return this.frame;
  }
  
  public void setFrame(String frame) {
    this.frame = frame;
  }
  
  public String getIconUrl() {
    return this.iconUrl;
  }
  
  public void setIconUrl(String iconUrl) {
    this.iconUrl = iconUrl;
  }
  
  public Boolean getSeparatorBefore() {
    return this.separatorBefore;
  }
  
  public void setSeparatorBefore(Boolean separatorBefore) {
    this.separatorBefore = separatorBefore;
  }
  
  public Boolean getSeparatorAfter() {
    return this.separatorAfter;
  }
  
  public void setSeparatorAfter(Boolean separatorAfter) {
    this.separatorAfter = separatorAfter;
  }
  
  @Transient
  public Boolean getLeafNode() {
    return (this.frame != null && !this.frame.equals("") );
  }
  
  public void setLeafNode(Boolean leafNode) {
    this.leafNode = leafNode;
  }
  
  public Collection<Role> getRoles() {
    return this.roles;
  }
  
  public void setRoles(Collection<Role> roles) {
    this.roles = roles;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
