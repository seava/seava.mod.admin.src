/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractAuditable;
import seava.mod.admin.domain.generated.model.AccessControl;

@Entity(name=AccessControlAsgn.ALIAS)
@Table(name=AccessControlAsgn.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=AccessControlAsgn.NQ_FIND_BY_UNIQUE,
    query="SELECT e FROM "+AccessControlAsgn.ALIAS+" e WHERE e.clientId = :clientId and e.accessControl = :accessControl and e.asgnName = :asgnName",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
  ,@NamedQuery(name=AccessControlAsgn.NQ_FIND_BY_UNIQUE_PRIMITIVE, query="SELECT e FROM "+AccessControlAsgn.ALIAS+" e WHERE e.clientId = :clientId and e.accessControl.id = :accessControlId and e.asgnName = :asgnName", hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE))
})
public class AccessControlAsgn extends AbstractAuditable {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_AccessControlAsgn";
  
  public static final String TABLE_NAME = "AD_ACL_ASGN";
  /**
   * Named query find by unique key: Unique.
   */
  public static final String NQ_FIND_BY_UNIQUE = "AccessControlAsgn.findByUnique";
  /**
   * Named query find by unique key: Unique using the ID field for references.
   */
  public static final String NQ_FIND_BY_UNIQUE_PRIMITIVE = "AccessControlAsgn.findByUnique_PRIMITIVE";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=AccessControl.class)
  @JoinColumn(name="ACCESSCONTROL_ID", referencedColumnName="ID")
  private AccessControl accessControl;
  
  @Column(name="ASGNNAME", nullable=false)
  private String asgnName;
  
  @Column(name="QUERYALLOWED", nullable=false)
  private Boolean queryAllowed;
  
  @Column(name="UPDATEALLOWED", nullable=false)
  private Boolean updateAllowed;
  
  @Column(name="IMPORTALLOWED", nullable=false)
  private Boolean importAllowed;
  
  @Column(name="EXPORTALLOWED", nullable=false)
  private Boolean exportAllowed;
  
  public AccessControl getAccessControl() {
    return this.accessControl;
  }
  
  public void setAccessControl(AccessControl accessControl) {
    this.accessControl = accessControl;
  }
  
  public String getAsgnName() {
    return this.asgnName;
  }
  
  public void setAsgnName(String asgnName) {
    this.asgnName = asgnName;
  }
  
  public Boolean getQueryAllowed() {
    return this.queryAllowed;
  }
  
  public void setQueryAllowed(Boolean queryAllowed) {
    this.queryAllowed = queryAllowed;
  }
  
  public Boolean getUpdateAllowed() {
    return this.updateAllowed;
  }
  
  public void setUpdateAllowed(Boolean updateAllowed) {
    this.updateAllowed = updateAllowed;
  }
  
  public Boolean getImportAllowed() {
    return this.importAllowed;
  }
  
  public void setImportAllowed(Boolean importAllowed) {
    this.importAllowed = importAllowed;
  }
  
  public Boolean getExportAllowed() {
    return this.exportAllowed;
  }
  
  public void setExportAllowed(Boolean exportAllowed) {
    this.exportAllowed = exportAllowed;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
    if (this.queryAllowed == null) {
      this.queryAllowed = false;
    }
    if (this.updateAllowed == null) {
      this.updateAllowed = false;
    }
    if (this.importAllowed == null) {
      this.importAllowed = false;
    }
    if (this.exportAllowed == null) {
      this.exportAllowed = false;
    }
  }
}
