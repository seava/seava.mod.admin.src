/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractAuditable;
import seava.mod.admin.domain.generated.model.Report;

@Entity(name=DsReport.ALIAS)
@Table(name=DsReport.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=DsReport.NQ_FIND_BY_REP_DS,
    query="SELECT e FROM "+DsReport.ALIAS+" e WHERE e.clientId = :clientId and e.report = :report and e.dataSource = :dataSource",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
  ,@NamedQuery(name=DsReport.NQ_FIND_BY_REP_DS_PRIMITIVE, query="SELECT e FROM "+DsReport.ALIAS+" e WHERE e.clientId = :clientId and e.report.id = :reportId and e.dataSource = :dataSource", hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE))
})
public class DsReport extends AbstractAuditable {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_DsReport";
  
  public static final String TABLE_NAME = "AD_RPT_DS";
  /**
   * Named query find by unique key: Rep_ds.
   */
  public static final String NQ_FIND_BY_REP_DS = "DsReport.findByRep_ds";
  /**
   * Named query find by unique key: Rep_ds using the ID field for references.
   */
  public static final String NQ_FIND_BY_REP_DS_PRIMITIVE = "DsReport.findByRep_ds_PRIMITIVE";
      
  
  @ManyToOne(fetch=FetchType.LAZY, targetEntity=Report.class)
  @JoinColumn(name="REPORT_ID", referencedColumnName="ID")
  private Report report;
  
  @Column(name="DATASOURCE", nullable=false)
  private String dataSource;
  
  public Report getReport() {
    return this.report;
  }
  
  public void setReport(Report report) {
    this.report = report;
  }
  
  public String getDataSource() {
    return this.dataSource;
  }
  
  public void setDataSource(String dataSource) {
    this.dataSource = dataSource;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
