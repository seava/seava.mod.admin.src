/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.domain.generated.model;

import java.util.Collection;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.QueryHint;
import javax.persistence.Table;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import seava.mod.abstracts.domain.generated.model.AbstractTypeWithCode;
import seava.mod.admin.domain.generated.model.User;

@Entity(name=UserGroup.ALIAS)
@Table(name=UserGroup.TABLE_NAME)
@NamedQueries({
  @NamedQuery(
    name=UserGroup.NQ_FIND_BY_CODE,
    query="SELECT e FROM "+UserGroup.ALIAS+" e WHERE e.clientId = :clientId and e.code = :code",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  ),
  @NamedQuery(
    name=UserGroup.NQ_FIND_BY_NAME,
    query="SELECT e FROM "+UserGroup.ALIAS+" e WHERE e.clientId = :clientId and e.name = :name",
    hints=@QueryHint(name=QueryHints.BIND_PARAMETERS, value=HintValues.TRUE)
  )
})
public class UserGroup extends AbstractTypeWithCode {
  
  private static final long serialVersionUID = -8865917134914502125L;
  
  public static final String ALIAS = "ad_UserGroup";
  
  public static final String TABLE_NAME = "AD_USRGRP";
  /**
   * Named query find by unique key: Code.
   */
  public static final String NQ_FIND_BY_CODE = "UserGroup.findByCode";
  /**
   * Named query find by unique key: Name.
   */
  public static final String NQ_FIND_BY_NAME = "UserGroup.findByName";
      
  
  @ManyToMany(mappedBy="groups")
  private Collection<User> users;
  
  public Collection<User> getUsers() {
    return this.users;
  }
  
  public void setUsers(Collection<User> users) {
    this.users = users;
  }
  
  protected void applyDefaultValues() {
    super.applyDefaultValues();
  }
}
