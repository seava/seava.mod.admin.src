/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCode_Ds;
import seava.mod.admin.domain.generated.model.Report;

@Ds(entity=Report.class, sort={@SortField(field=ReportRt_Ds.f_name)})
public class ReportRt_Ds extends AbstractTypeWithCode_Ds<Report> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_ReportRt_Ds";
	
	public static final String f_reportServerId = "reportServerId";
	public static final String f_reportServer = "reportServer";
	public static final String f_serverUrl = "serverUrl";
	public static final String f_queryBuilderClass = "queryBuilderClass";
	public static final String f_contextPath = "contextPath";
	
	@DsField(join="left", path="reportServer.id")
	private String reportServerId;
	
	@DsField(join="left", path="reportServer.name")
	private String reportServer;
	
	@DsField(join="left", path="reportServer.url")
	private String serverUrl;
	
	@DsField(join="left", path="reportServer.queryBuilderClass")
	private String queryBuilderClass;
	
	@DsField
	private String contextPath;
	
	public ReportRt_Ds() {
		super();
	}
	
	public ReportRt_Ds(Report e) {
		super(e);
	}
	
	public String getReportServerId() {
	  return this.reportServerId;
	}
	
	public void setReportServerId(String reportServerId) {
	  this.reportServerId = reportServerId;
	}
	
	public String getReportServer() {
	  return this.reportServer;
	}
	
	public void setReportServer(String reportServer) {
	  this.reportServer = reportServer;
	}
	
	public String getServerUrl() {
	  return this.serverUrl;
	}
	
	public void setServerUrl(String serverUrl) {
	  this.serverUrl = serverUrl;
	}
	
	public String getQueryBuilderClass() {
	  return this.queryBuilderClass;
	}
	
	public void setQueryBuilderClass(String queryBuilderClass) {
	  this.queryBuilderClass = queryBuilderClass;
	}
	
	public String getContextPath() {
	  return this.contextPath;
	}
	
	public void setContextPath(String contextPath) {
	  this.contextPath = contextPath;
	}
}
