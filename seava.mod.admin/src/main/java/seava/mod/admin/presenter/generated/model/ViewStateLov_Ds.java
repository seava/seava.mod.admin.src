/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeLov_Ds;
import seava.mod.admin.domain.generated.model.ViewState;

@Ds(entity=ViewState.class, sort={@SortField(field=ViewStateLov_Ds.f_name)})
public class ViewStateLov_Ds extends AbstractTypeLov_Ds<ViewState> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_ViewStateLov_Ds";
	
	
	public ViewStateLov_Ds() {
		super();
	}
	
	public ViewStateLov_Ds(ViewState e) {
		super(e);
	}
}
