/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.qb;

import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.AttachmentTypeLov_Ds;
import seava.lib.j4e.api.base.session.Session;

public class AttachmentTypeLov_DsQb extends QueryBuilderWithJpql<AttachmentTypeLov_Ds,AttachmentTypeLov_Ds, Object> {
  
  @Override
  public void setFilter(AttachmentTypeLov_Ds filter) {
    if (filter.getTargetType() == null || filter.getTargetType().equals("")) {
      filter.setTargetType("N/A");
    }
    super.setFilter(filter);
  }
}
