/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractType_Ds;
import seava.mod.admin.domain.generated.model.AttachmentType;

@Ds(entity=AttachmentType.class, sort={@SortField(field=AttachmentType_Ds.f_name)})
public class AttachmentType_Ds extends AbstractType_Ds<AttachmentType> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_AttachmentType_Ds";
	
	public static final String f_category = "category";
	public static final String f_uploadPath = "uploadPath";
	public static final String f_baseUrl = "baseUrl";
	
	@DsField
	private String category;
	
	@DsField
	private String uploadPath;
	
	@DsField
	private String baseUrl;
	
	public AttachmentType_Ds() {
		super();
	}
	
	public AttachmentType_Ds(AttachmentType e) {
		super(e);
	}
	
	public String getCategory() {
	  return this.category;
	}
	
	public void setCategory(String category) {
	  this.category = category;
	}
	
	public String getUploadPath() {
	  return this.uploadPath;
	}
	
	public void setUploadPath(String uploadPath) {
	  this.uploadPath = uploadPath;
	}
	
	public String getBaseUrl() {
	  return this.baseUrl;
	}
	
	public void setBaseUrl(String baseUrl) {
	  this.baseUrl = baseUrl;
	}
}
