/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.Param;
import seava.lib.j4e.api.presenter.annotation.RefLookup;
import seava.lib.j4e.api.presenter.annotation.RefLookups;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractAuditable_Ds;
import seava.mod.admin.domain.generated.model.AccessControl;
import seava.mod.admin.domain.generated.model.AccessControlDs;

@Ds(entity=AccessControlDs.class, sort={@SortField(field=AccessControlDs_Ds.f_dsName)})
@RefLookups({
	@RefLookup(refId = AccessControlDs_Ds.f_accessControlId, namedQuery = AccessControl.NQ_FIND_BY_NAME, params = {
		@Param(name = "name", field = AccessControlDs_Ds.f_accessControl)
	})
})
public class AccessControlDs_Ds extends AbstractAuditable_Ds<AccessControlDs> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_AccessControlDs_Ds";
	
	public static final String f_dsName = "dsName";
	public static final String f_queryAllowed = "queryAllowed";
	public static final String f_insertAllowed = "insertAllowed";
	public static final String f_updateAllowed = "updateAllowed";
	public static final String f_deleteAllowed = "deleteAllowed";
	public static final String f_importAllowed = "importAllowed";
	public static final String f_exportAllowed = "exportAllowed";
	public static final String f_accessControlId = "accessControlId";
	public static final String f_accessControl = "accessControl";
	
	@DsField
	private String dsName;
	
	@DsField
	private Boolean queryAllowed;
	
	@DsField
	private Boolean insertAllowed;
	
	@DsField
	private Boolean updateAllowed;
	
	@DsField
	private Boolean deleteAllowed;
	
	@DsField
	private Boolean importAllowed;
	
	@DsField
	private Boolean exportAllowed;
	
	@DsField(join="left", path="accessControl.id")
	private String accessControlId;
	
	@DsField(join="left", path="accessControl.name")
	private String accessControl;
	
	public AccessControlDs_Ds() {
		super();
	}
	
	public AccessControlDs_Ds(AccessControlDs e) {
		super(e);
	}
	
	public String getDsName() {
	  return this.dsName;
	}
	
	public void setDsName(String dsName) {
	  this.dsName = dsName;
	}
	
	public Boolean getQueryAllowed() {
	  return this.queryAllowed;
	}
	
	public void setQueryAllowed(Boolean queryAllowed) {
	  this.queryAllowed = queryAllowed;
	}
	
	public Boolean getInsertAllowed() {
	  return this.insertAllowed;
	}
	
	public void setInsertAllowed(Boolean insertAllowed) {
	  this.insertAllowed = insertAllowed;
	}
	
	public Boolean getUpdateAllowed() {
	  return this.updateAllowed;
	}
	
	public void setUpdateAllowed(Boolean updateAllowed) {
	  this.updateAllowed = updateAllowed;
	}
	
	public Boolean getDeleteAllowed() {
	  return this.deleteAllowed;
	}
	
	public void setDeleteAllowed(Boolean deleteAllowed) {
	  this.deleteAllowed = deleteAllowed;
	}
	
	public Boolean getImportAllowed() {
	  return this.importAllowed;
	}
	
	public void setImportAllowed(Boolean importAllowed) {
	  this.importAllowed = importAllowed;
	}
	
	public Boolean getExportAllowed() {
	  return this.exportAllowed;
	}
	
	public void setExportAllowed(Boolean exportAllowed) {
	  this.exportAllowed = exportAllowed;
	}
	
	public String getAccessControlId() {
	  return this.accessControlId;
	}
	
	public void setAccessControlId(String accessControlId) {
	  this.accessControlId = accessControlId;
	}
	
	public String getAccessControl() {
	  return this.accessControl;
	}
	
	public void setAccessControl(String accessControl) {
	  this.accessControl = accessControl;
	}
}
