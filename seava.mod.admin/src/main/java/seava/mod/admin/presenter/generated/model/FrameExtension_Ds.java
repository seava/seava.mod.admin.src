/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractAuditable_Ds;
import seava.mod.admin.domain.generated.model.FrameExtension;

@Ds(entity=FrameExtension.class, sort={@SortField(field=FrameExtension_Ds.f_frame), @SortField(field=FrameExtension_Ds.f_sequenceNo)})
public class FrameExtension_Ds extends AbstractAuditable_Ds<FrameExtension> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_FrameExtension_Ds";
	
	public static final String f_frame = "frame";
	public static final String f_sequenceNo = "sequenceNo";
	public static final String f_fileLocation = "fileLocation";
	public static final String f_relativePath = "relativePath";
	public static final String f_active = "active";
	
	@DsField
	private String frame;
	
	@DsField
	private Integer sequenceNo;
	
	@DsField
	private String fileLocation;
	
	@DsField
	private Boolean relativePath;
	
	@DsField
	private Boolean active;
	
	public FrameExtension_Ds() {
		super();
	}
	
	public FrameExtension_Ds(FrameExtension e) {
		super(e);
	}
	
	public String getFrame() {
	  return this.frame;
	}
	
	public void setFrame(String frame) {
	  this.frame = frame;
	}
	
	public Integer getSequenceNo() {
	  return this.sequenceNo;
	}
	
	public void setSequenceNo(Integer sequenceNo) {
	  this.sequenceNo = sequenceNo;
	}
	
	public String getFileLocation() {
	  return this.fileLocation;
	}
	
	public void setFileLocation(String fileLocation) {
	  this.fileLocation = fileLocation;
	}
	
	public Boolean getRelativePath() {
	  return this.relativePath;
	}
	
	public void setRelativePath(Boolean relativePath) {
	  this.relativePath = relativePath;
	}
	
	public Boolean getActive() {
	  return this.active;
	}
	
	public void setActive(Boolean active) {
	  this.active = active;
	}
}
