/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.extensions.qb;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.domain.generated.model.MenuItem;
import seava.mod.admin.presenter.generated.model.MenuItemRtLov_Ds;

public class MenuItemRtLov_DsQb extends
		QueryBuilderWithJpql<MenuItemRtLov_Ds, MenuItemRtLov_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws ManagedException {
		if (!Session.user.get().getProfile().isAdministrator()) {
			addFilterCondition("  e.id in ( select p.id from  "
					+ MenuItem.ALIAS
					+ " p, IN (p.roles) c where c.code in :pRoles )  ");
			this.addCustomFilterItem("pRoles", Session.user.get().getProfile()
					.getRoles());
		}
	}

}
