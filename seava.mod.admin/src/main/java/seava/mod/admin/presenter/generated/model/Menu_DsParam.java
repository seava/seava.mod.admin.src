/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

public class Menu_DsParam {
  
  public static final String f_withRole = "withRole";
  public static final String f_withRoleId = "withRoleId";
  
  private String  withRole;
  
  private String  withRoleId;
  
  public String getWithRole() {
    return this.withRole;
  }
  
  public void setWithRole(String withRole) {
    this.withRole = withRole;
  }
  
  public String getWithRoleId() {
    return this.withRoleId;
  }
  
  public void setWithRoleId(String withRoleId) {
    this.withRoleId = withRoleId;
  }
}
