/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractType_Ds;
import seava.mod.admin.domain.generated.model.Menu;

@Ds(entity=Menu.class, sort={@SortField(field=Menu_Ds.f_name)})
public class Menu_Ds extends AbstractType_Ds<Menu> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_Menu_Ds";
	
	public static final String f_sequenceNo = "sequenceNo";
	public static final String f_title = "title";
	public static final String f_tag = "tag";
	
	@DsField
	private Integer sequenceNo;
	
	@DsField
	private String title;
	
	@DsField
	private String tag;
	
	public Menu_Ds() {
		super();
	}
	
	public Menu_Ds(Menu e) {
		super(e);
	}
	
	public Integer getSequenceNo() {
	  return this.sequenceNo;
	}
	
	public void setSequenceNo(Integer sequenceNo) {
	  this.sequenceNo = sequenceNo;
	}
	
	public String getTitle() {
	  return this.title;
	}
	
	public void setTitle(String title) {
	  this.title = title;
	}
	
	public String getTag() {
	  return this.tag;
	}
	
	public void setTag(String tag) {
	  this.tag = tag;
	}
}
