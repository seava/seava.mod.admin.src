package seava.mod.admin.presenter.extensions;

import java.util.List;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.api.presenter.extensions.IExtensionContentProvider;
import seava.lib.j4e.api.presenter.service.IDsService;
import seava.lib.j4e.presenter.service.AbstractPresenterBaseService;
import seava.mod.admin.domain.generated.model.DsReportParam;
import seava.mod.admin.domain.generated.model.ReportParam;
import seava.mod.admin.presenter.generated.model.DsReportParamRt_Ds;
import seava.mod.admin.presenter.generated.model.DsReportUsageRt_Ds;

public class ExtensionProviderReports extends AbstractPresenterBaseService
		implements IExtensionContentProvider {

	@Override
	public String getContent(String targetFrame) throws ManagedException {

		StringBuffer sb = new StringBuffer();
		this.addFrameReports(targetFrame, sb);
		return sb.toString();
	}

	protected void addFrameReports(String targetFrame, StringBuffer sb)
			throws ManagedException {

		IDsService<DsReportUsageRt_Ds, DsReportUsageRt_Ds, Object> srv = this
				.findDsService(DsReportUsageRt_Ds.ALIAS);
		IDsService<DsReportParamRt_Ds, DsReportParamRt_Ds, Object> srvParam = this
				.findDsService(DsReportParamRt_Ds.ALIAS);

		DsReportUsageRt_Ds filter = new DsReportUsageRt_Ds();
		filter.setFrameName(targetFrame);
		List<DsReportUsageRt_Ds> reports = srv.find(filter);

		DsReportParamRt_Ds filterParam = new DsReportParamRt_Ds();

		IEntityService<ReportParam> rpsrv = this
				.findEntityService(ReportParam.class);

		int i = 0;
		sb.append("frameReports = [");
		for (DsReportUsageRt_Ds report : reports) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append("{");
			sb.append("dcAlias:\"" + report.getDcKey() + "\"");
			sb.append(",report:\"" + report.getReportCode() + "\"");
			sb.append(",title:\"" + report.getReportTitle() + "\"");
			sb.append(",toolbar:\"" + report.getToolbarKey() + "\"");
			sb.append(",url:\"" + report.getServerUrl() + "\"");
			sb.append(",queryBuilderClass:\"" + report.getQueryBuilderClass()
					+ "\"");

			if (report.getReportContextPath() != null
					&& !report.getReportContextPath().equals("")) {
				sb.append(",contextPath:\"" + report.getReportContextPath()
						+ "\"");
			}

			sb.append(",params:[");
			filterParam.setDsReportId(report.getDsReportId());

			List<DsReportParamRt_Ds> params = srvParam.find(filterParam);
			int j = 0;
			for (DsReportParamRt_Ds param : params) {
				if (j > 0) {
					sb.append(",");
				}
				sb.append("{");

				sb.append(" code:\"" + param.getParamName() + "\"");
				sb.append(",type:\"" + param.getParamDataType() + "\"");
				sb.append(",mandatory:\"" + param.getParamMandatory() + "\"");
				if (param.getDsField() != null
						&& !param.getDsField().equals("")) {
					sb.append(",dsField:\"" + param.getDsField() + "\"");
				}
				if (param.getParamListOfValues() != null
						&& !param.getParamListOfValues().equals("")) {
					sb.append(",listOfValues:\"" + param.getParamListOfValues()
							+ "\"");
				}

				if (param.getStaticValue() != null
						&& !param.getStaticValue().equals("")) {
					sb.append(",value:\"" + param.getStaticValue() + "\"");
				} else {
					sb.append(",value:\""
							+ ((param.getParamDefaultValue() != null) ? param
									.getParamDefaultValue() : "") + "\"");
				}
				sb.append("}");
				j++;
			}

			List<ReportParam> rplist = rpsrv
					.getEntityManager()
					.createQuery(
							"select e from "
									+ ReportParam.ALIAS
									+ " e "
									+ "where e.report.id = :reportId"
									+ "  and e.active = true "
									+ "  and e.id not in (select t.reportParam.id from "
									+ DsReportParam.ALIAS
									+ " t where t.dsReport.report.id = :reportId) ",
							ReportParam.class)
					.setParameter("reportId", report.getReportId())
					.getResultList();
			int k = 0;
			for (ReportParam rparam : rplist) {
				if (j > 0 || k > 0) {
					sb.append(",");
				}
				sb.append("{");

				sb.append(" code:\"" + rparam.getName() + "\"");
				sb.append(",type:\"" + rparam.getDataType() + "\"");
				sb.append(",mandatory:\"" + rparam.getMandatory() + "\"");

				if (rparam.getListOfValues() != null
						&& !rparam.getListOfValues().equals("")) {
					sb.append(",listOfValues:\"" + rparam.getListOfValues()
							+ "\"");
				}

				sb.append(",value:\""
						+ ((rparam.getDefaultValue() != null) ? rparam
								.getDefaultValue() : "") + "\"");

				sb.append("}");
				k++;
			}

			// params
			sb.append("]");
			sb.append("}");
			i++;
		}
		sb.append("];");
	}

}
