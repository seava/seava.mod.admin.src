/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.qb;

import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.MyUser_Ds;
import seava.lib.j4e.api.base.session.Session;

public class MyUser_DsQb extends QueryBuilderWithJpql<MyUser_Ds,MyUser_Ds, Object> {
  
  @Override
  public void setFilter(MyUser_Ds filter) {
    filter.setCode(Session.user.get().getCode());
    super.setFilter(filter);
  }
}
