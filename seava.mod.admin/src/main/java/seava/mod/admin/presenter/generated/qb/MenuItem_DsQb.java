/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.qb;

import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.MenuItem_Ds;
import seava.mod.admin.presenter.generated.model.MenuItem_DsParam;
import seava.lib.j4e.api.base.session.Session;

public class MenuItem_DsQb extends QueryBuilderWithJpql<MenuItem_Ds,MenuItem_Ds, MenuItem_DsParam> {
  
  @Override
  public void beforeBuildWhere() {
    if (this.params != null && this.params.getFoldersOnly() != null && !"".equals(this.params.getFoldersOnly())) {
      addFilterCondition("  (  (:foldersOnly = false  ) or (e.frame is null or e.frame = '' ) ) "); 
      addCustomFilterItem("foldersOnly",this.params.getFoldersOnly());
    }
    if (this.params != null && this.params.getFramesOnly() != null && !"".equals(this.params.getFramesOnly())) {
      addFilterCondition("  (  (:framesOnly = false  ) or (e.frame is not null and e.frame <> '' ) ) "); 
      addCustomFilterItem("framesOnly",this.params.getFramesOnly());
    }
    if (this.params != null && this.params.getWithRoleId() != null && !"".equals(this.params.getWithRoleId())) {
      addFilterCondition("  e.id in ( select p.id from  ad_MenuItem p, IN (p.roles) c where c.id = :withRoleId )  "); 
      addCustomFilterItem("withRoleId",this.params.getWithRoleId());
    }
  }
}
