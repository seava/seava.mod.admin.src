/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCode_Ds;
import seava.mod.admin.domain.generated.model.UserGroup;

@Ds(entity=UserGroup.class, sort={@SortField(field=UserGroup_Ds.f_code)})
public class UserGroup_Ds extends AbstractTypeWithCode_Ds<UserGroup> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_UserGroup_Ds";
	
	
	public UserGroup_Ds() {
		super();
	}
	
	public UserGroup_Ds(UserGroup e) {
		super(e);
	}
}
