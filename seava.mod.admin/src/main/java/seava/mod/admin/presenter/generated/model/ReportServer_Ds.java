/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractType_Ds;
import seava.mod.admin.domain.generated.model.ReportServer;

@Ds(entity=ReportServer.class, sort={@SortField(field=ReportServer_Ds.f_name)})
public class ReportServer_Ds extends AbstractType_Ds<ReportServer> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_ReportServer_Ds";
	
	public static final String f_url = "url";
	public static final String f_queryBuilderClass = "queryBuilderClass";
	
	@DsField
	private String url;
	
	@DsField
	private String queryBuilderClass;
	
	public ReportServer_Ds() {
		super();
	}
	
	public ReportServer_Ds(ReportServer e) {
		super(e);
	}
	
	public String getUrl() {
	  return this.url;
	}
	
	public void setUrl(String url) {
	  this.url = url;
	}
	
	public String getQueryBuilderClass() {
	  return this.queryBuilderClass;
	}
	
	public void setQueryBuilderClass(String queryBuilderClass) {
	  this.queryBuilderClass = queryBuilderClass;
	}
}
