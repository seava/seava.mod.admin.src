/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCodeNT_Ds;
import seava.mod.system.domain.generated.model.Client;

@Ds(entity=Client.class, sort={@SortField(field=MyClient_Ds.f_id)})
public class MyClient_Ds extends AbstractTypeWithCodeNT_Ds<Client> implements IModelWithId<String> {
	
	public static final String ALIAS = "ad_MyClient_Ds";
	
	public static final String f_workspacePath = "workspacePath";
	public static final String f_importPath = "importPath";
	public static final String f_exportPath = "exportPath";
	public static final String f_tempPath = "tempPath";
	public static final String f_adminRole = "adminRole";
	
	@DsField
	private String workspacePath;
	
	@DsField
	private String importPath;
	
	@DsField
	private String exportPath;
	
	@DsField
	private String tempPath;
	
	@DsField
	private String adminRole;
	
	public MyClient_Ds() {
		super();
	}
	
	public MyClient_Ds(Client e) {
		super(e);
	}
	
	public String getWorkspacePath() {
	  return this.workspacePath;
	}
	
	public void setWorkspacePath(String workspacePath) {
	  this.workspacePath = workspacePath;
	}
	
	public String getImportPath() {
	  return this.importPath;
	}
	
	public void setImportPath(String importPath) {
	  this.importPath = importPath;
	}
	
	public String getExportPath() {
	  return this.exportPath;
	}
	
	public void setExportPath(String exportPath) {
	  this.exportPath = exportPath;
	}
	
	public String getTempPath() {
	  return this.tempPath;
	}
	
	public void setTempPath(String tempPath) {
	  this.tempPath = tempPath;
	}
	
	public String getAdminRole() {
	  return this.adminRole;
	}
	
	public void setAdminRole(String adminRole) {
	  this.adminRole = adminRole;
	}
}
