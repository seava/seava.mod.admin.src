/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.Param;
import seava.lib.j4e.api.presenter.annotation.RefLookup;
import seava.lib.j4e.api.presenter.annotation.RefLookups;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractType_Ds;
import seava.mod.admin.domain.generated.model.Report;
import seava.mod.admin.domain.generated.model.ReportParam;

@Ds(entity=ReportParam.class, sort={@SortField(field=ReportParam_Ds.f_reportCode), @SortField(field=ReportParam_Ds.f_name)})
@RefLookups({
	@RefLookup(refId = ReportParam_Ds.f_reportId, namedQuery = Report.NQ_FIND_BY_CODE, params = {
		@Param(name = "code", field = ReportParam_Ds.f_reportCode)
	})
})
public class ReportParam_Ds extends AbstractType_Ds<ReportParam> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_ReportParam_Ds";
	
	public static final String f_reportId = "reportId";
	public static final String f_reportCode = "reportCode";
	public static final String f_title = "title";
	public static final String f_sequenceNo = "sequenceNo";
	public static final String f_defaultValue = "defaultValue";
	public static final String f_dataType = "dataType";
	public static final String f_listOfValues = "listOfValues";
	public static final String f_noEdit = "noEdit";
	public static final String f_mandatory = "mandatory";
	
	@DsField(join="left", path="report.id")
	private String reportId;
	
	@DsField(join="left", path="report.code")
	private String reportCode;
	
	@DsField
	private String title;
	
	@DsField
	private Integer sequenceNo;
	
	@DsField
	private String defaultValue;
	
	@DsField
	private String dataType;
	
	@DsField
	private String listOfValues;
	
	@DsField
	private Boolean noEdit;
	
	@DsField
	private Boolean mandatory;
	
	public ReportParam_Ds() {
		super();
	}
	
	public ReportParam_Ds(ReportParam e) {
		super(e);
	}
	
	public String getReportId() {
	  return this.reportId;
	}
	
	public void setReportId(String reportId) {
	  this.reportId = reportId;
	}
	
	public String getReportCode() {
	  return this.reportCode;
	}
	
	public void setReportCode(String reportCode) {
	  this.reportCode = reportCode;
	}
	
	public String getTitle() {
	  return this.title;
	}
	
	public void setTitle(String title) {
	  this.title = title;
	}
	
	public Integer getSequenceNo() {
	  return this.sequenceNo;
	}
	
	public void setSequenceNo(Integer sequenceNo) {
	  this.sequenceNo = sequenceNo;
	}
	
	public String getDefaultValue() {
	  return this.defaultValue;
	}
	
	public void setDefaultValue(String defaultValue) {
	  this.defaultValue = defaultValue;
	}
	
	public String getDataType() {
	  return this.dataType;
	}
	
	public void setDataType(String dataType) {
	  this.dataType = dataType;
	}
	
	public String getListOfValues() {
	  return this.listOfValues;
	}
	
	public void setListOfValues(String listOfValues) {
	  this.listOfValues = listOfValues;
	}
	
	public Boolean getNoEdit() {
	  return this.noEdit;
	}
	
	public void setNoEdit(Boolean noEdit) {
	  this.noEdit = noEdit;
	}
	
	public Boolean getMandatory() {
	  return this.mandatory;
	}
	
	public void setMandatory(Boolean mandatory) {
	  this.mandatory = mandatory;
	}
}
