/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

public class UserGroup_DsParam {
  
  public static final String f_withUser = "withUser";
  public static final String f_withUserId = "withUserId";
  
  private String  withUser;
  
  private String  withUserId;
  
  public String getWithUser() {
    return this.withUser;
  }
  
  public void setWithUser(String withUser) {
    this.withUser = withUser;
  }
  
  public String getWithUserId() {
    return this.withUserId;
  }
  
  public void setWithUserId(String withUserId) {
    this.withUserId = withUserId;
  }
}
