/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.qb;

import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.MyClient_Ds;
import seava.lib.j4e.api.base.session.Session;

public class MyClient_DsQb extends QueryBuilderWithJpql<MyClient_Ds,MyClient_Ds, Object> {
  
  @Override
  public void setFilter(MyClient_Ds filter) {
    filter.setId(Session.user.get().getClientId());
    super.setFilter(filter);
  }
}
