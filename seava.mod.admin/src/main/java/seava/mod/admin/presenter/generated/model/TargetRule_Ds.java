/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractAuditable_Ds;
import seava.mod.admin.domain.generated.model.TargetRule;

@Ds(entity=TargetRule.class, sort={@SortField(field=TargetRule_Ds.f_targetAlias), @SortField(field=TargetRule_Ds.f_targetType)})
public class TargetRule_Ds extends AbstractAuditable_Ds<TargetRule> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_TargetRule_Ds";
	
	public static final String f_sourceRefId = "sourceRefId";
	public static final String f_targetAlias = "targetAlias";
	public static final String f_targetType = "targetType";
	
	@DsField
	private String sourceRefId;
	
	@DsField
	private String targetAlias;
	
	@DsField
	private String targetType;
	
	public TargetRule_Ds() {
		super();
	}
	
	public TargetRule_Ds(TargetRule e) {
		super(e);
	}
	
	public String getSourceRefId() {
	  return this.sourceRefId;
	}
	
	public void setSourceRefId(String sourceRefId) {
	  this.sourceRefId = sourceRefId;
	}
	
	public String getTargetAlias() {
	  return this.targetAlias;
	}
	
	public void setTargetAlias(String targetAlias) {
	  this.targetAlias = targetAlias;
	}
	
	public String getTargetType() {
	  return this.targetType;
	}
	
	public void setTargetType(String targetType) {
	  this.targetType = targetType;
	}
}
