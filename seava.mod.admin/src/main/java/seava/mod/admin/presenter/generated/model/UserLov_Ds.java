/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCodeLov_Ds;
import seava.mod.admin.domain.generated.model.User;

@Ds(entity=User.class, sort={@SortField(field=UserLov_Ds.f_code)})
public class UserLov_Ds extends AbstractTypeWithCodeLov_Ds<User> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_UserLov_Ds";
	
	
	public UserLov_Ds() {
		super();
	}
	
	public UserLov_Ds(User e) {
		super(e);
	}
}
