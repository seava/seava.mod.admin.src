/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import java.util.Date;
public class ParamValue_DsParam {
  
  public static final String f_validAt = "validAt";
  
  private Date  validAt;
  
  public Date getValidAt() {
    return this.validAt;
  }
  
  public void setValidAt(Date validAt) {
    this.validAt = validAt;
  }
}
