/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeLov_Ds;
import seava.mod.admin.domain.generated.model.ReportServer;

@Ds(entity=ReportServer.class, sort={@SortField(field=ReportServerLov_Ds.f_name)})
public class ReportServerLov_Ds extends AbstractTypeLov_Ds<ReportServer> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_ReportServerLov_Ds";
	
	
	public ReportServerLov_Ds() {
		super();
	}
	
	public ReportServerLov_Ds(ReportServer e) {
		super(e);
	}
}
