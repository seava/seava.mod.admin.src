package seava.mod.admin.presenter.extensions;

import java.util.List;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.presenter.extensions.IExtensionContentProvider;
import seava.lib.j4e.api.presenter.extensions.IExtensions;
import seava.lib.j4e.api.presenter.service.IDsService;
import seava.lib.j4e.presenter.service.AbstractPresenterBaseService;
import seava.mod.admin.presenter.generated.model.MenuRtLov_Ds;

public class ExtensionProviderMainMenu extends AbstractPresenterBaseService
		implements IExtensionContentProvider {

	@Override
	public String getContent(String targetName) throws ManagedException {
		if (IExtensions.UI_EXTJS_MAIN.equals(targetName)) {
			StringBuffer sb = new StringBuffer();
			if (!Session.user.get().isSystemUser()) {
				this.addNavigationTreeMenus(sb);
				this.addNavigationTopMenus(sb);
			}
			return sb.toString();
		}
		return null;
	}

	/**
	 * 
	 * @param sb
	 * @throws ManagedException
	 */
	protected void addNavigationTreeMenus(StringBuffer sb)
			throws ManagedException {
		IDsService<MenuRtLov_Ds, MenuRtLov_Ds, Object> srv = this
				.findDsService(MenuRtLov_Ds.class);
		MenuRtLov_Ds filter = new MenuRtLov_Ds();
		filter.setActive(true);
		filter.setTag("left");
		List<MenuRtLov_Ds> menus = srv.find(filter);
		int i = 0;
		sb.append("Main.navigationTreeMenus = [");
		for (MenuRtLov_Ds menu : menus) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append("{name:\"" + menu.getName() + "\", title:\""
					+ menu.getTitle() + "\"}");
			i++;
		}
		sb.append("];");
	}

	/**
	 * 
	 * @param sb
	 * @throws ManagedException
	 */
	protected void addNavigationTopMenus(StringBuffer sb)
			throws ManagedException {
		IDsService<MenuRtLov_Ds, MenuRtLov_Ds, Object> srv = this
				.findDsService(MenuRtLov_Ds.class);
		MenuRtLov_Ds filter = new MenuRtLov_Ds();
		filter.setActive(true);
		filter.setTag("top");
		List<MenuRtLov_Ds> menus = srv.find(filter);
		int i = 0;
		sb.append("Main.navigationTopMenus = [");
		for (MenuRtLov_Ds menu : menus) {
			if (i > 0) {
				sb.append(",");
			}
			sb.append("{name:\"" + menu.getName() + "\", title:\""
					+ menu.getTitle() + "\"}");
			i++;
		}
		sb.append("];");
	}

}
