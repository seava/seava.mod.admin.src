/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractType_Ds;
import seava.mod.admin.domain.generated.model.ViewState;

@Ds(entity=ViewState.class, sort={@SortField(field=ViewState_Ds.f_name)})
public class ViewState_Ds extends AbstractType_Ds<ViewState> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_ViewState_Ds";
	
	public static final String f_cmp = "cmp";
	public static final String f_cmpType = "cmpType";
	public static final String f_value = "value";
	
	@DsField
	private String cmp;
	
	@DsField
	private String cmpType;
	
	@DsField
	private String value;
	
	public ViewState_Ds() {
		super();
	}
	
	public ViewState_Ds(ViewState e) {
		super(e);
	}
	
	public String getCmp() {
	  return this.cmp;
	}
	
	public void setCmp(String cmp) {
	  this.cmp = cmp;
	}
	
	public String getCmpType() {
	  return this.cmpType;
	}
	
	public void setCmpType(String cmpType) {
	  this.cmpType = cmpType;
	}
	
	public String getValue() {
	  return this.value;
	}
	
	public void setValue(String value) {
	  this.value = value;
	}
}
