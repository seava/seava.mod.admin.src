package seava.mod.admin.presenter.extensions;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.presenter.extensions.IExtensionFile;
import seava.lib.j4e.api.presenter.extensions.IExtensionProvider;
import seava.lib.j4e.api.presenter.extensions.IExtensions;
import seava.lib.j4e.presenter.descriptor.extension.ExtensionFile;
import seava.lib.j4e.presenter.service.AbstractPresenterBaseService;
import seava.mod.admin.domain.generated.model.FrameExtension;

/**
 * Return extension files defined in database for the given target
 * 
 * @author amathe
 * 
 */
public class ExtensionProviderScriptFromDb extends AbstractPresenterBaseService
		implements IExtensionProvider {

	@Override
	public List<IExtensionFile> getFiles(String targetFrame)
			throws ManagedException {

		List<IExtensionFile> files = new ArrayList<IExtensionFile>();

		try {
			DataSource dataSource = this.getApplicationContext().getBean(
					DataSource.class);
			JdbcTemplate tpl = new JdbcTemplate(dataSource);
			tpl.queryForList("select 1 from " + FrameExtension.TABLE_NAME
					+ "  where 1=0");

		} catch (Exception e) {
			return files;
		}

		List<String> targets = new ArrayList<String>();
		targets.add(targetFrame);
		if (!targetFrame.matches(IExtensions.UI_EXTJS_MAIN)
				&& !targetFrame.matches(IExtensions.UI_EXTJS_DASHBOARD)) {
			targets.add(IExtensions.UI_EXTJS_FRAME);
		}

		List<FrameExtension> result = this
				.findEntityService(FrameExtension.class)
				.getEntityManager()
				.createQuery(
						" select e from "
								+ FrameExtension.ALIAS
								+ " e where "
								+ " e.clientId = :clientId and e.frame in :frame and e.active = true order by e.sequenceNo ",
						FrameExtension.class).setParameter("frame", targets)
				.setParameter("clientId", Session.user.get().getClientId())
				.getResultList();

		for (FrameExtension e : result) {
			files.add(new ExtensionFile(e.getFileLocation(), e
					.getRelativePath()));
		}
		return files;
	}

}
