/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCodeLov_Ds;
import seava.mod.admin.domain.generated.model.Role;

@Ds(entity=Role.class, sort={@SortField(field=RoleLov_Ds.f_code)})
public class RoleLov_Ds extends AbstractTypeWithCodeLov_Ds<Role> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_RoleLov_Ds";
	
	
	public RoleLov_Ds() {
		super();
	}
	
	public RoleLov_Ds(Role e) {
		super(e);
	}
}
