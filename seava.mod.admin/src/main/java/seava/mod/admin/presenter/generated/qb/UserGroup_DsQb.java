/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.qb;

import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.UserGroup_Ds;
import seava.mod.admin.presenter.generated.model.UserGroup_DsParam;
import seava.lib.j4e.api.base.session.Session;

public class UserGroup_DsQb extends QueryBuilderWithJpql<UserGroup_Ds,UserGroup_Ds, UserGroup_DsParam> {
  
  @Override
  public void beforeBuildWhere() {
    if (this.params != null && this.params.getWithUserId() != null && !"".equals(this.params.getWithUserId())) {
      addFilterCondition("  e.id in ( select p.id from ad_UserGroup p, IN (p.users) c where c.id = :withUserId )  "); 
      addCustomFilterItem("withUserId",this.params.getWithUserId());
    }
  }
}
