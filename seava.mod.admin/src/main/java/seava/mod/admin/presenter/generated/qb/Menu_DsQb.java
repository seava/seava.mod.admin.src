/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.qb;

import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.Menu_Ds;
import seava.mod.admin.presenter.generated.model.Menu_DsParam;
import seava.lib.j4e.api.base.session.Session;

public class Menu_DsQb extends QueryBuilderWithJpql<Menu_Ds,Menu_Ds, Menu_DsParam> {
  
  @Override
  public void beforeBuildWhere() {
    if (this.params != null && this.params.getWithRoleId() != null && !"".equals(this.params.getWithRoleId())) {
      addFilterCondition("  e.id in ( select p.id from  Menu p, IN (p.roles) c where c.id = :withRoleId )  "); 
      addCustomFilterItem("withRoleId",this.params.getWithRoleId());
    }
  }
}
