/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCodeLov_Ds;
import seava.mod.admin.domain.generated.model.UserGroup;

@Ds(entity=UserGroup.class, sort={@SortField(field=UserGroupLov_Ds.f_code)})
public class UserGroupLov_Ds extends AbstractTypeWithCodeLov_Ds<UserGroup> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_UserGroupLov_Ds";
	
	
	public UserGroupLov_Ds() {
		super();
	}
	
	public UserGroupLov_Ds(UserGroup e) {
		super(e);
	}
}
