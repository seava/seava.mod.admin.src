/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.qb;

import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.AccessControl_Ds;
import seava.mod.admin.presenter.generated.model.AccessControl_DsParam;
import seava.lib.j4e.api.base.session.Session;

public class AccessControl_DsQb extends QueryBuilderWithJpql<AccessControl_Ds,AccessControl_Ds, AccessControl_DsParam> {
  
  @Override
  public void beforeBuildWhere() {
    if (this.params != null && this.params.getWithRoleId() != null && !"".equals(this.params.getWithRoleId())) {
      addFilterCondition("  e.id in ( select p.id from  ad_AccessControl p, IN (p.roles) c where c.id = :withRoleId )  "); 
      addCustomFilterItem("withRoleId",this.params.getWithRoleId());
    }
  }
}
