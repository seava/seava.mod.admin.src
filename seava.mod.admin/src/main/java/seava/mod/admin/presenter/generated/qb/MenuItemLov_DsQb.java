/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.qb;

import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.MenuItemLov_Ds;
import seava.mod.admin.presenter.generated.model.MenuItemLov_DsParam;
import seava.lib.j4e.api.base.session.Session;

public class MenuItemLov_DsQb extends QueryBuilderWithJpql<MenuItemLov_Ds,MenuItemLov_Ds, MenuItemLov_DsParam> {
  
  @Override
  public void beforeBuildWhere() {
    if (this.params != null && this.params.getFoldersOnly() != null && !"".equals(this.params.getFoldersOnly())) {
      addFilterCondition("  (  (:foldersOnly = false  ) or (e.frame is null or e.frame = '') ) "); 
      addCustomFilterItem("foldersOnly",this.params.getFoldersOnly());
    }
    if (this.params != null && this.params.getFramesOnly() != null && !"".equals(this.params.getFramesOnly())) {
      addFilterCondition("  (  (:framesOnly = false  ) or (e.frame is not null ) ) "); 
      addCustomFilterItem("framesOnly",this.params.getFramesOnly());
    }
  }
}
