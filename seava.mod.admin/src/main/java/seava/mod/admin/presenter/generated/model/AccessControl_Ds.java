/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractType_Ds;
import seava.mod.admin.domain.generated.model.AccessControl;

@Ds(entity=AccessControl.class, sort={@SortField(field=AccessControl_Ds.f_name)})
public class AccessControl_Ds extends AbstractType_Ds<AccessControl> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_AccessControl_Ds";
	
	
	public AccessControl_Ds() {
		super();
	}
	
	public AccessControl_Ds(AccessControl e) {
		super(e);
	}
}
