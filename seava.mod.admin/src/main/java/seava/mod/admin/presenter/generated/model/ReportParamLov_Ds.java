/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeLov_Ds;
import seava.mod.admin.domain.generated.model.ReportParam;

@Ds(entity=ReportParam.class, sort={@SortField(field=ReportParamLov_Ds.f_name)})
public class ReportParamLov_Ds extends AbstractTypeLov_Ds<ReportParam> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_ReportParamLov_Ds";
	
	public static final String f_reportId = "reportId";
	public static final String f_title = "title";
	
	@DsField(join="left", path="report.id")
	private String reportId;
	
	@DsField
	private String title;
	
	public ReportParamLov_Ds() {
		super();
	}
	
	public ReportParamLov_Ds(ReportParam e) {
		super(e);
	}
	
	public String getReportId() {
	  return this.reportId;
	}
	
	public void setReportId(String reportId) {
	  this.reportId = reportId;
	}
	
	public String getTitle() {
	  return this.title;
	}
	
	public void setTitle(String title) {
	  this.title = title;
	}
}
