/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractAuditableLov_Ds;
import seava.mod.admin.domain.generated.model.DsReport;

@Ds(entity=DsReport.class, sort={@SortField(field=DsReportLov_Ds.f_dataSource), @SortField(field=DsReportLov_Ds.f_reportCode)})
public class DsReportLov_Ds extends AbstractAuditableLov_Ds<DsReport> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_DsReportLov_Ds";
	
	public static final String f_reportId = "reportId";
	public static final String f_reportCode = "reportCode";
	public static final String f_dataSource = "dataSource";
	
	@DsField(join="left", path="report.id")
	private String reportId;
	
	@DsField(join="left", path="report.code")
	private String reportCode;
	
	@DsField
	private String dataSource;
	
	public DsReportLov_Ds() {
		super();
	}
	
	public DsReportLov_Ds(DsReport e) {
		super(e);
	}
	
	public String getReportId() {
	  return this.reportId;
	}
	
	public void setReportId(String reportId) {
	  this.reportId = reportId;
	}
	
	public String getReportCode() {
	  return this.reportCode;
	}
	
	public void setReportCode(String reportCode) {
	  this.reportCode = reportCode;
	}
	
	public String getDataSource() {
	  return this.dataSource;
	}
	
	public void setDataSource(String dataSource) {
	  this.dataSource = dataSource;
	}
}
