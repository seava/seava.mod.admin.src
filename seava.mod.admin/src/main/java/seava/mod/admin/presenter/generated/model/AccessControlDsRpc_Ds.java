/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.Param;
import seava.lib.j4e.api.presenter.annotation.RefLookup;
import seava.lib.j4e.api.presenter.annotation.RefLookups;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractAuditable_Ds;
import seava.mod.admin.domain.generated.model.AccessControl;
import seava.mod.admin.domain.generated.model.AccessControlDsRpc;

@Ds(entity=AccessControlDsRpc.class, sort={@SortField(field=AccessControlDsRpc_Ds.f_dsName), @SortField(field=AccessControlDsRpc_Ds.f_serviceMethod)})
@RefLookups({
	@RefLookup(refId = AccessControlDsRpc_Ds.f_accessControlId, namedQuery = AccessControl.NQ_FIND_BY_NAME, params = {
		@Param(name = "name", field = AccessControlDsRpc_Ds.f_accessControl)
	})
})
public class AccessControlDsRpc_Ds extends AbstractAuditable_Ds<AccessControlDsRpc> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_AccessControlDsRpc_Ds";
	
	public static final String f_dsName = "dsName";
	public static final String f_serviceMethod = "serviceMethod";
	public static final String f_accessControlId = "accessControlId";
	public static final String f_accessControl = "accessControl";
	
	@DsField
	private String dsName;
	
	@DsField
	private String serviceMethod;
	
	@DsField(join="left", path="accessControl.id")
	private String accessControlId;
	
	@DsField(join="left", path="accessControl.name")
	private String accessControl;
	
	public AccessControlDsRpc_Ds() {
		super();
	}
	
	public AccessControlDsRpc_Ds(AccessControlDsRpc e) {
		super(e);
	}
	
	public String getDsName() {
	  return this.dsName;
	}
	
	public void setDsName(String dsName) {
	  this.dsName = dsName;
	}
	
	public String getServiceMethod() {
	  return this.serviceMethod;
	}
	
	public void setServiceMethod(String serviceMethod) {
	  this.serviceMethod = serviceMethod;
	}
	
	public String getAccessControlId() {
	  return this.accessControlId;
	}
	
	public void setAccessControlId(String accessControlId) {
	  this.accessControlId = accessControlId;
	}
	
	public String getAccessControl() {
	  return this.accessControl;
	}
	
	public void setAccessControl(String accessControl) {
	  this.accessControl = accessControl;
	}
}
