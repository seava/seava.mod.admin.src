/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeLov_Ds;
import seava.mod.admin.domain.generated.model.AttachmentType;

@Ds(entity=AttachmentType.class, sort={@SortField(field=AttachmentTypeLov_Ds.f_name)})
public class AttachmentTypeLov_Ds extends AbstractTypeLov_Ds<AttachmentType> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_AttachmentTypeLov_Ds";
	
	public static final String f_category = "category";
	public static final String f_targetAlias = "targetAlias";
	public static final String f_targetType = "targetType";
	
	@DsField
	private String category;
	
	@DsField(fetch=false, jpqlFilter="  e.refid in (select t.sourceRefId from ad_TargetRule t where  t.targetAlias = :targetAlias and  t.targetType = :targetType and t.clientId = :clientId ) ")
	private String targetAlias;
	
	@DsField(fetch=false)
	private String targetType;
	
	public AttachmentTypeLov_Ds() {
		super();
	}
	
	public AttachmentTypeLov_Ds(AttachmentType e) {
		super(e);
	}
	
	public String getCategory() {
	  return this.category;
	}
	
	public void setCategory(String category) {
	  this.category = category;
	}
	
	public String getTargetAlias() {
	  return this.targetAlias;
	}
	
	public void setTargetAlias(String targetAlias) {
	  this.targetAlias = targetAlias;
	}
	
	public String getTargetType() {
	  return this.targetType;
	}
	
	public void setTargetType(String targetType) {
	  this.targetType = targetType;
	}
}
