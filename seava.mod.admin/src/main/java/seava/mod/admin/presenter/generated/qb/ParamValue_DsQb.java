/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.qb;

import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.ParamValue_Ds;
import seava.mod.admin.presenter.generated.model.ParamValue_DsParam;
import seava.lib.j4e.api.base.session.Session;

public class ParamValue_DsQb extends QueryBuilderWithJpql<ParamValue_Ds,ParamValue_Ds, ParamValue_DsParam> {
  
  @Override
  public void beforeBuildWhere() {
    if (this.params != null && this.params.getValidAt() != null && !"".equals(this.params.getValidAt())) {
      addFilterCondition("  :validAt between e.validFrom and e.validTo "); 
      addCustomFilterItem("validAt",this.params.getValidAt());
    }
  }
}
