/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.extensions.qb;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.ViewStateRtLov_Ds;
import seava.mod.admin.presenter.generated.model.ViewStateRtLov_DsParam;

public class ViewStateRtLov_DsQb
		extends
		QueryBuilderWithJpql<ViewStateRtLov_Ds, ViewStateRtLov_Ds, ViewStateRtLov_DsParam> {

	@Override
	protected void beforeBuildWhere() throws ManagedException {
		String userCode = Session.user.get().getCode();

		if (this.params.getHideMine() != null
				&& this.params.getHideMine() == true) {
			addFilterCondition("  e.createdBy <> :userCode ");
			addCustomFilterItem("userCode", userCode);
		}

		if (this.params.getHideOthers() != null
				&& this.params.getHideOthers() == true) {
			addFilterCondition("  e.createdBy = :userCode ");
			addCustomFilterItem("userCode", userCode);
		}
	}

}
