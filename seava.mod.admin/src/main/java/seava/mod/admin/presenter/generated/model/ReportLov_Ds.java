/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeWithCodeLov_Ds;
import seava.mod.admin.domain.generated.model.Report;

@Ds(entity=Report.class, sort={@SortField(field=ReportLov_Ds.f_name)})
public class ReportLov_Ds extends AbstractTypeWithCodeLov_Ds<Report> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_ReportLov_Ds";
	
	
	public ReportLov_Ds() {
		super();
	}
	
	public ReportLov_Ds(Report e) {
		super(e);
	}
}
