/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.extensions.delegate;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.business.service.IEntityService;
import seava.lib.j4e.presenter.service.AbstractPresenterDelegate;
import seava.mod.admin.domain.generated.model.ParamValue;
import seava.mod.admin.presenter.generated.model.MyParam_Ds;
import seava.mod.admin.presenter.generated.model.MyParam_DsParam;
import seava.mod.admin.presenter.generated.model.ParamValue_Ds;
import seava.mod.system.domain.generated.model.Param;

public class MyParam_Pd extends AbstractPresenterDelegate {

	/**
	 * Update runtime system parameter values .
	 * 
	 * @param filter
	 * @throws Exception
	 */
	public void publishValues(ParamValue_Ds filter) throws Exception {
		this.getSettings().reloadParamValues();
	}

	/**
	 * Create initial values for the given parameters. Value is set to the
	 * parameter default value. Ignores parameters which already have at least
	 * one value set.
	 * 
	 * @param list
	 * @param params
	 * @throws Exception
	 */
	public void createInitialValues(List<MyParam_Ds> list,
			MyParam_DsParam params) throws Exception {

		List<String> codes = new ArrayList<String>();
		for (MyParam_Ds ds : list) {
			codes.add(ds.getCode());
		}

		IEntityService<ParamValue> srv = (IEntityService<ParamValue>) this
				.findEntityService(ParamValue.class);

		List<Param> paramsList = srv
				.getEntityManager()
				.createQuery(
						"select e from "
								+ Param.ALIAS
								+ " e where e.code in :codes and e.code not in (select distinct v.sysParam from "
								+ ParamValue.ALIAS
								+ " v where v.clientId = :clientId ) ",
						Param.class)
				.setParameter("clientId", Session.user.get().getClientId())
				.setParameter("codes", codes).getResultList();
		List<ParamValue> values = new ArrayList<ParamValue>();
		for (Param p : paramsList) {
			ParamValue pv = new ParamValue();
			pv.setSysParam(p.getCode());
			pv.setValue(p.getDefaultValue());
			pv.setValidFrom(params.getValidFrom());
			pv.setValidTo(params.getValidTo());

			if (pv.getValidFrom() == null) {
				pv.setValidFrom(new Date());
			}
			if (pv.getValidTo() == null) {
				pv.setValidTo(new Date(199, 1, 1));
			}
			values.add(pv);
		}
		srv.insert(values);
	}
}
