/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.model;

import seava.lib.j4e.api.base.descriptor.IModelWithClientId;
import seava.lib.j4e.api.base.descriptor.IModelWithId;
import seava.lib.j4e.api.presenter.annotation.Ds;
import seava.lib.j4e.api.presenter.annotation.DsField;
import seava.lib.j4e.api.presenter.annotation.SortField;
import seava.mod.abstracts.presenter.generated.model.AbstractTypeLov_Ds;
import seava.mod.admin.domain.generated.model.Menu;

@Ds(entity=Menu.class, sort={@SortField(field=MenuLov_Ds.f_name)})
public class MenuLov_Ds extends AbstractTypeLov_Ds<Menu> implements IModelWithId<String>,  IModelWithClientId {
	
	public static final String ALIAS = "ad_MenuLov_Ds";
	
	public static final String f_title = "title";
	
	@DsField
	private String title;
	
	public MenuLov_Ds() {
		super();
	}
	
	public MenuLov_Ds(Menu e) {
		super(e);
	}
	
	public String getTitle() {
	  return this.title;
	}
	
	public void setTitle(String title) {
	  this.title = title;
	}
}
