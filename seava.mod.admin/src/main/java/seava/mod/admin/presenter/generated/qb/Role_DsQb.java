/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.generated.qb;

import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.presenter.generated.model.Role_Ds;
import seava.mod.admin.presenter.generated.model.Role_DsParam;
import seava.lib.j4e.api.base.session.Session;

public class Role_DsQb extends QueryBuilderWithJpql<Role_Ds,Role_Ds, Role_DsParam> {
  
  @Override
  public void beforeBuildWhere() {
    if (this.params != null && this.params.getWithUserId() != null && !"".equals(this.params.getWithUserId())) {
      addFilterCondition("  e.id in ( select p.id from ad_Role p, IN (p.users) c where c.id = :withUserId )  "); 
      addCustomFilterItem("withUserId",this.params.getWithUserId());
    }
    if (this.params != null && this.params.getWithPrivilegeId() != null && !"".equals(this.params.getWithPrivilegeId())) {
      addFilterCondition("  e.id in ( select p.id from ad_Role p, IN (p.accessControls) c where c.id = :withPrivilegeId )  "); 
      addCustomFilterItem("withPrivilegeId",this.params.getWithPrivilegeId());
    }
  }
}
