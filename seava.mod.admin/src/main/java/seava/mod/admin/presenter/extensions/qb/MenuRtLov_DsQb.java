/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.presenter.extensions.qb;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.presenter.descriptor.query.QueryBuilderWithJpql;
import seava.mod.admin.domain.generated.model.Menu;
import seava.mod.admin.presenter.generated.model.MenuRtLov_Ds;

public class MenuRtLov_DsQb extends
		QueryBuilderWithJpql<MenuRtLov_Ds, MenuRtLov_Ds, Object> {

	@Override
	protected void beforeBuildWhere() throws ManagedException {
		if (!Session.user.get().getProfile().isAdministrator()) {
			addFilterCondition("  e.id in ( select p.id from  " + Menu.ALIAS
					+ " p, IN (p.roles) c where c.code in :pRoles )  ");
			this.addCustomFilterItem("pRoles", Session.user.get().getProfile()
					.getRoles());
		}
	}

}
