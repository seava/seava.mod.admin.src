/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.security.authoriz;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.web.security.IAuthorization;
import seava.mod.admin.security.AbstractSecurity;

public class AuthorizationForJob extends AbstractSecurity implements
		IAuthorization {

	@Override
	public void authorize(String resourceName, String action, String rpcName)
			throws ManagedException {
		// If it doesn't throw exception is authorized
	}

}
