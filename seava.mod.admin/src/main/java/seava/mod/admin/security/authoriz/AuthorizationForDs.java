/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.security.authoriz;

import java.util.List;

import javax.persistence.TypedQuery;

import seava.lib.j4e.api.base.exceptions.ManagedException;
import seava.lib.j4e.api.base.session.IUser;
import seava.lib.j4e.api.base.session.Session;
import seava.lib.j4e.api.web.security.IAuthorization;
import seava.lib.j4e.base.exceptions.ErrorCodeCommons;
import seava.lib.j4e.web.ConstantsWeb;
import seava.mod.admin.domain.generated.model.AccessControl;
import seava.mod.admin.domain.generated.model.AccessControlDs;
import seava.mod.admin.domain.generated.model.AccessControlDsRpc;
import seava.mod.admin.security.AbstractSecurity;

public class AuthorizationForDs extends AbstractSecurity implements
		IAuthorization {

	public void authorize(String dsName, String action, String rpcName)
			throws ManagedException {

		IUser user = Session.user.get();

		if (user.isSystemUser() || user.getProfile().isAdministrator()) {
			return;
		}

		String filter = null;
		String eql = null;

		if (action.equals(ConstantsWeb.DS_ACTION_RPC)) {
			filter = " and e.serviceMethod = :serviceMethod ";
			eql = "select 1 from " + AccessControlDsRpc.class.getSimpleName()
					+ " e where e.clientId = :clientId "
					+ " and e.dsName = :dsName " + filter
					+ " and e.accessControl.id in ( select p.id from "
					+ AccessControl.class.getSimpleName()
					+ " p, IN (p.roles) c where c.code in :roles ) ";
		} else {
			filter = this.actionFilter(dsName, action);
			eql = "select 1 from " + AccessControlDs.class.getSimpleName()
					+ " e where e.clientId = :clientId "
					+ " and e.dsName = :dsName " + filter
					+ " and e.accessControl.id in ( select p.id from "
					+ AccessControl.class.getSimpleName()
					+ " p, IN (p.roles) c where c.code in :roles ) ";
		}

		TypedQuery<Integer> query = this.getEntityManager()
				.createQuery(eql, Integer.class).setParameter("dsName", dsName)
				.setParameter("clientId", Session.user.get().getClientId())
				.setParameter("roles", user.getProfile().getRoles());

		if (action.equals(ConstantsWeb.DS_ACTION_RPC)) {
			query.setParameter("serviceMethod", rpcName);
		}

		List<Integer> result = query.getResultList();
		if (result.size() < 1) {

			throw new ManagedException(ErrorCodeCommons.OPERATION_NOT_ALLOWED,
					"You are not authorized to execute `"
							+ ((rpcName != null) ? rpcName : action)
							+ "`. <BR> Not enough privileges on resource `"
							+ dsName + "`");
		}
	}

	private String actionFilter(String dsName, String action) {

		if (action.equals(ConstantsWeb.DS_ACTION_QUERY)) {
			return " and e.queryAllowed = true ";
		} else if (action.equals(ConstantsWeb.DS_ACTION_EXPORT)) {
			return " and e.exportAllowed = true ";
		} else if (action.equals(ConstantsWeb.DS_ACTION_IMPORT)) {
			return " and e.importAllowed = true ";
		} else if (action.equals(ConstantsWeb.DS_ACTION_INSERT)) {
			return " and e.insertAllowed = true ";
		} else if (action.equals(ConstantsWeb.DS_ACTION_UPDATE)) {
			return " and e.updateAllowed = true ";
		} else if (action.equals(ConstantsWeb.DS_ACTION_DELETE)) {
			return " and e.deleteAllowed = true ";
		}
		return "";
	}

}
