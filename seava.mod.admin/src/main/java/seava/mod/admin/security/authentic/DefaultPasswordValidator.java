/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.security.authentic;

import seava.lib.j4e.api.web.security.IPasswordValidator;

/**
 * Simple password validator. Extend it according to your needs or use a a third
 * party password validator.
 * 
 * @author amathe
 * 
 */
public class DefaultPasswordValidator implements IPasswordValidator {

	/**
	 * 
	 */
	@Override
	public void validate(String passwordToValidate) throws Exception {
		if (passwordToValidate == null || passwordToValidate.equals("")) {
			throw new Exception("Empty password not allowed");
		}
		if (passwordToValidate.length() < 4) {
			throw new Exception("Password must be at least 4 characters long.");
		}

	}

}
