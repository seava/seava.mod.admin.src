/** 
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
package seava.mod.admin.security.authoriz;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import seava.lib.j4e.api.web.security.IAuthorization;
import seava.lib.j4e.api.web.security.IAuthorizationFactory;

public class AuthorizationFactory implements IAuthorizationFactory,
		ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Override
	public IAuthorization getAsgnAuthorizationProvider() {
		return (IAuthorization) this.applicationContext
				.getBean(AuthorizationForAsgn.class);
	}

	@Override
	public IAuthorization getDsAuthorizationProvider() {
		return (IAuthorization) this.applicationContext
				.getBean(AuthorizationForDs.class);
	}

	@Override
	public IAuthorization getJobAuthorizationProvider() {
		return (IAuthorization) this.applicationContext
				.getBean(AuthorizationForJob.class);
	}

	public ApplicationContext getApplicationContext() {
		return applicationContext;
	}

	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}

}
