/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.MyClient_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("client", Ext.create(seava.mod.admin.ui.extjs.generated.dc.MyClient_Dc,{}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addDcView("client", {name:"clientEdit", xtype:"admin_MyClient_Dc$Edit"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["clientEdit"],["center"])
      .addToolbarTo("main", "clientEditTlb")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("clientEditTlb", {dc: "client"})
        .addTitle().addSeparator().addSeparator()
        .addSave().addCancel()
        .addReports()
      .end();
  },
  
  _afterDefineDcs_: function() {
    this._getDc_("client").doQuery();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.MyClient_Ui"
});
