Ext.define("seava.mod.admin.i18n.extjs.en.generated.frame.ReportsRt_Ui", {
  /* view */
  repFilter__ttl: "Filter",
  wdwParams__ttl: "Parameters",
  /* menu */
  tlbRepList__ttl: "Reports",
  /* button */
  btnCancelReport__lbl: "Cancel",
  btnCancelReport__tlp: "Cancel",
  btnRunReport__lbl: "Run report",
  btnRunReport__tlp: "Run report",
  btnShowParamsWdw__lbl: "Run report",
  btnShowParamsWdw__tlp: "Show parameter form to run report",
  
  title: "Runtime center"
});
