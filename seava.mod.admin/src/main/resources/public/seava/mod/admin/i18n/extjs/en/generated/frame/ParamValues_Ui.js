Ext.define("seava.mod.admin.i18n.extjs.en.generated.frame.ParamValues_Ui", {
  /* view */
  valFilter__ttl: "Filter",
  /* menu */
  tlbValList__ttl: "Parameter values",
  /* button */
  btnPublish__lbl: "Publish changes",
  btnPublish__tlp: "Publish the changes .",
  btnShowParamsFrame__lbl: "Open parameters frame",
  btnShowParamsFrame__tlp: "Show the parameters frame",
  
  title: "Parameter values"
});
