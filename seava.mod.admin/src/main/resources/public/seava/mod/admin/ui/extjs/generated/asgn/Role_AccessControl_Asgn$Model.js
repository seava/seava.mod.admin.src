/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.asgn.Role_AccessControl_Asgn$Model", {
  extend: 'Ext.data.Model',
  statics: {
    ALIAS: "ad_Role_AccessControl_Asgn"
  },
  fields:  [
    {name:"id", type:"string"},
    {name:"name", type:"string"},
    {name:"description", type:"string"}
  ]
});
