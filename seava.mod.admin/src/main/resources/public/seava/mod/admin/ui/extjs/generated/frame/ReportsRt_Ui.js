/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.ReportsRt_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("rep", Ext.create(seava.mod.admin.ui.extjs.generated.dc.ReportRt_Dc,{ autoLoad:true}))
      .addDc("param", Ext.create(seava.mod.admin.ui.extjs.generated.dc.ReportParamRt_Dc,{multiEdit: true}))
      .linkDc("param", "rep",{
        fields:[{childField:"reportId", parentField:"id"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnShowParamsWdw", disabled:false, handler: this.onBtnShowParamsWdw, scope:this})
      .addButton({name:"btnRunReport", disabled:false, handler: this.onBtnRunReport, scope:this})
      .addButton({name:"btnCancelReport", disabled:false, handler: this.onBtnCancelReport, scope:this})
      .addDcView("rep", {name:"repFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_ReportRt_Dc$FilterV"})
      .addDcView("rep", {name:"repList", width:550, xtype:"admin_ReportRt_Dc$List"})
      .addDcView("param", {name:"paramList", xtype:"admin_ReportParamRt_Dc$EditList"})
      .addWindow({name:"wdwParams", _hasTitle_:true, width:450, height:300,  closable:false, 
          dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
            items:[ this._elems_.get("btnRunReport"), this._elems_.get("btnCancelReport")]}], closeAction:'hide', resizable:true, layout:"fit", modal:true,
        items:[this._elems_.get("paramList")]})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["repFilter", "repList"],["west", "center"])
      .addToolbarTo("main", "tlbRepList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbRepList", {dc: "rep"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnShowParamsWdw") ])
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnShowParamsWdw
   */
  onBtnShowParamsWdw: function() {
    this._getDc_("param").doQuery();
    this._getWindow_("wdwParams").show();
  },
  
  /**
   * On-Click handler for button btnRunReport
   */
  onBtnRunReport: function() {
    this.runReport();
    this._getWindow_("wdwParams").close();
  },
  
  /**
   * On-Click handler for button btnCancelReport
   */
  onBtnCancelReport: function() {
    this._getDc_("param").doCancel();
    this._getWindow_("wdwParams").close();
  },
  
  runReport: function() {
    
    var rep = this._getDc_("rep").record.data;
    var paramDc = this._getDc_("param");
    
    if (!rep.queryBuilderClass) {
    	rep.queryBuilderClass = "e4e.dc.tools.DcReport";
    }
    
    var b = Ext.ClassManager
    		.isCreated(rep.queryBuilderClass);
    if (!b) {
    	Main.error("Query builder class `"
    			+ rep.queryBuilderClass + "` not found. Invalid report server configuration.");
    	return;
    }
    var dcReport = Ext.create(rep.queryBuilderClass); 
    var params = []
    
    paramDc.store.each(function(r) {
    	//code, name, type, lov, value, mandatory, noEdit
    	params[params.length] = {
    		code:  r.data.name,
    		name:  r.data.title,
    		type:  r.data.dataType,
    		lov:  r.data.listOfValues,
    		value:  r.data.value,
    		mandatory:  r.data.mandatory,
    		noEdit:  r.data.noEdit
    	}
    }); 
     
    dcReport.run({
    	url : rep.serverUrl,
    	contextPath : rep.contextPath,
    	params : params
    });
    
    paramDc.store.commitChanges();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.ReportsRt_Ui"
});
