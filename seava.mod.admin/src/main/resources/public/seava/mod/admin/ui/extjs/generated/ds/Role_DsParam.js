/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.Role_DsParam", {
  extend: 'Ext.data.Model',
  
  fields: [
    {name:"withPrivilege", type:"string", forFilter:true},
    {name:"withPrivilegeId", type:"string", forFilter:true},
    {name:"withUser", type:"string", forFilter:true},
    {name:"withUserId", type:"string", forFilter:true}
  ]
});
