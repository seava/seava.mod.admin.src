/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.MyParam_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"code", bind: "{d.code}", xtype:"seava.mod.system.ui.extjs.generated.lov.Params_Lov"})
      .addLov({name:"name", bind: "{d.name}", xtype:"seava.mod.system.ui.extjs.generated.lov.ParamNames_Lov"})
      .addTextField({ name:"defaultValue", bind: "{d.defaultValue}", maxLength: 4000})
      .addTextField({ name:"description", bind: "{d.description}", maxLength: 4000})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:60}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["code", "name", "description", "defaultValue", "active" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_MyParam_Dc$FilterV"
});
