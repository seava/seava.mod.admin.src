/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.Roles_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("rol", Ext.create(seava.mod.admin.ui.extjs.generated.dc.Role_Dc,{multiEdit: true,  autoLoad:true}))
      .addDc("usr", Ext.create(seava.mod.admin.ui.extjs.generated.dc.User_Dc,{}))
      .addDc("acl", Ext.create(seava.mod.admin.ui.extjs.generated.dc.AccessControl_Dc,{}))
      .addDc("menu", Ext.create(seava.mod.admin.ui.extjs.generated.dc.Menu_Dc,{}))
      .addDc("mi", Ext.create(seava.mod.admin.ui.extjs.generated.dc.MenuItem_Dc,{}))
      .linkDc("usr", "rol",{
        fields:[{childParam:"withRoleId", parentField:"id"}]})
      .linkDc("acl", "rol",{
        fields:[{childParam:"withRoleId", parentField:"id"}]})
      .linkDc("menu", "rol",{
        fields:[{childParam:"withRoleId", parentField:"id"}]})
      .linkDc("mi", "rol",{
        fields:[{childParam:"withRoleId", parentField:"id"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnAsgnRoleToUsers", disabled:true, handler: this.onBtnAsgnRoleToUsers,
          stateManager:{ name:"selected_one_clean", dc:"rol" }, scope:this})
      .addButton({name:"btnAsgnRoleToAccessCtrl", disabled:true, handler: this.onBtnAsgnRoleToAccessCtrl,
          stateManager:{ name:"selected_one_clean", dc:"rol" }, scope:this})
      .addButton({name:"btnAsgnRoleToMenuItem", disabled:true, handler: this.onBtnAsgnRoleToMenuItem,
          stateManager:{ name:"selected_one_clean", dc:"rol" }, scope:this})
      .addButton({name:"btnAsgnRoleToMenu", disabled:true, handler: this.onBtnAsgnRoleToMenu,
          stateManager:{ name:"selected_one_clean", dc:"rol" }, scope:this})
      .addButton({name:"btnDetailsUser", disabled:true, handler: this.onBtnDetailsUser,
          stateManager:{ name:"selected_one", dc:"usr" }, scope:this})
      .addButton({name:"btnDetailsPrivilege", disabled:true, handler: this.onBtnDetailsPrivilege,
          stateManager:{ name:"selected_one", dc:"acl" }, scope:this})
      .addDcView("rol", {name:"rolFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_Role_Dc$FilterV"})
      .addDcView("rol", {name:"rolList", xtype:"admin_Role_Dc$EditList"})
      .addDcView("usr", {name:"usrList", _hasTitle_:true, xtype:"admin_User_Dc$ListShort"})
      .addDcView("acl", {name:"aclList", _hasTitle_:true, xtype:"admin_AccessControl_Dc$List"})
      .addDcView("menu", {name:"menuList", _hasTitle_:true, xtype:"admin_Menu_Dc$List"})
      .addDcView("mi", {name:"miList", _hasTitle_:true, xtype:"admin_MenuItem_Dc$ShortList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:2,center:1,south:1}}, defaults:{split:true}})
      .addPanel({name:"detailsTab", height:280, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["rolFilter", "rolList", "detailsTab"],["west", "center", "south"])
      .addChildrenTo("detailsTab", ["usrList", "aclList", "menuList", "miList"])
      .addToolbarTo("main", "tlbRolList")
      .addToolbarTo("usrList", "tlbUsrList")
      .addToolbarTo("aclList", "tlbAclList")
      .addToolbarTo("menuList", "tlbMenuList")
      .addToolbarTo("miList", "tlbMiList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbRolList", {dc: "rol"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addReports()
      .end().beginToolbar("tlbUsrList", {dc: "usr"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnAsgnRoleToUsers") ,this._elems_.get("btnDetailsUser") ])
        .addReports()
      .end().beginToolbar("tlbAclList", {dc: "acl"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnAsgnRoleToAccessCtrl") ,this._elems_.get("btnDetailsPrivilege") ])
        .addReports()
      .end().beginToolbar("tlbMenuList", {dc: "menu"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnAsgnRoleToMenu") ])
        .addReports()
      .end().beginToolbar("tlbMiList", {dc: "mi"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnAsgnRoleToMenuItem") ])
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnAsgnRoleToUsers
   */
  onBtnAsgnRoleToUsers: function() {
    this._showAsgnWindow_("seava.mod.admin.ui.extjs.generated.asgn.Role_User_Asgn$Window" ,{dc: "rol", objectIdField: "id"
    ,listeners: {close: {scope: this, fn: function() { this._getDc_("usr").doQuery();
    }} }});
  },
  
  /**
   * On-Click handler for button btnAsgnRoleToAccessCtrl
   */
  onBtnAsgnRoleToAccessCtrl: function() {
    this._showAsgnWindow_("seava.mod.admin.ui.extjs.generated.asgn.Role_AccessControl_Asgn$Window" ,{dc: "rol", objectIdField: "id"
    ,listeners: {close: {scope: this, fn: function() { this._getDc_("acl").doQuery();
    }} }});
  },
  
  /**
   * On-Click handler for button btnAsgnRoleToMenuItem
   */
  onBtnAsgnRoleToMenuItem: function() {
    this._showAsgnWindow_("seava.mod.admin.ui.extjs.generated.asgn.Role_MenuItem_Asgn$Window" ,{dc: "rol", objectIdField: "id"
    ,listeners: {close: {scope: this, fn: function() { this._getDc_("mi").doQuery();
    }} }});
  },
  
  /**
   * On-Click handler for button btnAsgnRoleToMenu
   */
  onBtnAsgnRoleToMenu: function() {
    this._showAsgnWindow_("seava.mod.admin.ui.extjs.generated.asgn.Role_Menu_Asgn$Window" ,{dc: "rol", objectIdField: "id"
    ,listeners: {close: {scope: this, fn: function() { this._getDc_("menu").doQuery();
    }} }});
  },
  
  /**
   * On-Click handler for button btnDetailsUser
   */
  onBtnDetailsUser: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.Users_Ui",{
      params: {
        id: this._getDc_("usr").getRecord().get("id"),
        code: this._getDc_("usr").getRecord().get("code")
      },
      callback: function (params) {
        this._when_called_for_details(params);
      }
    });
  },
  
  /**
   * On-Click handler for button btnDetailsPrivilege
   */
  onBtnDetailsPrivilege: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.AccessControls_Ui",{
      params: {
        id: this._getDc_("acl").getRecord().get("id"),
        name: this._getDc_("acl").getRecord().get("name")
      },
      callback: function (params) {
        this._when_called_for_details(params);
      }
    });
  },
  
  _when_called_for_details: function(params) {
    var dc = this._getDc_("rol");
    dc.setFilterValue("id", params.id);
    dc.setFilterValue("code", params.code);
    dc.doQuery();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.Roles_Ui"
});
