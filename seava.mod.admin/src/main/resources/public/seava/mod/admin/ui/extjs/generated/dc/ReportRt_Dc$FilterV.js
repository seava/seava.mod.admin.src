/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.ReportRt_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"code", bind: "{d.code}", xtype:"seava.mod.admin.ui.extjs.generated.lov.Reports_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addLov({name:"name", bind: "{d.name}", xtype:"seava.mod.admin.ui.extjs.generated.lov.ReportsName_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addLov({name:"reportServer", bind: "{d.reportServer}", xtype:"seava.mod.admin.ui.extjs.generated.lov.ReportServers_Lov",
        retFieldMapping: [{lovField:"id", dsField: "reportServerId"} ]})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:60}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["code", "name", "reportServer" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_ReportRt_Dc$FilterV"
});
