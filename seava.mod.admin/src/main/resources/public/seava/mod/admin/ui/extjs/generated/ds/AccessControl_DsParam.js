/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.AccessControl_DsParam", {
  extend: 'Ext.data.Model',
  
  fields: [
    {name:"copyFrom", type:"string", forFilter:true},
    {name:"copyFromId", type:"string", forFilter:true},
    {name:"resetRules", type:"boolean", forFilter:true},
    {name:"skipAsgn", type:"boolean", forFilter:true},
    {name:"skipDs", type:"boolean", forFilter:true},
    {name:"withRole", type:"string", forFilter:true},
    {name:"withRoleId", type:"string", forFilter:true}
  ]
});
