/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.UserGroups_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("group", Ext.create(seava.mod.admin.ui.extjs.generated.dc.UserGroup_Dc,{multiEdit: true}))
      .addDc("usr", Ext.create(seava.mod.admin.ui.extjs.generated.dc.User_Dc,{}))
      .linkDc("usr", "group",{fetchMode:"auto",
        fields:[{childParam:"inGroupId", parentField:"id"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnAsgnUsers", disabled:true, handler: this.onBtnAsgnUsers,
          stateManager:{ name:"selected_one_clean", dc:"group" }, scope:this})
      .addButton({name:"btnDetailsUser", disabled:true, handler: this.onBtnDetailsUser,
          stateManager:{ name:"selected_one", dc:"usr" }, scope:this})
      .addDcView("group", {name:"filterGroup", _hasTitle_:true, width:250,  collapsible:true, xtype:"admin_UserGroup_Dc$FilterPG"})
      .addDcView("group", {name:"listGroup", xtype:"admin_UserGroup_Dc$EditList"})
      .addDcView("usr", {name:"usrList", height:280, xtype:"admin_User_Dc$ListShort"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:2,center:0,south:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["filterGroup", "listGroup", "usrList"],["west", "center", "south"])
      .addToolbarTo("main", "tlbGroupEditList")
      .addToolbarTo("usrList", "tlbUsrList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbGroupEditList", {dc: "group"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addReports()
      .end().beginToolbar("tlbUsrList", {dc: "usr"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnAsgnUsers") ,this._elems_.get("btnDetailsUser") ])
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnAsgnUsers
   */
  onBtnAsgnUsers: function() {
    this._showAsgnWindow_("seava.mod.admin.ui.extjs.generated.asgn.UserGroup_User_Asgn$Window" ,{dc: "group", objectIdField: "id"
    ,listeners: {close: {scope: this, fn: function() { this._getDc_("usr").doReloadPage();
    }} }});
  },
  
  /**
   * On-Click handler for button btnDetailsUser
   */
  onBtnDetailsUser: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.Users_Ui",{
      params: {
        id: this._getDc_("usr").getRecord().get("id"),
        code: this._getDc_("usr").getRecord().get("code")
      },
      callback: function (params) {
        this._when_called_for_details(params);
      }
    });
  },
  
  _when_called_for_details: function(params) {
    
    var dc = this._getDc_("group");
    dc.setFilterValue("id", params.id);
    dc.setFilterValue("code", params.code);
    dc.doQuery();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.UserGroups_Ui"
});
