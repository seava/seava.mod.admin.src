/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.User_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextField({ name:"loginName", bind: "{d.loginName}", maxLength: 255})
      .addLov({name:"name", bind: "{d.name}", xtype:"seava.mod.admin.ui.extjs.generated.lov.UserNames_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addLov({name:"code", bind: "{d.code}", xtype:"seava.mod.admin.ui.extjs.generated.lov.Users_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addLov({name:"withRole", paramIndex:"withRole", bind: "{p.withRole}", xtype:"seava.mod.admin.ui.extjs.generated.lov.Roles_Lov",
        retFieldMapping: [{lovField:"id", dsParam: "withRoleId"} ]})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      .addBooleanField({ name:"locked", bind: "{d.locked}"})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:60}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["name", "code", "loginName", "withRole", "active", "locked" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_User_Dc$FilterV"
});
