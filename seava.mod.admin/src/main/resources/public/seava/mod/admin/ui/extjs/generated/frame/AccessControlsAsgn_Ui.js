/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.AccessControlsAsgn_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("asgnAccess", Ext.create(seava.mod.admin.ui.extjs.generated.dc.AccessControlAsgn_Dc,{multiEdit: true,  autoLoad:true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addDcView("asgnAccess", {name:"asgnAccessFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_AccessControlAsgn_Dc$FilterV"})
      .addDcView("asgnAccess", {name:"asgnAccessEditList", xtype:"admin_AccessControlAsgn_Dc$EditList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["asgnAccessFilter", "asgnAccessEditList"],["west", "center"])
      .addToolbarTo("main", "tlbAsgnAccessEditList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbAsgnAccessEditList", {dc: "asgnAccess"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addReports()
      .end();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.AccessControlsAsgn_Ui"
});
