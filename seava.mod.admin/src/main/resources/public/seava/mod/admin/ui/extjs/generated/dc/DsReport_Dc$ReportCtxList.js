/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.DsReport_Dc$ReportCtxList", {
  _noExport_:true,
  _noImport_:true,
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"reportId", dataIndex:"reportId", hidden:true, editor: {xtype: "textfield"}})
      .addLov({ name:"dataSource", dataIndex:"dataSource", width:200, noUpdate:true, editor: {xtype: "seava.mod.system.ui.extjs.generated.lov.DataSources_Lov", noUpdate:true}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_DsReport_Dc$ReportCtxList"
});
