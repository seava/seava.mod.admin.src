/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.Menus_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("menu", Ext.create(seava.mod.admin.ui.extjs.generated.dc.Menu_Dc,{multiEdit: true,  autoLoad:true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnAsgnMenuRoles", disabled:true, handler: this.onBtnAsgnMenuRoles,
          stateManager:{ name:"selected_one_clean", dc:"menu" }, scope:this})
      .addDcView("menu", {name:"menuFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_Menu_Dc$FilterV"})
      .addDcView("menu", {name:"menuEditList", xtype:"admin_Menu_Dc$EditList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["menuFilter", "menuEditList"],["west", "center"])
      .addToolbarTo("main", "tlbMenuEditList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbMenuEditList", {dc: "menu"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnAsgnMenuRoles") ])
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnAsgnMenuRoles
   */
  onBtnAsgnMenuRoles: function() {
    this._showAsgnWindow_("seava.mod.admin.ui.extjs.generated.asgn.Menu_Role_Asgn$Window" ,{dc: "menu", objectIdField: "id"});
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.Menus_Ui"
});
