/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.DsReportUsageCtx_Dc$EditList", {
  _noExport_:true,
  _noImport_:true,
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"frameName", dataIndex:"frameName", width:250, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"dcKey", dataIndex:"dcKey", width:80, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"toolbarKey", dataIndex:"toolbarKey", width:80, editor: {xtype: "textfield"}})
      .addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", width:80, editor: {xtype: "numberfield"}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_DsReportUsageCtx_Dc$EditList"
});
