/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.AccessControlDs_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"dsName", bind: "{d.dsName}", xtype:"seava.mod.system.ui.extjs.generated.lov.DataSourcesDs_Lov"})
      .addLov({name:"accessControl", bind: "{d.accessControl}", xtype:"seava.mod.admin.ui.extjs.generated.lov.AccessControls_Lov",
        retFieldMapping: [{lovField:"id", dsField: "accessControlId"} ]})
      .addLov({name:"withRole", paramIndex:"withRole", bind: "{p.withRole}", xtype:"seava.mod.admin.ui.extjs.generated.lov.Roles_Lov",
        retFieldMapping: [{lovField:"id", dsParam: "withRoleId"} ]})
      .addBooleanField({ name:"queryAllowed", bind: "{d.queryAllowed}", width:140})
      .addBooleanField({ name:"insertAllowed", bind: "{d.insertAllowed}", width:140})
      .addBooleanField({ name:"updateAllowed", bind: "{d.updateAllowed}", width:140})
      .addBooleanField({ name:"deleteAllowed", bind: "{d.deleteAllowed}", width:140})
      .addBooleanField({ name:"importAllowed", bind: "{d.importAllowed}", width:140})
      .addBooleanField({ name:"exportAllowed", bind: "{d.exportAllowed}", width:140})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:60}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["accessControl", "dsName", "withRole", "queryAllowed", "importAllowed", "exportAllowed", "insertAllowed", "updateAllowed", "deleteAllowed" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_AccessControlDs_Dc$FilterV"
});
