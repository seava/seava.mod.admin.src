/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.MyParam_Dc$List", {
  _noImport_:true,
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"code", dataIndex:"code", width:200})
      .addTextColumn({ name:"name", dataIndex:"name", width:200})
      .addTextColumn({ name:"description", dataIndex:"description", width:300})
      .addTextColumn({ name:"defaultValue", dataIndex:"defaultValue", width:200})
      .addTextColumn({ name:"listOfValues", dataIndex:"listOfValues", width:100})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.admin_MyParam_Dc$List"
});
