/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.AccessControl_Dc", {
  extend: "seava.lib.e4e.js.dc.AbstractDcController",
  
  paramModel: "seava.mod.admin.ui.extjs.generated.ds.AccessControl_DsParam",
  recordModel: "seava.mod.admin.ui.extjs.generated.ds.AccessControl_Ds"
});
