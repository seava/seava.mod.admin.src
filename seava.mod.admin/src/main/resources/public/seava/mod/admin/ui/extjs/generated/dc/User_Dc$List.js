/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.User_Dc$List", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"code", dataIndex:"code", width:150})
      .addTextColumn({ name:"name", dataIndex:"name", width:200})
      .addTextColumn({ name:"loginName", dataIndex:"loginName", width:150})
      .addTextColumn({ name:"dateFormat", dataIndex:"dateFormat"})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addBooleanColumn({ name:"locked", dataIndex:"locked"})
      .addTextColumn({ name:"decimalSeparator", dataIndex:"decimalSeparator", hidden:true})
      .addTextColumn({ name:"thousandSeparator", dataIndex:"thousandSeparator", hidden:true})
      .addTextColumn({ name:"notes", dataIndex:"notes", hidden:true})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.admin_User_Dc$List"
});
