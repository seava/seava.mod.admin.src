/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Menu_Dc$EditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", width:90, editor: {xtype: "numberfield"}})
      .addTextColumn({ name:"name", dataIndex:"name", width:150, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"title", dataIndex:"title", width:200, editor: {xtype: "textfield"}})
      .addComboColumn({ name:"tag", dataIndex:"tag", editor: {xtype: "combo", store:["top","left"]}})
      .addTextColumn({ name:"description", dataIndex:"description", width:250, editor: {xtype: "textfield"}})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_Menu_Dc$EditList"
});
