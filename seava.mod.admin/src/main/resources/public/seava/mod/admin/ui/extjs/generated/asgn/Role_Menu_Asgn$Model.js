/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.asgn.Role_Menu_Asgn$Model", {
  extend: 'Ext.data.Model',
  statics: {
    ALIAS: "ad_Role_Menu_Asgn"
  },
  fields:  [
    {name:"id", type:"string"},
    {name:"name", type:"string"},
    {name:"title", type:"string"}
  ]
});
