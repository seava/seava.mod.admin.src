/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.asgn.Role_Menu_Asgn$Window", {
  extend: "seava.lib.e4e.js.asgn.AbstractAsgnUi",
  width:700,
  height:400,
  title:"Assign menus to role",
  _filterFields_: [
    ["id"],
    ["name"],
    ["title"]
  ],
  _defaultFilterField_ : "name",

  _defineElements_: function () {
    this._getBuilder_()
      .addLeftGrid({ xtype:"Role_Menu_AsgnList$Left"})
      .addRightGrid({ xtype:"Role_Menu_AsgnList$Right"})
  }
});
