/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Role_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"code", bind: "{d.code}", xtype:"seava.mod.admin.ui.extjs.generated.lov.Roles_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addLov({name:"name", bind: "{d.name}", xtype:"seava.mod.admin.ui.extjs.generated.lov.RoleNames_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addLov({name:"withUser", paramIndex:"withUser", bind: "{p.withUser}", xtype:"seava.mod.admin.ui.extjs.generated.lov.Users_Lov",
        retFieldMapping: [{lovField:"id", dsParam: "withUserId"} ]})
      .addLov({name:"withPrivilege", paramIndex:"withPrivilege", bind: "{p.withPrivilege}", xtype:"seava.mod.admin.ui.extjs.generated.lov.AccessControls_Lov",
        retFieldMapping: [{lovField:"id", dsParam: "withPrivilegeId"} ]})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:60}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["name", "code", "withUser", "withPrivilege", "active" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_Role_Dc$FilterV"
});
