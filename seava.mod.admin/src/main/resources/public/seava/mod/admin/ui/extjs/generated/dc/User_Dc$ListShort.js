/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.User_Dc$ListShort", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"code", dataIndex:"code"})
      .addTextColumn({ name:"name", dataIndex:"name"})
      .addTextColumn({ name:"loginName", dataIndex:"loginName"})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addBooleanColumn({ name:"locked", dataIndex:"locked"})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.admin_User_Dc$ListShort"
});
