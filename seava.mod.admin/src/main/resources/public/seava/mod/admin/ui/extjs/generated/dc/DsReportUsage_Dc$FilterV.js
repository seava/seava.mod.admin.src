/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.DsReportUsage_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"report", bind: "{d.report}", xtype:"seava.mod.admin.ui.extjs.generated.lov.Reports_Lov",
        retFieldMapping: [{lovField:"id", dsField: "reportId"} ]})
      .addLov({name:"reportName", bind: "{d.reportName}", xtype:"seava.mod.admin.ui.extjs.generated.lov.ReportsName_Lov",
        retFieldMapping: [{lovField:"id", dsField: "reportId"} ]})
      .addLov({name:"dataSource", bind: "{d.dataSource}", xtype:"seava.mod.system.ui.extjs.generated.lov.DataSources_Lov"})
      .addTextField({ name:"frameName", bind: "{d.frameName}", maxLength: 255})
      .addTextField({ name:"toolbarKey", bind: "{d.toolbarKey}", maxLength: 4000})
      .addTextField({ name:"dcKey", bind: "{d.dcKey}", maxLength: 4000})
      .addNumberField({name:"sequenceNo", bind: "{d.sequenceNo}"})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:80}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["report", "reportName", "dataSource", "frameName", "toolbarKey", "dcKey" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_DsReportUsage_Dc$FilterV"
});
