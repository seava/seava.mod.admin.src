/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.AccessControlDsRpc_Dc$EditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({ name:"accessControl", dataIndex:"accessControl", editor: {xtype: "seava.mod.admin.ui.extjs.generated.lov.AccessControls_Lov",
        retFieldMapping: [{lovField:"id", dsField: "accessControlId"} ]}})
      .addLov({ name:"dsName", dataIndex:"dsName", editor: {xtype: "seava.mod.system.ui.extjs.generated.lov.DataSourcesDs_Lov"}})
      .addLov({ name:"serviceMethod", dataIndex:"serviceMethod", editor: {xtype: "seava.mod.system.ui.extjs.generated.lov.DataSourceRpcs_Lov",
        filterFieldMapping: [{lovField:"dataSourceName", dsField: "dsName"} ]}})
      .addTextColumn({ name:"accessControlId", dataIndex:"accessControlId", hidden:true, noEdit:true })
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_AccessControlDsRpc_Dc$EditList"
});
