/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.AccessControls_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("ctrl", Ext.create(seava.mod.admin.ui.extjs.generated.dc.AccessControl_Dc,{multiEdit: true,  autoLoad:true}))
      .addDc("dsAccess", Ext.create(seava.mod.admin.ui.extjs.generated.dc.AccessControlDs_Dc,{multiEdit: true}))
      .addDc("asgnAccess", Ext.create(seava.mod.admin.ui.extjs.generated.dc.AccessControlAsgn_Dc,{multiEdit: true}))
      .addDc("dsMtdAccess", Ext.create(seava.mod.admin.ui.extjs.generated.dc.AccessControlDsRpc_Dc,{multiEdit: true}))
      .addDc("rol", Ext.create(seava.mod.admin.ui.extjs.generated.dc.Role_Dc,{}))
      .linkDc("dsAccess", "ctrl",{fetchMode:"auto",
        fields:[{childField:"accessControlId", parentField:"id"}]})
      .linkDc("asgnAccess", "ctrl",{
        fields:[{childField:"accessControlId", parentField:"id"}]})
      .linkDc("dsMtdAccess", "ctrl",{
        fields:[{childField:"accessControlId", parentField:"id"}]})
      .linkDc("rol", "ctrl",{
        fields:[{childParam:"withPrivilegeId", parentField:"id"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnCopyRules", disabled:true, iconCls:"icon-action-copy", handler: this.onBtnCopyRules,
          stateManager:{ name:"selected_one_clean", dc:"ctrl" }, scope:this})
      .addButton({name:"btnCopyRulesExec", disabled:false, iconCls:"icon-action-copy", handler: this.onBtnCopyRulesExec, scope:this})
      .addButton({name:"btnAsgnRoles", disabled:true, handler: this.onBtnAsgnRoles,
          stateManager:{ name:"selected_one_clean", dc:"ctrl" }, scope:this})
      .addButton({name:"btnShowUiAsgnRules", disabled:false, handler: this.onBtnShowUiAsgnRules, scope:this})
      .addButton({name:"btnShowUiDsRules", disabled:false, handler: this.onBtnShowUiDsRules, scope:this})
      .addButton({name:"btnShowUiServiceRules", disabled:false, handler: this.onBtnShowUiServiceRules, scope:this})
      .addButton({name:"btnDetailsRole", disabled:true, handler: this.onBtnDetailsRole,
          stateManager:{ name:"selected_one", dc:"rol" }, scope:this})
      .addDcView("ctrl", {name:"privilegeFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_AccessControl_Dc$FilterV"})
      .addDcView("ctrl", {name:"privilegeEditList", xtype:"admin_AccessControl_Dc$EditList"})
      .addDcView("ctrl", {name:"privilegeCopyRules", xtype:"admin_AccessControl_Dc$CopyRulesFromSource"})
      .addDcView("dsAccess", {name:"dsAccessCtxEditList", _hasTitle_:true, xtype:"admin_AccessControlDs_Dc$CtxEditList"})
      .addDcView("asgnAccess", {name:"asgnAccessCtxEditList", _hasTitle_:true, xtype:"admin_AccessControlAsgn_Dc$CtxEditList"})
      .addDcView("dsMtdAccess", {name:"dsMtdAccessCtxEditList", _hasTitle_:true, xtype:"admin_AccessControlDsRpc_Dc$CtxEditList"})
      .addDcView("rol", {name:"rolList", _hasTitle_:true, xtype:"admin_Role_Dc$List"})
      .addWindow({name:"wdwCopyRules", _hasTitle_:true, width:400, 
          dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
            items:[ this._elems_.get("btnCopyRulesExec")]}], closeAction:'hide', resizable:true, layout:"fit", modal:true,
        items:[this._elems_.get("privilegeCopyRules")]})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:2,center:1,south:1}}, defaults:{split:true}})
      .addPanel({name:"detailTabs", height:280, xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["privilegeFilter", "privilegeEditList", "detailTabs"],["west", "center", "south"])
      .addChildrenTo("detailTabs", ["dsAccessCtxEditList", "asgnAccessCtxEditList", "dsMtdAccessCtxEditList", "rolList"])
      .addToolbarTo("main", "tlbCtrlEditList")
      .addToolbarTo("dsAccessCtxEditList", "tlbDsAccessCtxEditList")
      .addToolbarTo("asgnAccessCtxEditList", "tlbAsgnAccessCtxEditList")
      .addToolbarTo("dsMtdAccessCtxEditList", "tlbDsMtdAccessCtxEditList")
      .addToolbarTo("rolList", "tlbRolList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbCtrlEditList", {dc: "ctrl"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnCopyRules") ,this._elems_.get("btnShowUiDsRules") ,this._elems_.get("btnShowUiAsgnRules") ,this._elems_.get("btnShowUiServiceRules") ])
        .addReports()
      .end().beginToolbar("tlbDsAccessCtxEditList", {dc: "dsAccess"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addAutoLoad()
        .addReports()
      .end().beginToolbar("tlbAsgnAccessCtxEditList", {dc: "asgnAccess"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addAutoLoad()
        .addReports()
      .end().beginToolbar("tlbDsMtdAccessCtxEditList", {dc: "dsMtdAccess"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addAutoLoad()
        .addReports()
      .end().beginToolbar("tlbRolList", {dc: "rol"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnAsgnRoles") ,this._elems_.get("btnDetailsRole") ])
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnCopyRules
   */
  onBtnCopyRules: function() {
    this._getWindow_("wdwCopyRules").show();
  },
  
  /**
   * On-Click handler for button btnCopyRulesExec
   */
  onBtnCopyRulesExec: function() {
    var successFn = function(dc,response,serviceName,specs) {
      this._getWindow_("wdwCopyRules").close();
      this._getDc_("dsAccess").doQuery();
      this._getDc_("asgnAccess").doQuery();
    };
    var o={
      name:"copyRulesFromAccessControl",
      callbacks:{
        successFn: successFn,
        successScope: this
      },
      modal:true
    };
    this._getDc_("ctrl").doRpcData(o);
  },
  
  /**
   * On-Click handler for button btnAsgnRoles
   */
  onBtnAsgnRoles: function() {
    this._showAsgnWindow_("seava.mod.admin.ui.extjs.generated.asgn.AccessControl_Role_Asgn$Window" ,{dc: "ctrl", objectIdField: "id"
    ,listeners: {close: {scope: this, fn: function() { this._getDc_("rol").doReloadPage();
    }} }});
  },
  
  /**
   * On-Click handler for button btnShowUiAsgnRules
   */
  onBtnShowUiAsgnRules: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.AccessControlsAsgn_Ui");
  },
  
  /**
   * On-Click handler for button btnShowUiDsRules
   */
  onBtnShowUiDsRules: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.AccessControlsDs_Ui");
  },
  
  /**
   * On-Click handler for button btnShowUiServiceRules
   */
  onBtnShowUiServiceRules: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.AccessControlsDsRpc_Ui");
  },
  
  /**
   * On-Click handler for button btnDetailsRole
   */
  onBtnDetailsRole: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.Roles_Ui",{
      params: {
        id: this._getDc_("rol").getRecord().get("id"),
        code: this._getDc_("rol").getRecord().get("code")
      },
      callback: function (params) {
        this._when_called_for_details(params);
      }
    });
  },
  
  _when_called_for_details: function(params) {
    
    var dc = this._getDc_("ctrl");
    dc.setFilterValue("id", params.id);
    dc.setFilterValue("name", params.name);
    dc.doQuery();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.AccessControls_Ui"
});
