/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Report_Dc$Edit", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextField({ name:"name", bind: "{d.name}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"code", bind: "{d.code}", allowBlank:false, maxLength: 64})
      .addTextField({ name:"contextPath", bind: "{d.contextPath}", maxLength: 255})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      .addTextArea({ name:"notes", bind: "{d.notes}", height:60})
      .addLov({name:"reportServer", bind: "{d.reportServer}", allowBlank:false, xtype:"seava.mod.admin.ui.extjs.generated.lov.ReportServers_Lov",
        retFieldMapping: [{lovField:"id", dsField: "reportServerId"} ],
        filterFieldMapping: [{lovField:"active", value: "true"} ]})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col1", width:350, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}})
      .addPanel({ name:"col2", width:500, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["col1", "col2" ])
       .addChildrenTo("col1", ["name", "code", "reportServer", "active" ])
       .addChildrenTo("col2", ["contextPath", "notes" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditForm",
  alias: "widget.admin_Report_Dc$Edit"
});
