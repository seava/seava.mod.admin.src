/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.User_Ds", {
  extend: 'Ext.data.Model',
  
  statics: {
    ALIAS: "ad_User_Ds"
  },
  
  fields: [
    {name:"loginName", type:"string"},
    {name:"code", type:"string"},
    {name:"locked", type:"boolean"},
    {name:"email", type:"string"},
    {name:"decimalSeparator", type:"string"},
    {name:"thousandSeparator", type:"string"},
    {name:"dateFormatId", type:"string"},
    {name:"dateFormat", type:"string"},
    {name:"id", type:"string"},
    {name:"clientId", type:"string"},
    {name:"code", type:"string"},
    {name:"name", type:"string"},
    {name:"description", type:"string"},
    {name:"notes", type:"string"},
    {name:"active", type:"boolean"},
    {name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"createdBy", type:"string"},
    {name:"modifiedBy", type:"string"},
    {name:"version", type:"int", useNull:true},
    {name:"refid", type:"string"},
    {name:"entityAlias", type:"string"},
    {name:"entityFqn", type:"string"}
  ]
});
