/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.AccessControlAsgn_Dc$CtxEditList", {
  _noImport_:true,
  _noExport_:true,
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({ name:"asgnName", dataIndex:"asgnName", editor: {xtype: "seava.mod.system.ui.extjs.generated.lov.DataSourcesAsgn_Lov"}})
      .addBooleanColumn({ name:"queryAllowed", dataIndex:"queryAllowed"})
      .addBooleanColumn({ name:"updateAllowed", dataIndex:"updateAllowed"})
      .addTextColumn({ name:"accessControlId", dataIndex:"accessControlId", hidden:true, editor: {xtype: "textfield"}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_AccessControlAsgn_Dc$CtxEditList"
});
