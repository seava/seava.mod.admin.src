/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.ParamValue_DsParam", {
  extend: 'Ext.data.Model',
  
  fields: [
    {name:"validAt", type:"date", forFilter:true, dateFormat:Main.MODEL_DATE_FORMAT}
  ]
});
