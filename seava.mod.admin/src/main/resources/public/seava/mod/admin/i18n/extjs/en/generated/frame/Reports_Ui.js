Ext.define("seava.mod.admin.i18n.extjs.en.generated.frame.Reports_Ui", {
  /* view */
  dsRep__ttl: "DS links",
  dsparamList__ttl: "Parameter mapping",
  paramEditList__ttl: "Parameters",
  repFilter__ttl: "Filter",
  usageList__ttl: "Usage",
  wdwTestReport__ttl: "Test report",
  /* menu */
  tlbDsParamList__ttl: "Parameter mapping",
  tlbDsRepList__ttl: "Data-source",
  tlbParamList__ttl: "Parameters",
  tlbRepEdit__ttl: "Report",
  tlbRepList__ttl: "Reports",
  tlbUsageList__ttl: "Usage",
  /* button */
  btnCancelReport__lbl: "Cancel",
  btnCancelReport__tlp: "",
  btnRunReport__lbl: "Run",
  btnRunReport__tlp: "",
  btnTestReport__lbl: "Test report",
  btnTestReport__tlp: "",
  
  title: "Reports"
});
