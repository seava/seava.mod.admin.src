/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.ParamValue_Dc$CtxEditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"value", dataIndex:"value", width:350, editor: {xtype: "textfield"}})
      .addDateColumn({name:"validFrom", dataIndex:"validFrom", _mask_: Masks.DATE, editor: {xtype: "datefield", _mask_: Masks.DATE}})
      .addDateColumn({name:"validTo", dataIndex:"validTo", _mask_: Masks.DATE, editor: {xtype: "datefield", _mask_: Masks.DATE}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_ParamValue_Dc$CtxEditList"
});
