/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.asgn.Role_User_Asgn$Window", {
  extend: "seava.lib.e4e.js.asgn.AbstractAsgnUi",
  width:800,
  height:400,
  title:"Assign users to role",
  _filterFields_: [
    ["id"],
    ["code"],
    ["name"]
  ],
  _defaultFilterField_ : "code",

  _defineElements_: function () {
    this._getBuilder_()
      .addLeftGrid({ xtype:"Role_User_AsgnList$Left"})
      .addRightGrid({ xtype:"Role_User_AsgnList$Right"})
  }
});
