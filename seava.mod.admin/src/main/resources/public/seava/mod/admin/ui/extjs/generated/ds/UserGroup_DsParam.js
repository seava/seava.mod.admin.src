/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.UserGroup_DsParam", {
  extend: 'Ext.data.Model',
  
  fields: [
    {name:"withUser", type:"string", forFilter:true},
    {name:"withUserId", type:"string", forFilter:true}
  ]
});
