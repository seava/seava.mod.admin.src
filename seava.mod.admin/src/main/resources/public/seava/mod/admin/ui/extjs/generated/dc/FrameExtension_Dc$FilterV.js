/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.FrameExtension_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextField({ name:"frame", bind: "{d.frame}", maxLength: 255})
      .addTextField({ name:"fileLocation", bind: "{d.fileLocation}", maxLength: 255})
      .addBooleanField({ name:"relativePath", bind: "{d.relativePath}"})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:60}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["frame", "fileLocation", "relativePath", "active" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_FrameExtension_Dc$FilterV"
});
