Ext.define("seava.mod.admin.i18n.extjs.en.generated.frame.AccessControls_Ui", {
  /* view */
  asgnAccessCtxEditList__ttl: "Access rules - ASGN",
  dsAccessCtxEditList__ttl: "Access rules - DS",
  dsMtdAccessCtxEditList__ttl: "Access rules - Services",
  privilegeFilter__ttl: "Filter",
  rolList__ttl: "Roles",
  wdwCopyRules__ttl: "Select source",
  /* menu */
  tlbAsgnAccessCtxEditList__ttl: "Access rules - ASGN",
  tlbCtrlEditList__ttl: "Privileges",
  tlbDsAccessCtxEditList__ttl: "Access rules - DS",
  tlbDsMtdAccessCtxEditList__ttl: "Access rules - Services",
  tlbRolList__ttl: "Roles",
  /* button */
  btnAsgnRoles__lbl: "Assign-Roles",
  btnAsgnRoles__tlp: "Assign selected privilege to roles",
  btnCopyRules__lbl: "Copy rules",
  btnCopyRules__tlp: "Copy rules from another privilege",
  btnCopyRulesExec__lbl: "OK",
  btnCopyRulesExec__tlp: "Copy rules from selected privilege",
  btnDetailsRole__lbl: "Role details",
  btnDetailsRole__tlp: "Open frame to show more details for the selected role",
  btnShowUiAsgnRules__lbl: "ASGN rules",
  btnShowUiAsgnRules__tlp: "Open assignment rules frame",
  btnShowUiDsRules__lbl: "DS rules",
  btnShowUiDsRules__tlp: "Open data-source rules frame",
  btnShowUiServiceRules__lbl: "Service rules",
  btnShowUiServiceRules__tlp: "Open data-source service rules frame",
  
  title: "Privileges"
});
