Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.AccessControlDsRpc_Ds", {
  accessControlId__lbl: "Access Control(ID)",
  accessControl__lbl: "Access Control",
  clientId__lbl: "Client Id",
  createdAt__lbl: "Created At",
  createdBy__lbl: "Created By",
  dsName__lbl: "Ds Name",
  entityAlias__lbl: "Entity Alias",
  entityFqn__lbl: "Entity Fqn",
  id__lbl: "Id",
  modifiedAt__lbl: "Modified At",
  modifiedBy__lbl: "Modified By",
  notes__lbl: "Notes",
  refid__lbl: "Refid",
  serviceMethod__lbl: "Allow method",
  version__lbl: "Version"
});
