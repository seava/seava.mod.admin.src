/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Report_Dc", {
  extend: "seava.lib.e4e.js.dc.AbstractDcController",
  
  recordModel: "seava.mod.admin.ui.extjs.generated.ds.Report_Ds"
});
