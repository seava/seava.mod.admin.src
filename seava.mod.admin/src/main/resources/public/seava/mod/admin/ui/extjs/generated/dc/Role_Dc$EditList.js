/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Role_Dc$EditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"code", dataIndex:"code", width:120, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"name", dataIndex:"name", width:200, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"description", dataIndex:"description", width:300, editor: {xtype: "textfield"}})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_Role_Dc$EditList"
});
