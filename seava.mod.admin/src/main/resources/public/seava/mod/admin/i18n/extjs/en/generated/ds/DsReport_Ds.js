Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.DsReport_Ds", {
  clientId__lbl: "Client Id",
  createdAt__lbl: "Created At",
  createdBy__lbl: "Created By",
  dataSource__lbl: "Data Source",
  entityAlias__lbl: "Entity Alias",
  entityFqn__lbl: "Entity Fqn",
  id__lbl: "Id",
  modifiedAt__lbl: "Modified At",
  modifiedBy__lbl: "Modified By",
  notes__lbl: "Notes",
  refid__lbl: "Refid",
  reportCode__lbl: "Report",
  reportId__lbl: "Report(ID)",
  version__lbl: "Version"
});
