Ext.define("seava.mod.admin.i18n.extjs.en.generated.frame.Users_Ui", {
  /* view */
  grpList__ttl: "Groups",
  rolList__ttl: "Roles",
  usrFilter__ttl: "Filter",
  wdwChangePassword__ttl: "Change password",
  /* menu */
  tlbGrpList__ttl: "Groups",
  tlbRolList__ttl: "Roles",
  tlbUsrEdit__ttl: "User",
  tlbUsrList__ttl: "Users",
  /* button */
  btnAsgnGroups__lbl: "Assign groups",
  btnAsgnGroups__tlp: "Add to user-groups",
  btnAsgnRoles__lbl: "Assign roles",
  btnAsgnRoles__tlp: "Assign roles",
  btnChangePassword__lbl: "Change password",
  btnChangePassword__tlp: "Change the user`s password",
  btnDetailsGroup__lbl: "Group details",
  btnDetailsGroup__tlp: "Open frame to show more details for the selected group",
  btnDetailsRole__lbl: "Role details",
  btnDetailsRole__tlp: "Open frame to show more details for the selected role",
  btnSavePassword__lbl: "Save",
  btnSavePassword__tlp: "Save new password",
  
  title: "Users"
});
