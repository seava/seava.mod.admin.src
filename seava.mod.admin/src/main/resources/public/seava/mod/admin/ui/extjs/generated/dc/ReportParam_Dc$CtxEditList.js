/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.ReportParam_Dc$CtxEditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"reportCode", dataIndex:"reportCode", width:60, hidden:true, editor: {xtype: "textfield"}})
      .addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", editor: {xtype: "numberfield"}})
      .addTextColumn({ name:"name", dataIndex:"name", editor: {xtype: "textfield"}})
      .addTextColumn({ name:"title", dataIndex:"title", editor: {xtype: "textfield"}})
      .addComboColumn({ name:"dataType", dataIndex:"dataType", editor: {xtype: "combo", store:["string","text","integer","decimal","boolean","date"]}})
      .addTextColumn({ name:"listOfValues", dataIndex:"listOfValues", width:100, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"defaultValue", dataIndex:"defaultValue", width:100, editor: {xtype: "textfield"}})
      .addBooleanColumn({ name:"mandatory", dataIndex:"mandatory", width:60})
      .addBooleanColumn({ name:"noEdit", dataIndex:"noEdit", width:60})
      .addBooleanColumn({ name:"active", dataIndex:"active", width:60})
      .addTextColumn({ name:"notes", dataIndex:"notes", hidden:true, editor: {xtype: "textfield"}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_ReportParam_Dc$CtxEditList"
});
