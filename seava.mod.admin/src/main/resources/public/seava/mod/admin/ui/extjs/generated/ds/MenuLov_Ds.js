/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.MenuLov_Ds", {
  extend: 'Ext.data.Model',
  
  statics: {
    ALIAS: "ad_MenuLov_Ds"
  },
  
  fields: [
    {name:"title", type:"string"},
    {name:"id", type:"string"},
    {name:"clientId", type:"string"},
    {name:"name", type:"string"},
    {name:"description", type:"string"},
    {name:"active", type:"boolean"},
    {name:"refid", type:"string"}
  ]
});
