Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.AccessControl_DsParam", {
  copyFromId__lbl: "Copy From Id",
  copyFrom__lbl: "Copy From",
  resetRules__lbl: "Reset Rules",
  skipAsgn__lbl: "Skip Asgn",
  skipDs__lbl: "Skip Ds",
  withRoleId__lbl: "With Role Id",
  withRole__lbl: "With Role"
});
