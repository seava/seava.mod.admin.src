/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Attachment_Dc$List", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"type", dataIndex:"type", width:150, noEdit:true })
      .addTextColumn({ name:"name", dataIndex:"name", width:300, renderer:function(v, md, rec, ri, ci, store) {return '<a href="javascript:void(0);"  onClick=\'javascript: window.open("'+rec.data.url+'", "Attachment", "location=1,status=1,scrollbars=1,width=660,height=500");\'>'+  rec.data.name  +'</a>'; }, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"notes", dataIndex:"notes", width:300, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"url", dataIndex:"url", hidden:true, noEdit:true })
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_Attachment_Dc$List"
});
