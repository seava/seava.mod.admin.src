/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.ViewState_Dc$Value", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextArea({ name:"value", bind: "{d.value}", noEdit:true })
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: { type: "fit"}, defaults: { labelAlign:"top"}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["value" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditForm",
  alias: "widget.admin_ViewState_Dc$Value"
});
