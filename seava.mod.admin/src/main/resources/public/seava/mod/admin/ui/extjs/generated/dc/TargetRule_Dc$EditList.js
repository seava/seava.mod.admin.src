/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.TargetRule_Dc$EditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"targetAlias", dataIndex:"targetAlias", width:200, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"targetType", dataIndex:"targetType", width:200, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"sourceRefId", dataIndex:"sourceRefId", hidden:true, noEdit:true })
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_TargetRule_Dc$EditList"
});
