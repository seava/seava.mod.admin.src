Ext.define("seava.mod.admin.i18n.extjs.en.generated.frame.MyParams_Ui", {
  /* view */
  paramFilter__ttl: "Filter",
  wdwInitVal__ttl: "Set validity",
  /* menu */
  tlbParamList__ttl: "Parameters",
  tlbValList__ttl: "Parameter values",
  /* button */
  btnInitVal__lbl: "Initialize values",
  btnInitVal__tlp: "Create initial values for the selected parameters (Ignores those already initialized).",
  btnOk__lbl: "Ok",
  btnOk__tlp: "",
  btnPublish__lbl: "Publish changes",
  btnPublish__tlp: "Publish the changes .",
  btnShowValuesFrame__lbl: "Open values frame",
  btnShowValuesFrame__tlp: "Show the values frame",
  
  title: "Parameters"
});
