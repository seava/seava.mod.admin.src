Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.ReportServer_Ds", {
  active__lbl: "Active",
  clientId__lbl: "Client Id",
  createdAt__lbl: "Created At",
  createdBy__lbl: "Created By",
  description__lbl: "Description",
  entityAlias__lbl: "Entity Alias",
  entityFqn__lbl: "Entity Fqn",
  id__lbl: "Id",
  modifiedAt__lbl: "Modified At",
  modifiedBy__lbl: "Modified By",
  name__lbl: "Name",
  notes__lbl: "Notes",
  queryBuilderClass__lbl: "Query Builder Class",
  refid__lbl: "Refid",
  url__lbl: "Url",
  version__lbl: "Version"
});
