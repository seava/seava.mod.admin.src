/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.ReportParamRt_Dc$EditList", {
  _noPaginator_:true,
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", hidden:true, noEdit:true })
      .addTextColumn({ name:"name", dataIndex:"name", hidden:true, noEdit:true })
      .addTextColumn({ name:"title", dataIndex:"title", width:200, noEdit:true })
      .addTextColumn({ name:"value", dataIndex:"value", editor: {xtype: "textfield"}})
      .addDefaults();
  },
  
  _getCustomCellEditor_: function(record,column) {
    
    		var ed = null;
    		var cfg = {};
     		
    		if ( record.data.noEdit ) {			 
    	    	ed = new  Ext.form.field.Text({ 
    				readOnly:true 
    			});
    	    }
    		else if (!Ext.isEmpty(record.data.listOfValues)) {
    	    	ed = new Ext.form.field.ComboBox({
    				store:record.data.listOfValues.split(","),
    				allowBlank : ((record.data.mandatory === true) ? true : false)
    			});
    	    }
    		else if (record.data.dataType == "integer" || record.data.dataType == "decimal") {
    	    	ed = new Ext.form.field.Number({ 
    				allowBlank : ((record.data.mandatory === true) ? true : false),
    				fieldStyle : "text-align:right;",
    				hideTrigger : true,
    				keyNavEnabled : false,
    				mouseWheelEnabled : false 
    			});
    	    }
    	    else if (record.data.dataType == "date") {
    	    	ed = new Ext.form.field.Date({
    				format: Main.MODEL_DATE_FORMAT
    			});
    	    }
    	    else if (record.data.dataType == "boolean") {
    	    	ed = new Ext.form.field.ComboBox({
    				store:["true","false"]
    			});
    	    } else {
    	    	ed = new  Ext.form.field.Text({
    				allowBlank : ((record.data.mandatory === true) ? true : false),
    			});
    		}
    	    if(ed){
    		    ed._dcView_ =  this;
    	    }
    	    return ed;
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_ReportParamRt_Dc$EditList"
});
