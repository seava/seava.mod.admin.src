/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.ReportsUsage_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("ru", Ext.create(seava.mod.admin.ui.extjs.generated.dc.DsReportUsage_Dc,{multiEdit: true,  autoLoad:true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addDcView("ru", {name:"ruFilter", _hasTitle_:true, width:250,  collapsed:true, collapsible:true, minWidth:230, maxWidth:300, xtype:"admin_DsReportUsage_Dc$FilterV"})
      .addDcView("ru", {name:"ruList", xtype:"admin_DsReportUsage_Dc$List"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["ruFilter", "ruList"],["west", "center"])
      .addToolbarTo("main", "tlbRuList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbRuList", {dc: "ru"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addReports()
      .end();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.ReportsUsage_Ui"
});
