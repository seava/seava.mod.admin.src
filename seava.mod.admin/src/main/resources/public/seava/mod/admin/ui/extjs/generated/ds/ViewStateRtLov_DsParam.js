/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.ViewStateRtLov_DsParam", {
  extend: 'Ext.data.Model',
  
  fields: [
    {name:"hideMine", type:"boolean", forFilter:true},
    {name:"hideOthers", type:"boolean", forFilter:true}
  ]
});
