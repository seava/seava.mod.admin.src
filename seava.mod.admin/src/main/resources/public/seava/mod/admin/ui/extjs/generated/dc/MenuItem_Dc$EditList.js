/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.MenuItem_Dc$EditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", width:70, format:"0", editor: {xtype: "numberfield", allowBlank:false}})
      .addTextColumn({ name:"name", dataIndex:"name", width:150, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"title", dataIndex:"title", width:150, editor: {xtype: "textfield"}})
      .addLov({ name:"menu", dataIndex:"menu", width:100, editor: {xtype: "seava.mod.admin.ui.extjs.generated.lov.Menus_Lov",
        retFieldMapping: [{lovField:"id", dsField: "menuId"} ]}})
      .addLov({ name:"menuItem", dataIndex:"menuItem", width:120, editor: {xtype: "seava.mod.admin.ui.extjs.generated.lov.MenuItems_Lov",
        retFieldMapping: [{lovField:"id", dsField: "menuItemId"} ],
        filterFieldMapping: [{lovParam:"foldersOnly", value: "true"} ]}})
      .addTextColumn({ name:"frame", dataIndex:"frame", width:350, editor: {xtype: "textfield"}})
      .addBooleanColumn({ name:"active", dataIndex:"active", width:70})
      .addTextColumn({ name:"description", dataIndex:"description", hidden:true, editor: {xtype: "textfield"}})
      .addBooleanColumn({ name:"separatorBefore", dataIndex:"separatorBefore", hidden:true})
      .addBooleanColumn({ name:"separatorAfter", dataIndex:"separatorAfter", hidden:true})
      .addTextColumn({ name:"iconUrl", dataIndex:"iconUrl", hidden:true, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"menuId", dataIndex:"menuId", hidden:true, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"menuItemId", dataIndex:"menuItemId", hidden:true, editor: {xtype: "textfield"}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_MenuItem_Dc$EditList"
});
