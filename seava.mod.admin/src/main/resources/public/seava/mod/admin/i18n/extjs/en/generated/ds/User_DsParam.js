Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.User_DsParam", {
  confirmPassword__lbl: "Confirm Password",
  inGroupId__lbl: "In Group Id",
  inGroup__lbl: "In Group",
  newPassword__lbl: "New Password",
  withRoleId__lbl: "With Role Id",
  withRole__lbl: "With Role"
});
