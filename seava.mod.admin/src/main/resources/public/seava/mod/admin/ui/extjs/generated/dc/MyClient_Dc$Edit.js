/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.MyClient_Dc$Edit", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextField({ name:"name", bind: "{d.name}", noEdit:true , maxLength: 255})
      .addTextField({ name:"code", bind: "{d.code}", noEdit:true , maxLength: 64})
      .addTextField({ name:"workspacePath", bind: "{d.workspacePath}", noEdit:true , maxLength: 255})
      .addTextField({ name:"importPath", bind: "{d.importPath}", noEdit:true , maxLength: 255})
      .addTextField({ name:"exportPath", bind: "{d.exportPath}", noEdit:true , maxLength: 255})
      .addTextField({ name:"tempPath", bind: "{d.tempPath}", noEdit:true , maxLength: 255})
      .addTextField({ name:"adminRole", bind: "{d.adminRole}", allowBlank:false, maxLength: 32})
      .addTextField({ name:"createdBy", bind: "{d.createdBy}", noEdit:true , maxLength: 32})
      .addTextField({ name:"modifiedBy", bind: "{d.modifiedBy}", noEdit:true , maxLength: 32})
      .addDateField({name:"createdAt", bind: "{d.createdAt}", _mask_:Masks.DATETIMESEC, noEdit:true })
      .addDateField({name:"modifiedAt", bind: "{d.modifiedAt}", _mask_:Masks.DATETIMESEC, noEdit:true })
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col1", width:350, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}})
      .addPanel({ name:"col3", _hasTitle_: true, width:600, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"},  xtype:"fieldset", collapsible:true, border:true, margin:"0 0 0 10", defaults: { labelWidth:120}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["col1", "col3" ])
       .addChildrenTo("col1", ["name", "code", "adminRole", "createdAt", "modifiedAt", "createdBy", "modifiedBy" ])
       .addChildrenTo("col3", ["workspacePath", "importPath", "exportPath", "tempPath" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditForm",
  alias: "widget.admin_MyClient_Dc$Edit"
});
