/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define( "seava.mod.admin.ui.extjs.generated.asgn.MenuItem_Role_Asgn$List", {
  extend: "seava.lib.e4e.js.asgn.AbstractAsgnGrid",
  alias:[ "widget.MenuItem_Role_AsgnList$Left","widget.MenuItem_Role_AsgnList$Right" ],
  _defineColumns_: function () {
    this._getBuilder_()
    .addTextColumn({name:"id", dataIndex:"id", hidden:true})
    .addTextColumn({name:"code", dataIndex:"code", width:120})
    .addTextColumn({name:"name", dataIndex:"name"})
    .addTextColumn({name:"description", dataIndex:"description", hidden:true})
  } 
});
