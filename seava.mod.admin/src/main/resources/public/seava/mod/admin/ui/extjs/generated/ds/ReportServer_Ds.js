/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.ReportServer_Ds", {
  extend: 'Ext.data.Model',
  
  statics: {
    ALIAS: "ad_ReportServer_Ds"
  },
  
  fields: [
    {name:"url", type:"string"},
    {name:"queryBuilderClass", type:"string"},
    {name:"id", type:"string"},
    {name:"name", type:"string"},
    {name:"active", type:"boolean"},
    {name:"description", type:"string"},
    {name:"notes", type:"string"},
    {name:"clientId", type:"string"},
    {name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"createdBy", type:"string"},
    {name:"modifiedBy", type:"string"},
    {name:"version", type:"int", useNull:true},
    {name:"refid", type:"string"},
    {name:"entityAlias", type:"string"},
    {name:"entityFqn", type:"string"}
  ]
});
