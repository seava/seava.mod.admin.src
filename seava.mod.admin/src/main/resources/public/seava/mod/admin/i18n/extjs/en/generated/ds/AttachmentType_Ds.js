Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.AttachmentType_Ds", {
  active__lbl: "Active",
  baseUrl__lbl: "Base Url",
  category__lbl: "Category",
  clientId__lbl: "Client Id",
  createdAt__lbl: "Created At",
  createdBy__lbl: "Created By",
  description__lbl: "Description",
  entityAlias__lbl: "Entity Alias",
  entityFqn__lbl: "Entity Fqn",
  id__lbl: "Id",
  modifiedAt__lbl: "Modified At",
  modifiedBy__lbl: "Modified By",
  name__lbl: "Name",
  notes__lbl: "Notes",
  refid__lbl: "Refid",
  uploadPath__lbl: "Upload Path",
  version__lbl: "Version"
});
