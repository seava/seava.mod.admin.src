/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.UserGroup_Dc$FilterPG", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({ name:"code", dataIndex:"code", editor: {xtype: "seava.mod.admin.ui.extjs.generated.lov.UserGroups_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
      .addTextColumn({ name:"name", dataIndex:"name", editor: {xtype: "textfield"}})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addLov({ name:"withUser", dataIndex:"withUser", editor: {xtype: "seava.mod.admin.ui.extjs.generated.lov.Users_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]}})
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterPropGrid",
  alias: "widget.admin_UserGroup_Dc$FilterPG"
});
