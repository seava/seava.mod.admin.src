/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.Users_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("usr", Ext.create(seava.mod.admin.ui.extjs.generated.dc.User_Dc,{trackEditMode: true,  autoLoad:true}))
      .addDc("rol", Ext.create(seava.mod.admin.ui.extjs.generated.dc.Role_Dc,{}))
      .addDc("grp", Ext.create(seava.mod.admin.ui.extjs.generated.dc.UserGroup_Dc,{}))
      .linkDc("rol", "usr",{fetchMode:"auto",
        fields:[{childParam:"withUserId", parentField:"id"}]})
      .linkDc("grp", "usr",{
        fields:[{childParam:"withUserId", parentField:"id"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnChangePassword", disabled:true, handler: this.onBtnChangePassword,
          stateManager:{ name:"record_is_clean", dc:"usr" }, scope:this})
      .addButton({name:"btnSavePassword", disabled:false, handler: this.onBtnSavePassword, scope:this})
      .addButton({name:"btnAsgnRoles", disabled:true, handler: this.onBtnAsgnRoles,
          stateManager:{ name:"record_is_clean", dc:"usr" }, scope:this})
      .addButton({name:"btnAsgnGroups", disabled:true, handler: this.onBtnAsgnGroups,
          stateManager:{ name:"record_is_clean", dc:"usr" }, scope:this})
      .addButton({name:"btnDetailsRole", disabled:true, handler: this.onBtnDetailsRole,
          stateManager:{ name:"selected_one", dc:"rol" }, scope:this})
      .addButton({name:"btnDetailsGroup", disabled:true, handler: this.onBtnDetailsGroup,
          stateManager:{ name:"selected_one", dc:"grp" }, scope:this})
      .addDcView("usr", {name:"usrFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_User_Dc$FilterV"})
      .addDcView("usr", {name:"usrList", xtype:"admin_User_Dc$List"})
      .addDcView("usr", {name:"usrEdit", xtype:"admin_User_Dc$Edit"})
      .addDcView("usr", {name:"canvasPassword", preventHeader:true, isCanvas:true, xtype:"admin_User_Dc$ChangePasswordForm", noInsert:true, noUpdate:true})
      .addDcView("rol", {name:"rolList", _hasTitle_:true, xtype:"admin_Role_Dc$List"})
      .addDcView("grp", {name:"grpList", _hasTitle_:true, xtype:"admin_UserGroup_Dc$List"})
      .addWindow({name:"wdwChangePassword", _hasTitle_:true, 
          dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
            items:[ this._elems_.get("btnSavePassword")]}], closeAction:'hide', resizable:true, layout:"fit", modal:true,
        items:[this._elems_.get("canvasPassword")]})
      .addPanel({name:"main", layout:{ type: "card" }, activeItem:0})
      .addPanel({name:"canvas1", preventHeader:true, isCanvas:true, layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      .addPanel({name:"canvas2", preventHeader:true, isCanvas:true, layout:{ type: "border", regionWeights: {north:0,center:0}}, defaults:{split:true}})
      .addPanel({name:"detailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["canvas1", "canvas2"])
      .addChildrenTo("canvas1", ["usrFilter", "usrList"],["west", "center"])
      .addChildrenTo("canvas2", ["usrEdit", "detailsTab"],["north", "center"])
      .addChildrenTo("detailsTab", ["rolList", "grpList"])
      .addToolbarTo("canvas1", "tlbUsrList")
      .addToolbarTo("canvas2", "tlbUsrEdit")
      .addToolbarTo("rolList", "tlbRolList")
      .addToolbarTo("grpList", "tlbGrpList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbUsrList", {dc: "usr"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addEdit().addNew().addCopy().addDelete()
        .addReports()
      .end().beginToolbar("tlbUsrEdit", {dc: "usr"})
        .addTitle().addSeparator().addSeparator()
        .addBack().addSave().addNew().addCopy().addCancel().addPrevRec().addNextRec()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnChangePassword") ])
        .addReports()
      .end().beginToolbar("tlbRolList", {dc: "rol"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnAsgnRoles") ,this._elems_.get("btnDetailsRole") ])
        .addReports()
      .end().beginToolbar("tlbGrpList", {dc: "grp"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addAutoLoad()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnAsgnGroups") ,this._elems_.get("btnDetailsGroup") ])
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnChangePassword
   */
  onBtnChangePassword: function() {
    this._getWindow_("wdwChangePassword").show();
  },
  
  /**
   * On-Click handler for button btnSavePassword
   */
  onBtnSavePassword: function() {
    var successFn = function(dc,response,serviceName,specs) {
      this._getWindow_("wdwChangePassword").close();
    };
    var o={
      name:"changePassword",
      callbacks:{
        successFn: successFn,
        successScope: this
      },
      modal:true
    };
    this._getDc_("usr").doRpcData(o);
  },
  
  /**
   * On-Click handler for button btnAsgnRoles
   */
  onBtnAsgnRoles: function() {
    this._showAsgnWindow_("seava.mod.admin.ui.extjs.generated.asgn.User_Role_Asgn$Window" ,{dc: "usr", objectIdField: "id"
    ,listeners: {close: {scope: this, fn: function() { this._getDc_("rol").doQuery();
    }} }});
  },
  
  /**
   * On-Click handler for button btnAsgnGroups
   */
  onBtnAsgnGroups: function() {
    this._showAsgnWindow_("seava.mod.admin.ui.extjs.generated.asgn.User_UserGroup_Asgn$Window" ,{dc: "usr", objectIdField: "id"
    ,listeners: {close: {scope: this, fn: function() { this._getDc_("grp").doQuery();
    }} }});
  },
  
  /**
   * On-Click handler for button btnDetailsRole
   */
  onBtnDetailsRole: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.Roles_Ui",{
      params: {
        id: this._getDc_("rol").getRecord().get("id"),
        code: this._getDc_("rol").getRecord().get("code")
      },
      callback: function (params) {
        this._when_called_for_details(params);
      }
    });
  },
  
  /**
   * On-Click handler for button btnDetailsGroup
   */
  onBtnDetailsGroup: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.UserGroups_Ui",{
      params: {
        id: this._getDc_("grp").getRecord().get("id"),
        code: this._getDc_("grp").getRecord().get("code")
      },
      callback: function (params) {
        this._when_called_for_details(params);
      }
    });
  },
  
  _when_called_for_details: function(params) {
    
    var dc = this._getDc_("usr");
    dc.setFilterValue("id", params.id);
    dc.setFilterValue("code", params.code);
    dc.doQuery();
    dc.doEditIn();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.Users_Ui"
});
