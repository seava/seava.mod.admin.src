/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Attachment_Dc$Create", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnUpload", _enableFn_: function(dc, rec) { return rec.data.category != 'link' ; } ,listeners:{
        click:{scope:this, fn:this._createContinue_}
      }, text: "Select file"})
      .addButton({name:"btnSave", _enableFn_: function(dc, rec) { return rec.data.category == 'link' ; } ,listeners:{
        click:{scope:this, fn:this._save_}
      }, text: "Save"})
      .addButton({name:"btnCancel",listeners:{
        click:{scope:this, fn:this._cancel_}
      }, text: "Cancel"})
      .addLov({name:"type", bind: "{d.type}", allowBlank:false,listeners:{
        change:{scope:this, fn:this.onTypeChange}
      }, xtype:"seava.mod.admin.ui.extjs.generated.lov.AttachmentTypes_Lov",
        retFieldMapping: [{lovField:"id", dsField: "typeId"} ,{lovField:"category", dsField: "category"} ],
        filterFieldMapping: [{lovField:"targetAlias", dsField: "targetAlias"}, {lovField:"targetType", dsField: "targetType"} ]})
      .addTextField({ name:"name", bind: "{d.name}", maxLength: 255})
      .addTextArea({ name:"notes", bind: "{d.notes}", height:80})
      .addTextField({ name:"location", bind: "{d.location}", _enableFn_: function(dc, rec) { return rec.data.category == 'link' ; } , maxLength: 4000})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"},  xtype:"panel", buttonAlign:"center", buttons: [this._getConfig_("btnUpload"),this._getConfig_("btnSave"),this._getConfig_("btnCancel")]})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["type", "name", "location", "notes" ])
  },
  
  onTypeChange: function() {
    var rec = this._getController_().getRecord();		 
    this._setFieldsEnabledState_(["location","btnUpload","btnSave"], rec);
  },
  
  _save_: function() {
    
    if (this.isValid()) { 
    	this._controller_.doSave();
    }
  },
  
  _cancel_: function() {
    
    this.up("window").hide();
    this._controller_.doCancel();
  },
  
  _createContinue_: function() {
    
    var rd = this._controller_.record.data;
    (new seava.lib.e4e.js.base.FileUploadWindow(
    	{
    		_handler_ : "uploadAttachment",
    		_fields_ : {							 
    			a_id : {
    				xtype : "hidden",
    				value : rd.id
    			},
    			a_name : {
    				xtype : "hidden",
    				value : rd.name
    			},
    			a_type : {
    				xtype : "hidden",
    				value : rd.type
    			},
    			a_typeId : {
    				xtype : "hidden",
    				value : rd.typeId
    			},
    			a_notes : {
    				xtype : "hidden",
    				value : rd.notes
    			},
    			a_category : {
    				xtype : "hidden",
    				value : rd.category
    			},
    			a_targetAlias : {
    				xtype : "hidden",
    				value : rd.targetAlias
    			},
    			a_targetRefid : {
    				xtype : "hidden",
    				value : rd.targetRefid
    			},
    			a_targetType : {
    				xtype : "hidden",
    				value : rd.targetType
    			}
    		},
    		_succesCallbackScope_ : this._controller_,
    		_succesCallbackFn_ : function() {
    			this.doCancel();
    			if (this._doNewWdw_ != null) {this._doNewWdw_.close();}
    			(new Ext.util.DelayedTask(this.doQuery, this)).delay(100);
    		}
    	})).show();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditForm",
  alias: "widget.admin_Attachment_Dc$Create"
});
