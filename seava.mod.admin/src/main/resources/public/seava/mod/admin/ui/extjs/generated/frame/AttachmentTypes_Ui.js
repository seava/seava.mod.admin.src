/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.AttachmentTypes_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("attchType", Ext.create(seava.mod.admin.ui.extjs.generated.dc.AttachmentType_Dc,{multiEdit: true,  autoLoad:true}))
      .addDc("targetRule", Ext.create(seava.mod.admin.ui.extjs.generated.dc.TargetRule_Dc,{multiEdit: true}))
      .linkDc("targetRule", "attchType",{fetchMode:"auto",
        fields:[{childField:"sourceRefId", parentField:"refid"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addDcView("attchType", {name:"attchTypeFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_AttachmentType_Dc$FilterV"})
      .addDcView("attchType", {name:"attchTypeEditList", xtype:"admin_AttachmentType_Dc$EditList"})
      .addDcView("targetRule", {name:"targetRuleEditList", height:240, xtype:"admin_TargetRule_Dc$EditList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:2,center:0,south:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["attchTypeFilter", "attchTypeEditList", "targetRuleEditList"],["west", "center", "south"])
      .addToolbarTo("main", "tlbAttchTypeEditList")
      .addToolbarTo("targetRuleEditList", "tlbTargetRuleEditList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbAttchTypeEditList", {dc: "attchType"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addReports()
      .end().beginToolbar("tlbTargetRuleEditList", {dc: "targetRule"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addAutoLoad()
        .addReports()
      .end();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.AttachmentTypes_Ui"
});
