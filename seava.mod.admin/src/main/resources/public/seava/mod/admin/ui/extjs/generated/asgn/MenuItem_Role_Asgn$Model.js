/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.asgn.MenuItem_Role_Asgn$Model", {
  extend: 'Ext.data.Model',
  statics: {
    ALIAS: "ad_MenuItem_Role_Asgn"
  },
  fields:  [
    {name:"id", type:"string"},
    {name:"code", type:"string"},
    {name:"name", type:"string"},
    {name:"description", type:"string"}
  ]
});
