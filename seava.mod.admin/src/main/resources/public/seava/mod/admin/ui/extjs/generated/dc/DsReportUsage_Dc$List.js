/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.DsReportUsage_Dc$List", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"report", dataIndex:"report"})
      .addTextColumn({ name:"reportName", dataIndex:"reportName", noEdit:true })
      .addTextColumn({ name:"dataSource", dataIndex:"dataSource"})
      .addTextColumn({ name:"frameName", dataIndex:"frameName"})
      .addTextColumn({ name:"toolbarKey", dataIndex:"toolbarKey", width:120})
      .addTextColumn({ name:"dcKey", dataIndex:"dcKey", width:120})
      .addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", width:80})
      .addTextColumn({ name:"dsReportId", dataIndex:"dsReportId", hidden:true, noEdit:true })
      .addTextColumn({ name:"reportId", dataIndex:"reportId", hidden:true, noEdit:true })
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.admin_DsReportUsage_Dc$List"
});
