Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.ViewState_Ds", {
  active__lbl: "Active",
  clientId__lbl: "Client Id",
  cmpType__lbl: "Cmp Type",
  cmp__lbl: "Cmp",
  createdAt__lbl: "Created At",
  createdBy__lbl: "Created By",
  description__lbl: "Description",
  entityAlias__lbl: "Entity Alias",
  entityFqn__lbl: "Entity Fqn",
  id__lbl: "Id",
  modifiedAt__lbl: "Modified At",
  modifiedBy__lbl: "Modified By",
  name__lbl: "Name",
  notes__lbl: "Notes",
  refid__lbl: "Refid",
  value__lbl: "Value",
  version__lbl: "Version"
});
