/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.AccessControlsDs_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("dsAccess", Ext.create(seava.mod.admin.ui.extjs.generated.dc.AccessControlDs_Dc,{multiEdit: true,  autoLoad:true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addDcView("dsAccess", {name:"dsAccessFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_AccessControlDs_Dc$FilterV"})
      .addDcView("dsAccess", {name:"dsAccessEditList", xtype:"admin_AccessControlDs_Dc$EditList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["dsAccessFilter", "dsAccessEditList"],["west", "center"])
      .addToolbarTo("main", "tlbDsAccessEditList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbDsAccessEditList", {dc: "dsAccess"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addReports()
      .end();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.AccessControlsDs_Ui"
});
