Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.FrameExtension_Ds", {
  active__lbl: "Active?",
  clientId__lbl: "Client Id",
  createdAt__lbl: "Created At",
  createdBy__lbl: "Created By",
  entityAlias__lbl: "Entity Alias",
  entityFqn__lbl: "Entity Fqn",
  fileLocation__lbl: "File Location",
  frame__lbl: "Frame",
  id__lbl: "Id",
  modifiedAt__lbl: "Modified At",
  modifiedBy__lbl: "Modified By",
  notes__lbl: "Notes",
  refid__lbl: "Refid",
  relativePath__lbl: "Relative Path",
  sequenceNo__lbl: "Sequence No",
  version__lbl: "Version"
});
