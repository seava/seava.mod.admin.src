/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.AttachmentType_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"name", bind: "{d.name}", xtype:"seava.mod.admin.ui.extjs.generated.lov.AttachmentTypes_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      .addCombo({ name:"category", bind: "{d.category}", store:["link","upload"]})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:60}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["name", "category", "active" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_AttachmentType_Dc$FilterV"
});
