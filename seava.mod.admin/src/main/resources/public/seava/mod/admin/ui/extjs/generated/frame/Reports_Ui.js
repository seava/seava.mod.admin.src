/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.Reports_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("rep", Ext.create(seava.mod.admin.ui.extjs.generated.dc.Report_Dc,{ autoLoad:true}))
      .addDc("params", Ext.create(seava.mod.admin.ui.extjs.generated.dc.ReportParam_Dc,{multiEdit: true}))
      .addDc("paramsRt", Ext.create(seava.mod.admin.ui.extjs.generated.dc.ReportParamRt_Dc,{multiEdit: true}))
      .addDc("dsrep", Ext.create(seava.mod.admin.ui.extjs.generated.dc.DsReport_Dc,{multiEdit: true}))
      .addDc("dsparam", Ext.create(seava.mod.admin.ui.extjs.generated.dc.DsReportParam_Dc,{multiEdit: true}))
      .addDc("usage", Ext.create(seava.mod.admin.ui.extjs.generated.dc.DsReportUsageCtx_Dc,{multiEdit: true}))
      .linkDc("params", "rep",{fetchMode:"auto",
        fields:[{childField:"reportId", parentField:"id"}]})
      .linkDc("paramsRt", "rep",{
        fields:[{childField:"reportId", parentField:"id"}]})
      .linkDc("dsrep", "rep",{fetchMode:"auto",
        fields:[{childField:"reportId", parentField:"id"}]})
      .linkDc("dsparam", "dsrep",{fetchMode:"auto",
        fields:[{childField:"dsReportId", parentField:"id"}, {childField:"reportId", parentField:"reportId"}, {childField:"dataSource", parentField:"dataSource"}]})
      .linkDc("usage", "dsrep",{fetchMode:"auto",
        fields:[{childField:"dsReportId", parentField:"id"}, {childField:"reportId", parentField:"reportId"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnTestReport", disabled:false, iconCls:"icon-action-run", handler: this.onBtnTestReport, scope:this})
      .addButton({name:"btnRunReport", disabled:false, handler: this.onBtnRunReport, scope:this})
      .addButton({name:"btnCancelReport", disabled:false, iconCls:"icon-action-reset", handler: this.onBtnCancelReport, scope:this})
      .addDcView("rep", {name:"repFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_Report_Dc$FilterV"})
      .addDcView("rep", {name:"repList", xtype:"admin_Report_Dc$List"})
      .addDcView("rep", {name:"repEdit", xtype:"admin_Report_Dc$Edit"})
      .addDcView("params", {name:"paramEditList", _hasTitle_:true, xtype:"admin_ReportParam_Dc$CtxEditList"})
      .addDcView("paramsRt", {name:"paramTest", xtype:"admin_ReportParamRt_Dc$EditList"})
      .addDcView("dsrep", {name:"dsrepList", width:600, xtype:"admin_DsReport_Dc$ReportCtxList"})
      .addDcView("dsparam", {name:"dsparamList", _hasTitle_:true, xtype:"admin_DsReportParam_Dc$CtxEditList"})
      .addDcView("usage", {name:"usageList", _hasTitle_:true, xtype:"admin_DsReportUsageCtx_Dc$EditList"})
      .addWindow({name:"wdwTestReport", _hasTitle_:true, width:450, height:350,  closable:false, 
          dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
            items:[ this._elems_.get("btnRunReport"), this._elems_.get("btnCancelReport")]}], closeAction:'hide', resizable:true, layout:"fit", modal:true,
        items:[this._elems_.get("paramTest")]})
      .addPanel({name:"main", layout:{ type: "card" }, activeItem:0})
      .addPanel({name:"canvas1", preventHeader:true, isCanvas:true, layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      .addPanel({name:"canvas2", preventHeader:true, isCanvas:true, layout:{ type: "border", regionWeights: {north:0,center:0}}, defaults:{split:true}})
      .addPanel({name:"repDetailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
      .addPanel({name:"dsRep", _hasTitle_:true, layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      .addPanel({name:"dsrepDetailsTab", xtype:"tabpanel", activeTab:0, plain:false, deferredRender:false})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["canvas1", "canvas2"])
      .addChildrenTo("canvas1", ["repFilter", "repList"],["west", "center"])
      .addChildrenTo("canvas2", ["repEdit", "repDetailsTab"],["north", "center"])
      .addChildrenTo("repDetailsTab", ["paramEditList", "dsRep"])
      .addChildrenTo("dsRep", ["dsrepList", "dsrepDetailsTab"],["west", "center"])
      .addChildrenTo("dsrepDetailsTab", ["dsparamList", "usageList"])
      .addToolbarTo("canvas1", "tlbRepList")
      .addToolbarTo("canvas2", "tlbRepEdit")
      .addToolbarTo("paramEditList", "tlbParamList")
      .addToolbarTo("dsrepList", "tlbDsRepList")
      .addToolbarTo("dsparamList", "tlbDsParamList")
      .addToolbarTo("usageList", "tlbUsageList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbRepList", {dc: "rep"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addEdit().addNew().addCopy().addDelete()
        .addReports()
      .end().beginToolbar("tlbRepEdit", {dc: "rep"})
        .addTitle().addSeparator().addSeparator()
        .addBack().addSave().addNew().addCopy().addCancel().addPrevRec().addNextRec()
        .addReports()
      .end().beginToolbar("tlbParamList", {dc: "params"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addAutoLoad()
        .addReports()
      .end().beginToolbar("tlbDsRepList", {dc: "dsrep"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addAutoLoad()
        .addReports()
      .end().beginToolbar("tlbDsParamList", {dc: "dsparam"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addAutoLoad()
        .addReports()
      .end().beginToolbar("tlbUsageList", {dc: "usage"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addAutoLoad()
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnTestReport
   */
  onBtnTestReport: function() {
    this._getWindow_("wdwTestReport").show();
    this._getDc_("paramsRt").doQuery();
  },
  
  /**
   * On-Click handler for button btnRunReport
   */
  onBtnRunReport: function() {
    this._runReport_();
  },
  
  /**
   * On-Click handler for button btnCancelReport
   */
  onBtnCancelReport: function() {
    this._getDc_("paramsRt").doCancel();
    this._getWindow_("wdwTestReport").close();
  },
  
  _runReport_: function() {
    var paramsDc = this._getDc_("paramsRt")
    			serverUrl = this._getDc_("rep").record.data.serverUrl;
    		
    		var qs = "";
    		
    		paramsDc.store.data.each(function(item, idx,len) {
    			if(qs != "") {
    				qs += "&";
    			}
    			qs += item.get("code") + "=" + item.get("value");
    		});
    		window.open(serverUrl + "?" + qs,"Test-report","")
    		.focus();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.Reports_Ui"
});
