Ext.define("seava.mod.admin.i18n.extjs.en.generated.frame.MenuItems_Ui", {
  /* view */
  menuitemFilter__ttl: "Filter",
  /* menu */
  tlbMenuitemList__ttl: "Menu items",
  /* button */
  btnAsgnItemRoles__lbl: "Assign-Roles",
  btnAsgnItemRoles__tlp: "Assign roles to the selected menu item",
  
  title: "Menu items"
});
