/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define( "seava.mod.admin.ui.extjs.generated.asgn.Role_MenuItem_Asgn$List", {
  extend: "seava.lib.e4e.js.asgn.AbstractAsgnGrid",
  alias:[ "widget.Role_MenuItem_AsgnList$Left","widget.Role_MenuItem_AsgnList$Right" ],
  _defineColumns_: function () {
    this._getBuilder_()
    .addTextColumn({name:"id", dataIndex:"id", hidden:true})
    .addTextColumn({name:"name", dataIndex:"name"})
    .addTextColumn({name:"title", dataIndex:"title"})
  } 
});
