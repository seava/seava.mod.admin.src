/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.MenuItemLov_DsParam", {
  extend: 'Ext.data.Model',
  
  fields: [
    {name:"foldersOnly", type:"boolean", forFilter:true},
    {name:"framesOnly", type:"boolean", forFilter:true}
  ]
});
