/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.ViewState_Dc$List", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"name", dataIndex:"name", width:150})
      .addTextColumn({ name:"cmpType", dataIndex:"cmpType", width:120})
      .addTextColumn({ name:"cmp", dataIndex:"cmp", width:400})
      .addTextColumn({ name:"value", dataIndex:"value", hidden:true})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.admin_ViewState_Dc$List"
});
