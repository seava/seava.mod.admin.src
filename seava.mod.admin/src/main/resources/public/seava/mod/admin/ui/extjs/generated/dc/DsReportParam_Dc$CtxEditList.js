/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.DsReportParam_Dc$CtxEditList", {
  _noExport_:true,
  _noImport_:true,
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({ name:"paramName", dataIndex:"paramName", editor: {xtype: "seava.mod.admin.ui.extjs.generated.lov.ReportParams_Lov",
        retFieldMapping: [{lovField:"id", dsField: "paramId"} ,{lovField:"title", dsField: "paramTitle"} ],
        filterFieldMapping: [{lovField:"reportId", dsField: "reportId"} ]}})
      .addTextColumn({ name:"paramTitle", dataIndex:"paramTitle", noEdit:true })
      .addLov({ name:"dsField", dataIndex:"dsField", editor: {xtype: "seava.mod.system.ui.extjs.generated.lov.DataSourceFields_Lov",
        filterFieldMapping: [{lovField:"dataSourceName", dsField: "dataSource"} ]}})
      .addTextColumn({ name:"staticValue", dataIndex:"staticValue", editor: {xtype: "textfield"}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_DsReportParam_Dc$CtxEditList"
});
