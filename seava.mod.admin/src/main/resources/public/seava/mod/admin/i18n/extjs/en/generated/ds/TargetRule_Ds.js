Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.TargetRule_Ds", {
  clientId__lbl: "Client Id",
  createdAt__lbl: "Created At",
  createdBy__lbl: "Created By",
  entityAlias__lbl: "Entity Alias",
  entityFqn__lbl: "Entity Fqn",
  id__lbl: "Id",
  modifiedAt__lbl: "Modified At",
  modifiedBy__lbl: "Modified By",
  notes__lbl: "Notes",
  refid__lbl: "Refid",
  sourceRefId__lbl: "Source Ref Id",
  targetAlias__lbl: "Entity alias",
  targetType__lbl: "Object type",
  version__lbl: "Version"
});
