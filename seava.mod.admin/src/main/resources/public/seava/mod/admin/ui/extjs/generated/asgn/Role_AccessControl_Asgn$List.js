/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define( "seava.mod.admin.ui.extjs.generated.asgn.Role_AccessControl_Asgn$List", {
  extend: "seava.lib.e4e.js.asgn.AbstractAsgnGrid",
  alias:[ "widget.Role_AccessControl_AsgnList$Left","widget.Role_AccessControl_AsgnList$Right" ],
  _defineColumns_: function () {
    this._getBuilder_()
    .addTextColumn({name:"id", dataIndex:"id", hidden:true})
    .addTextColumn({name:"name", dataIndex:"name", width:120})
    .addTextColumn({name:"description", dataIndex:"description"})
  } 
});
