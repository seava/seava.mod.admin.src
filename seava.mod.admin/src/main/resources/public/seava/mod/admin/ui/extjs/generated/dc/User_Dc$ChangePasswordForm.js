/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.User_Dc$ChangePasswordForm", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextField({ name:"newPassword", paramIndex:"newPassword", bind: "{p.newPassword}", allowBlank:false, inputType:"password", maxLength: 255})
      .addTextField({ name:"confirmPassword", paramIndex:"confirmPassword", bind: "{p.confirmPassword}", allowBlank:false, inputType:"password", maxLength: 255})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, width:350, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:130}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["newPassword", "confirmPassword" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditForm",
  alias: "widget.admin_User_Dc$ChangePasswordForm"
});
