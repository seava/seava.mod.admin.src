Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.ViewStateRtLov_Ds", {
  active__lbl: "Active",
  clientId__lbl: "Client Id",
  cmpType__lbl: "Cmp Type",
  cmp__lbl: "Cmp",
  description__lbl: "Description",
  id__lbl: "Id",
  name__lbl: "Name",
  owner__lbl: "Created By",
  refid__lbl: "Refid",
  value__lbl: "Value"
});
