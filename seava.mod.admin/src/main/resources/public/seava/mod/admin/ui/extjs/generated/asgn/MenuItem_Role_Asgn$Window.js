/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.asgn.MenuItem_Role_Asgn$Window", {
  extend: "seava.lib.e4e.js.asgn.AbstractAsgnUi",
  width:800,
  height:400,
  title:"Assign roles to menu-item",
  _filterFields_: [
    ["id"],
    ["code"],
    ["name"]
  ],
  _defaultFilterField_ : "code",

  _defineElements_: function () {
    this._getBuilder_()
      .addLeftGrid({ xtype:"MenuItem_Role_AsgnList$Left"})
      .addRightGrid({ xtype:"MenuItem_Role_AsgnList$Right"})
  }
});
