/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.ViewStates_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("viewstate", Ext.create(seava.mod.admin.ui.extjs.generated.dc.ViewState_Dc,{ autoLoad:true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addDcView("viewstate", {name:"viewstateFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_ViewState_Dc$FilterV"})
      .addDcView("viewstate", {name:"viewstateList", xtype:"admin_ViewState_Dc$List"})
      .addDcView("viewstate", {name:"viewstateViewState", height:180, xtype:"admin_ViewState_Dc$Value"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:2,center:0,south:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["viewstateFilter", "viewstateList", "viewstateViewState"],["west", "center", "south"])
      .addToolbarTo("main", "tlbViewstateList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbViewstateList", {dc: "viewstate"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addDelete()
        .addReports()
      .end();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.ViewStates_Ui"
});
