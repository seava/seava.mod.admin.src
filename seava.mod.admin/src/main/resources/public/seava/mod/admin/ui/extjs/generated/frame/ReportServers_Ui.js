/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.ReportServers_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("m", Ext.create(seava.mod.admin.ui.extjs.generated.dc.ReportServer_Dc,{multiEdit: true,  autoLoad:true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addDcView("m", {name:"mFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_ReportServer_Dc$FilterV"})
      .addDcView("m", {name:"mEditList", xtype:"admin_ReportServer_Dc$EditList"})
      .addPanel({name:"main", layout:{ type: "card" }, activeItem:0})
      .addPanel({name:"canvas1", preventHeader:true, isCanvas:true, layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["canvas1"])
      .addChildrenTo("canvas1", ["mFilter", "mEditList"],["west", "center"])
      .addToolbarTo("canvas1", "tlbMEditList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbMEditList", {dc: "m"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addReports()
      .end();
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.ReportServers_Ui"
});
