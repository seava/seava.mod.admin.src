Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.ParamValue_Ds", {
  clientId__lbl: "Client Id",
  createdAt__lbl: "Created At",
  createdBy__lbl: "Created By",
  entityAlias__lbl: "Entity Alias",
  entityFqn__lbl: "Entity Fqn",
  id__lbl: "Id",
  modifiedAt__lbl: "Modified At",
  modifiedBy__lbl: "Modified By",
  notes__lbl: "Notes",
  refid__lbl: "Refid",
  sysParam__lbl: "Sys Param",
  validFrom__lbl: "Valid From",
  validTo__lbl: "Valid To",
  value__lbl: "Value",
  version__lbl: "Version"
});
