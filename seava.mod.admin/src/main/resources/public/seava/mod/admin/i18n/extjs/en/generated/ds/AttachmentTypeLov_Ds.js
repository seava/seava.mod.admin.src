Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.AttachmentTypeLov_Ds", {
  active__lbl: "Active",
  category__lbl: "Category",
  clientId__lbl: "Client Id",
  description__lbl: "Description",
  id__lbl: "Id",
  name__lbl: "Name",
  refid__lbl: "Refid",
  targetAlias__lbl: "Target Alias",
  targetType__lbl: "Target Type"
});
