/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Menu_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"name", bind: "{d.name}", xtype:"seava.mod.admin.ui.extjs.generated.lov.Menus_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addTextField({ name:"title", bind: "{d.title}", maxLength: 255})
      .addCombo({ name:"tag", bind: "{d.tag}", store:["top","left"]})
      .addBooleanField({ name:"active", bind: "{d.active}", width:170})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:60}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["name", "title", "tag", "active" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_Menu_Dc$FilterV"
});
