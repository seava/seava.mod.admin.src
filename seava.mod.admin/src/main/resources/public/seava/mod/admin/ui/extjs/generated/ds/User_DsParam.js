/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.User_DsParam", {
  extend: 'Ext.data.Model',
  
  fields: [
    {name:"confirmPassword", type:"string"},
    {name:"inGroup", type:"string", forFilter:true},
    {name:"inGroupId", type:"string", forFilter:true},
    {name:"newPassword", type:"string"},
    {name:"withRole", type:"string", forFilter:true},
    {name:"withRoleId", type:"string", forFilter:true}
  ]
});
