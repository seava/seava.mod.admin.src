/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.AccessControlAsgn_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"asgnName", bind: "{d.asgnName}", xtype:"seava.mod.system.ui.extjs.generated.lov.DataSourcesAsgn_Lov"})
      .addLov({name:"accessControl", bind: "{d.accessControl}", xtype:"seava.mod.admin.ui.extjs.generated.lov.AccessControls_Lov",
        retFieldMapping: [{lovField:"id", dsField: "accessControlId"} ]})
      .addBooleanField({ name:"queryAllowed", bind: "{d.queryAllowed}"})
      .addBooleanField({ name:"updateAllowed", bind: "{d.updateAllowed}"})
      .addBooleanField({ name:"importAllowed", bind: "{d.importAllowed}"})
      .addBooleanField({ name:"exportAllowed", bind: "{d.exportAllowed}"})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:70}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["accessControl", "asgnName", "queryAllowed", "updateAllowed" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_AccessControlAsgn_Dc$FilterV"
});
