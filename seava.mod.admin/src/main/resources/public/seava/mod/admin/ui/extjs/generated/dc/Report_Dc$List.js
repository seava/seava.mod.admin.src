/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Report_Dc$List", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"name", dataIndex:"name", width:200})
      .addTextColumn({ name:"code", dataIndex:"code", width:150})
      .addTextColumn({ name:"notes", dataIndex:"notes", width:200})
      .addTextColumn({ name:"reportServer", dataIndex:"reportServer", width:150})
      .addTextColumn({ name:"contextPath", dataIndex:"contextPath", width:150})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addTextColumn({ name:"reportServerId", dataIndex:"reportServerId", hidden:true})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.admin_Report_Dc$List"
});
