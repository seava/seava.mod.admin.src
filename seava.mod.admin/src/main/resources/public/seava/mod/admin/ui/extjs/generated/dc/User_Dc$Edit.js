/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.User_Dc$Edit", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextField({ name:"code", bind: "{d.code}", allowBlank:false, noUpdate:true, maxLength: 64})
      .addTextField({ name:"name", bind: "{d.name}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"loginName", bind: "{d.loginName}", allowBlank:false, maxLength: 255})
      .addTextField({ name:"email", bind: "{d.email}", maxLength: 128})
      .addTextArea({ name:"notes", bind: "{d.notes}", height:60})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      .addBooleanField({ name:"locked", bind: "{d.locked}"})
      .addCombo({ name:"decimalSeparator", bind: "{d.decimalSeparator}", store:[".",","]})
      .addCombo({ name:"thousandSeparator", bind: "{d.thousandSeparator}", store:[".",","]})
      .addLov({name:"dateFormat", bind: "{d.dateFormat}", xtype:"seava.mod.system.ui.extjs.generated.lov.DateFormats_Lov",
        retFieldMapping: [{lovField:"id", dsField: "dateFormatId"} ]})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col1", width:300, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}})
      .addPanel({ name:"col2", width:170, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}})
      .addPanel({ name:"col3", _hasTitle_: true, width:300, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"},  xtype:"fieldset", collapsible:true, border:true, margin:"0 10 0 0", defaults: { labelWidth:120}})
      .addPanel({ name:"col4", width:300, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelAlign:"top"}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["col1", "col2", "col3", "col4" ])
       .addChildrenTo("col1", ["name", "code", "loginName", "email" ])
       .addChildrenTo("col2", ["active", "locked" ])
       .addChildrenTo("col3", ["dateFormat", "decimalSeparator", "thousandSeparator" ])
       .addChildrenTo("col4", ["notes" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditForm",
  alias: "widget.admin_User_Dc$Edit"
});
