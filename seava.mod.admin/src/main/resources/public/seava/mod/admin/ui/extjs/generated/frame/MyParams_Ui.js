/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.MyParams_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("param", Ext.create(seava.mod.admin.ui.extjs.generated.dc.MyParam_Dc,{ autoLoad:true}))
      .addDc("val", Ext.create(seava.mod.admin.ui.extjs.generated.dc.ParamValue_Dc,{multiEdit: true}))
      .linkDc("val", "param",{fetchMode:"auto",
        fields:[{childField:"sysParam", parentField:"code"}]})
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnPublish", disabled:false, handler: this.onBtnPublish, scope:this})
      .addButton({name:"btnShowValuesFrame", disabled:false, handler: this.onBtnShowValuesFrame, scope:this})
      .addButton({name:"btnInitVal", disabled:true, handler: this.onBtnInitVal,
          stateManager:{ name:"selected_not_zero", dc:"param" }, scope:this})
      .addButton({name:"btnOk", disabled:false, handler: this.onBtnOk, scope:this})
      .addDcView("param", {name:"paramFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_MyParam_Dc$FilterV"})
      .addDcView("param", {name:"paramList", xtype:"admin_MyParam_Dc$List"})
      .addDcView("param", {name:"initvalForm", xtype:"admin_MyParam_Dc$InitValuesDateRange"})
      .addDcView("val", {name:"valList", height:250, xtype:"admin_ParamValue_Dc$CtxEditList"})
      .addWindow({name:"wdwInitVal", _hasTitle_:true, width:350, height:150, 
          dockedItems:[{xtype:"toolbar", ui:"footer", dock:'bottom', weight:-1,
            items:[ this._elems_.get("btnOk")]}], closeAction:'hide', resizable:true, layout:"fit", modal:true,
        items:[this._elems_.get("initvalForm")]})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:2,center:0,south:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["paramFilter", "paramList", "valList"],["west", "center", "south"])
      .addToolbarTo("main", "tlbParamList")
      .addToolbarTo("valList", "tlbValList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbParamList", {dc: "param"})
        .addTitle().addSeparator().addSeparator()
        .addQuery()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnPublish") ,this._elems_.get("btnInitVal") ,this._elems_.get("btnShowValuesFrame") ])
        .addReports()
      .end().beginToolbar("tlbValList", {dc: "val"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addAutoLoad()
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnPublish
   */
  onBtnPublish: function() {
    var o={
      name:"publishValues",
      modal:true
    };
    this._getDc_("val").doRpcFilter(o);
  },
  
  /**
   * On-Click handler for button btnShowValuesFrame
   */
  onBtnShowValuesFrame: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.ParamValues_Ui");
  },
  
  /**
   * On-Click handler for button btnInitVal
   */
  onBtnInitVal: function() {
    this._getWindow_("wdwInitVal").show();
  },
  
  /**
   * On-Click handler for button btnOk
   */
  onBtnOk: function() {
    var successFn = function(dc,response,serviceName,specs) {
      this._getWindow_("wdwInitVal").close();
      this._getDc_("val").doQuery();
    };
    var o={
      name:"createInitialValues",
      callbacks:{
        successFn: successFn,
        successScope: this
      },
      modal:true
    };
    this._getDc_("param").doRpcDataList(o);
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.MyParams_Ui"
});
