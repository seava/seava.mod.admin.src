/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.AccessControl_Dc$List", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"name", dataIndex:"name"})
      .addTextColumn({ name:"description", dataIndex:"description", width:300})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.admin_AccessControl_Dc$List"
});
