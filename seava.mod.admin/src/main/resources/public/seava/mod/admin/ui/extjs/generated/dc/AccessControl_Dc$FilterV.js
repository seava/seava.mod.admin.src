/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.AccessControl_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"name", bind: "{d.name}", xtype:"seava.mod.admin.ui.extjs.generated.lov.AccessControls_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      .addLov({name:"withRole", paramIndex:"withRole", bind: "{p.withRole}", xtype:"seava.mod.admin.ui.extjs.generated.lov.Roles_Lov",
        retFieldMapping: [{lovField:"id", dsParam: "withRoleId"} ]})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:70}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["name", "withRole", "active" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_AccessControl_Dc$FilterV"
});
