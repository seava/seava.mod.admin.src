/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Attachment_Dc", {
  extend: "seava.lib.e4e.js.dc.AbstractDcController",
  
  doNew: function() {
    
    if (this._doNewWdw_ == null ) {
    	this._doNewWdw_ = Ext.create("Ext.window.Window", {
    		width:500, 
    		height:280, 
    		closable: false,
    		closeAction: "hide",
    		resizable:true, 
    		layout:"fit", 
    		modal:true,
        title:"Attachment",
    		items: {
    			_controller_:this,
    			xtype:"admin_Attachment_Dc$Create",
          id:Ext.id()
    		}	
    	});
    }
    this._doNewWdw_.show();
    this.commands.doNew.execute();
  },
  
  afterDoSaveSuccess: function() {
    if (this._doNewWdw_ != null) {this._doNewWdw_.close();}
  },
  
  recordModel: "seava.mod.admin.ui.extjs.generated.ds.Attachment_Ds"
});
