/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.MenuItems_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("menuitem", Ext.create(seava.mod.admin.ui.extjs.generated.dc.MenuItem_Dc,{multiEdit: true,  autoLoad:true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnAsgnItemRoles", disabled:true, handler: this.onBtnAsgnItemRoles,
          stateManager:{ name:"selected_one_clean", dc:"menuitem" }, scope:this})
      .addDcView("menuitem", {name:"menuitemFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_MenuItem_Dc$FilterV"})
      .addDcView("menuitem", {name:"menuitemList", xtype:"admin_MenuItem_Dc$EditList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["menuitemFilter", "menuitemList"],["west", "center"])
      .addToolbarTo("main", "tlbMenuitemList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbMenuitemList", {dc: "menuitem"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnAsgnItemRoles") ])
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnAsgnItemRoles
   */
  onBtnAsgnItemRoles: function() {
    this._showAsgnWindow_("seava.mod.admin.ui.extjs.generated.asgn.MenuItem_Role_Asgn$Window" ,{dc: "menuitem", objectIdField: "id"});
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.MenuItems_Ui"
});
