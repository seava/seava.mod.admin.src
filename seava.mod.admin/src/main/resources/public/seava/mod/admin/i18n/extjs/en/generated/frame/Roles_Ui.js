Ext.define("seava.mod.admin.i18n.extjs.en.generated.frame.Roles_Ui", {
  /* view */
  aclList__ttl: "Granted privileges",
  menuList__ttl: "Accessible menus",
  miList__ttl: "Accessible menu items",
  rolFilter__ttl: "Filter",
  usrList__ttl: "Users",
  /* menu */
  tlbAclList__ttl: "Granted privileges",
  tlbMenuList__ttl: "Accessible menus",
  tlbMiList__ttl: "Accessible menu items",
  tlbRolList__ttl: "Roles",
  tlbUsrList__ttl: "Users",
  /* button */
  btnAsgnRoleToAccessCtrl__lbl: "Assign privileges",
  btnAsgnRoleToAccessCtrl__tlp: "Assign privileges to the selected role",
  btnAsgnRoleToMenu__lbl: "Assign menus",
  btnAsgnRoleToMenu__tlp: "Assign menus to the selected role",
  btnAsgnRoleToMenuItem__lbl: "Assign menu items",
  btnAsgnRoleToMenuItem__tlp: "Assign menu items to the selected role",
  btnAsgnRoleToUsers__lbl: "Assign users",
  btnAsgnRoleToUsers__tlp: "Assign users to the selected role",
  btnDetailsPrivilege__lbl: "Privilege details",
  btnDetailsPrivilege__tlp: "Open frame to show more details for the selected privilege",
  btnDetailsUser__lbl: "User details",
  btnDetailsUser__tlp: "Open frame to show more details for the selected user",
  
  title: "Roles"
});
