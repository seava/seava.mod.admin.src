/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.lov.ViewStates_Lov", {
  extend: "seava.lib.e4e.js.lov.AbstractCombo",
  alias: "widget.seava.mod.admin.ui.extjs.generated.lov.ViewStates_Lov",
  displayField: "name",
  listConfig: {
    getInnerTpl: function() {
      return '<span>{name}, {description}</span>';
    }
    //width:
  },
  _editFrame_: {
    name: "seava.mod.admin.ui.extjs.generated.frame.ViewStates_Ui"
  },
  triggers : {
    picker : {
      handler : 'onTriggerClick',
      scope : 'this'
    },
    showFrame : {
      handler : 'onTrigger2Click',
      scope : 'this',
      cls : Ext.baseCSSPrefix + 'form-search-trigger'
    }
  },
  recordModel: "seava.mod.admin.ui.extjs.generated.ds.ViewStateLov_Ds"
});
