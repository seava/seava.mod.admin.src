Ext.define("seava.mod.admin.i18n.extjs.en.generated.frame.Menus_Ui", {
  /* view */
  menuFilter__ttl: "Filter",
  /* menu */
  tlbMenuEditList__ttl: "Menus",
  /* button */
  btnAsgnMenuRoles__lbl: "Assign-Roles",
  btnAsgnMenuRoles__tlp: "Assign roles to the selected menu",
  
  title: "Menus"
});
