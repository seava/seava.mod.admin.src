/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.UserGroup_Dc$Filter", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addLov({name:"code", bind: "{d.code}", xtype:"seava.mod.admin.ui.extjs.generated.lov.UserGroups_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addTextField({ name:"name", bind: "{d.name}", maxLength: 255})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col1", width:280, layout: {type:"vbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col2", width:170, layout: {type:"vbox", align:"begin", pack:"start"}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["col1", "col2" ])
       .addChildrenTo("col1", ["code", "name" ])
       .addChildrenTo("col2", ["active" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_UserGroup_Dc$Filter"
});
