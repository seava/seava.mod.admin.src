/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.FrameExtension_Ds", {
  extend: 'Ext.data.Model',
  
  statics: {
    ALIAS: "ad_FrameExtension_Ds"
  },
  
  validators: {
    frame: [{ type: 'presence'}],
    sequenceNo: [{ type: 'presence'}],
    fileLocation: [{ type: 'presence'}]
  },
  
  fields: [
    {name:"frame", type:"string"},
    {name:"sequenceNo", type:"int", useNull:true},
    {name:"fileLocation", type:"string"},
    {name:"relativePath", type:"boolean"},
    {name:"active", type:"boolean"},
    {name:"id", type:"string"},
    {name:"clientId", type:"string"},
    {name:"notes", type:"string"},
    {name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"createdBy", type:"string"},
    {name:"modifiedBy", type:"string"},
    {name:"version", type:"int", useNull:true},
    {name:"refid", type:"string"},
    {name:"entityAlias", type:"string"},
    {name:"entityFqn", type:"string"}
  ]
});
