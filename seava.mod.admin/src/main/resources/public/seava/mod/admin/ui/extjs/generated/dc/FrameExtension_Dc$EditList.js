/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.FrameExtension_Dc$EditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"frame", dataIndex:"frame", width:300, editor: {xtype: "textfield"}})
      .addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", width:80, editor: {xtype: "numberfield"}})
      .addTextColumn({ name:"fileLocation", dataIndex:"fileLocation", width:250, editor: {xtype: "textfield"}})
      .addBooleanColumn({ name:"relativePath", dataIndex:"relativePath"})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addTextColumn({ name:"notes", dataIndex:"notes", width:200, editor: {xtype: "textfield"}})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_FrameExtension_Dc$EditList"
});
