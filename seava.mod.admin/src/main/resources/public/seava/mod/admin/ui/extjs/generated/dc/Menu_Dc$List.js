/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Menu_Dc$List", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addNumberColumn({name:"sequenceNo", dataIndex:"sequenceNo", width:70})
      .addTextColumn({ name:"name", dataIndex:"name", width:100})
      .addTextColumn({ name:"title", dataIndex:"title"})
      .addTextColumn({ name:"tag", dataIndex:"tag"})
      .addTextColumn({ name:"description", dataIndex:"description"})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.admin_Menu_Dc$List"
});
