/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.ParamValue_Ds", {
  extend: 'Ext.data.Model',
  
  statics: {
    ALIAS: "ad_ParamValue_Ds"
  },
  
  validators: {
    sysParam: [{ type: 'presence'}],
    validFrom: [{ type: 'presence'}],
    validTo: [{ type: 'presence'}]
  },
  
  fields: [
    {name:"sysParam", type:"string"},
    {name:"value", type:"string"},
    {name:"validFrom", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"validTo", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"id", type:"string"},
    {name:"clientId", type:"string"},
    {name:"notes", type:"string"},
    {name:"createdAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"modifiedAt", type:"date", dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"createdBy", type:"string"},
    {name:"modifiedBy", type:"string"},
    {name:"version", type:"int", useNull:true},
    {name:"refid", type:"string"},
    {name:"entityAlias", type:"string"},
    {name:"entityFqn", type:"string"}
  ]
});
