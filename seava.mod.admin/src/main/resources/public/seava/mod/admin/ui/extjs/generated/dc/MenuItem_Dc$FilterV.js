/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.MenuItem_Dc$FilterV", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextField({ name:"title", bind: "{d.title}", maxLength: 255})
      .addLov({name:"name", bind: "{d.name}", xtype:"seava.mod.admin.ui.extjs.generated.lov.MenuItems_Lov",
        retFieldMapping: [{lovField:"id", dsField: "id"} ]})
      .addTextField({ name:"frame", bind: "{d.frame}", maxLength: 255})
      .addLov({name:"menuItem", bind: "{d.menuItem}", xtype:"seava.mod.admin.ui.extjs.generated.lov.MenuItems_Lov",
        retFieldMapping: [{lovField:"id", dsField: "menuItemId"} ],
        filterFieldMapping: [{lovParam:"foldersOnly", value: "true"} ]})
      .addLov({name:"menu", bind: "{d.menu}", xtype:"seava.mod.admin.ui.extjs.generated.lov.Menus_Lov",
        retFieldMapping: [{lovField:"id", dsField: "menuId"} ]})
      .addBooleanField({ name:"separatorBefore", bind: "{d.separatorBefore}"})
      .addBooleanField({ name:"separatorAfter", bind: "{d.separatorAfter}"})
      .addBooleanField({ name:"active", bind: "{d.active}"})
      .addBooleanField({ name:"foldersOnly", paramIndex:"foldersOnly", bind: "{p.foldersOnly}"})
      .addBooleanField({ name:"framesOnly", paramIndex:"framesOnly", bind: "{p.framesOnly}"})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}, defaults: { labelWidth:60}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["name", "title", "menu", "menuItem", "frame", "separatorBefore", "separatorAfter", "foldersOnly", "framesOnly", "active" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_MenuItem_Dc$FilterV"
});
