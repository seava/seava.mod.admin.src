/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */

Ext.define("seava.mod.admin.ui.extjs.generated.ds.MyParam_DsParam", {
  extend: 'Ext.data.Model',
  
  fields: [
    {name:"validFrom", type:"date", forFilter:true, dateFormat:Main.MODEL_DATE_FORMAT},
    {name:"validTo", type:"date", forFilter:true, dateFormat:Main.MODEL_DATE_FORMAT}
  ]
});
