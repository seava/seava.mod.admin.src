Ext.define("seava.mod.admin.i18n.extjs.en.generated.ds.Role_DsParam", {
  withPrivilegeId__lbl: "With Privilege Id",
  withPrivilege__lbl: "With Privilege",
  withUserId__lbl: "With User Id",
  withUser__lbl: "With User"
});
