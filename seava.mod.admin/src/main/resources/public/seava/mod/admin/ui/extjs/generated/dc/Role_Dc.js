/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Role_Dc", {
  extend: "seava.lib.e4e.js.dc.AbstractDcController",
  
  paramModel: "seava.mod.admin.ui.extjs.generated.ds.Role_DsParam",
  recordModel: "seava.mod.admin.ui.extjs.generated.ds.Role_Ds"
});
