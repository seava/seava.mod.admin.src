/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.AttachmentType_Dc$EditList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"name", dataIndex:"name", width:180, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"description", dataIndex:"description", width:250, editor: {xtype: "textfield"}})
      .addComboColumn({ name:"category", dataIndex:"category", width:120, editor: {xtype: "combo", store:["link","upload"]}})
      .addTextColumn({ name:"uploadPath", dataIndex:"uploadPath", width:150, editor: {xtype: "textfield"}})
      .addTextColumn({ name:"baseUrl", dataIndex:"baseUrl", width:150, editor: {xtype: "textfield"}})
      .addBooleanColumn({ name:"active", dataIndex:"active"})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvEditableGrid",
  alias: "widget.admin_AttachmentType_Dc$EditList"
});
