/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.frame.ParamValues_Ui", {
  
  /**
   * Data-controls definition
   */
  _defineDcs_: function() {
    this._getBuilder_()
      .addDc("val", Ext.create(seava.mod.admin.ui.extjs.generated.dc.ParamValue_Dc,{multiEdit: true,  autoLoad:true}))
      ;
  },
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addButton({name:"btnPublish", disabled:false, handler: this.onBtnPublish, scope:this})
      .addButton({name:"btnShowParamsFrame", disabled:false, handler: this.onBtnShowParamsFrame, scope:this})
      .addDcView("val", {name:"valFilter", _hasTitle_:true, width:230,  collapsed:true, collapsible:true, minWidth:220, maxWidth:300, xtype:"admin_ParamValue_Dc$FilterV"})
      .addDcView("val", {name:"valList", xtype:"admin_ParamValue_Dc$EditList"})
      .addPanel({name:"main", layout:{ type: "border", regionWeights: {west:0,center:0}}, defaults:{split:true}})
      ;
  },
  
  /**
   * Combine the components
   */
  _linkElements_: function() {
    this._getBuilder_()
      .addChildrenTo("main", ["valFilter", "valList"],["west", "center"])
      .addToolbarTo("main", "tlbValList")
      ;
  },
  
  /**
   * Create toolbars
   */
  _defineToolbars_: function() {
    this._getBuilder_()
      .beginToolbar("tlbValList", {dc: "val"})
        .addTitle().addSeparator().addSeparator()
        .addQuery().addSave().addNew().addCopy().addDelete().addCancel()
        .addSeparator().addSeparator()
        .addButtons([this._elems_.get("btnPublish") ,this._elems_.get("btnShowParamsFrame") ])
        .addReports()
      .end();
  },
  
  /**
   * On-Click handler for button btnPublish
   */
  onBtnPublish: function() {
    var o={
      name:"publishValues",
      modal:true
    };
    this._getDc_("val").doRpcFilter(o);
  },
  
  /**
   * On-Click handler for button btnShowParamsFrame
   */
  onBtnShowParamsFrame: function() {
    getApplication().showFrame("seava.mod.admin.ui.extjs.generated.frame.MyParams_Ui");
  },
  
  extend: "seava.lib.e4e.js.frame.AbstractFrame",
  alias: "widget.ParamValues_Ui"
});
