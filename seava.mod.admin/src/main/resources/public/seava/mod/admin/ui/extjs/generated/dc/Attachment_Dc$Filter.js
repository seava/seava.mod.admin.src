/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.Attachment_Dc$Filter", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextField({ name:"name", bind: "{d.name}", maxLength: 255})
      .addLov({name:"type", bind: "{d.type}", xtype:"seava.mod.admin.ui.extjs.generated.lov.AttachmentTypes_Lov",
        retFieldMapping: [{lovField:"id", dsField: "typeId"} ]})
      .addTextField({ name:"targetType", bind: "{d.targetType}", maxLength: 255})
      
      /* =========== containers =========== */
      
      .addPanel({ name:"main", autoScroll:true, layout: {type:"hbox", align:"begin", pack:"start"}})
      .addPanel({ name:"col1", layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}})
      .addPanel({ name:"col2", width:250, layout: {type:"anchor"}, childrenDefaults: {anchor: "-5"}})
  },
  
  /**
    * Combine the components
    */     
  _linkElements_: function() {
     this._getBuilder_()
       .addChildrenTo("main", ["col1", "col2" ])
       .addChildrenTo("col1", ["name", "type" ])
       .addChildrenTo("col2", ["targetType" ])
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvFilterForm",
  alias: "widget.admin_Attachment_Dc$Filter"
});
