/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.ReportRt_Dc$List", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"name", dataIndex:"name", width:300})
      .addTextColumn({ name:"code", dataIndex:"code", width:150})
      .addTextColumn({ name:"reportServer", dataIndex:"reportServer", width:100, hidden:true})
      .addTextColumn({ name:"reportServerId", dataIndex:"reportServerId", hidden:true})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.admin_ReportRt_Dc$List"
});
