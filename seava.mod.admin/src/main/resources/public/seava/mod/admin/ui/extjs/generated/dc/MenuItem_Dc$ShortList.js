/**
 * Copyright: 2013 Nan21 Electronics SRL. All rights reserved.
 * Use is subject to license terms.
 */
Ext.define("seava.mod.admin.ui.extjs.generated.dc.MenuItem_Dc$ShortList", {
  
  /**
   * Components definition
   */
  _defineElements_: function() {
    this._getBuilder_()
      .addTextColumn({ name:"name", dataIndex:"name", width:150})
      .addTextColumn({ name:"title", dataIndex:"title", width:150})
      .addTextColumn({ name:"menu", dataIndex:"menu", width:80})
      .addTextColumn({ name:"menuItem", dataIndex:"menuItem", width:120})
      .addTextColumn({ name:"frame", dataIndex:"frame", width:350})
      .addBooleanColumn({ name:"active", dataIndex:"active", width:70})
      .addDefaults();
  },
  
  extend: "seava.lib.e4e.js.dc.view.AbstractDcvGrid",
  alias: "widget.admin_MenuItem_Dc$ShortList"
});
