
  /* ==================== AD_PARAMVAL ==================== */
  
  /* Table */
  
  create table if not exists AD_PARAMVAL (
    SYSPARAM varchar(64) not null,
    VALUE varchar(4000), 
    VALIDFROM date not null,
    VALIDTO date not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_PARAMVAL add primary key(ID);
