
  /* ==================== AD_USR_USRGRP ==================== */
  
  /* Table */
  
  create table if not exists AD_USR_USRGRP (
    USERS_ID varchar(64) not null,
    GROUP_ID varchar(64) not null
  );
  
  /* Constraints */
  
  alter table AD_USR_USRGRP add primary key(USERS_ID,GROUP_ID);
