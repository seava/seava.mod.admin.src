alter table AD_ROLE_ACL add constraint FK_AD_ROLE_ACL_1 foreign key (ROLES_ID)
references AD_ROLE(ID);
alter table AD_ROLE_ACL add constraint FK_AD_ROLE_ACL_2 foreign key (ACL_ID)
references AD_ACL(ID);
