alter table AD_USR_USRGRP add constraint FK_AD_USR_USRGRP_1 foreign key (USERS_ID)
references AD_USR(ID);
alter table AD_USR_USRGRP add constraint FK_AD_USR_USRGRP_2 foreign key (GROUP_ID)
references AD_USRGRP(ID);
