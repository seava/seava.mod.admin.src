
  /* ==================== AD_RPT_PRM ==================== */
  
  /* Table */
  
  create table if not exists AD_RPT_PRM (
    REPORT_ID varchar(64) not null,
    TITLE varchar(255) not null,
    SEQUENCENO int(4) not null,
    MANDATORY int(1) not null,
    NOEDIT int(1) not null,
    DATATYPE varchar(32) not null,
    DEFAULTVALUE varchar(4000), 
    LISTOFVALUES varchar(4000), 
    NAME varchar(255) not null,
    DESCRIPTION varchar(4000), 
    ACTIVE int(1) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_RPT_PRM add primary key(ID);
  alter table AD_RPT_PRM add constraint UK_AD_RPT_PRM_1 unique (CLIENTID,REPORT_ID,NAME);
