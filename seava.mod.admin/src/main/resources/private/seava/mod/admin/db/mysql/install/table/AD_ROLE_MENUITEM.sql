
  /* ==================== AD_ROLE_MENUITEM ==================== */
  
  /* Table */
  
  create table if not exists AD_ROLE_MENUITEM (
    ROLE_ID varchar(64) not null,
    MENUITEM_ID varchar(64) not null
  );
  
  /* Constraints */
  
  alter table AD_ROLE_MENUITEM add primary key(ROLE_ID,MENUITEM_ID);
