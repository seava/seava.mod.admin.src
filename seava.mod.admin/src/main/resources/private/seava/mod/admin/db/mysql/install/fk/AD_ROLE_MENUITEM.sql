alter table AD_ROLE_MENUITEM add constraint FK_AD_ROLE_MENUITEM_1 foreign key (ROLE_ID)
references AD_ROLE(ID);
alter table AD_ROLE_MENUITEM add constraint FK_AD_ROLE_MENUITEM_2 foreign key (MENUITEM_ID)
references AD_MENU_ITEM(ID);
