alter table AD_RPT_DS_PARAM add constraint FK_AD_RPT_DS_PARAM_1 foreign key (DSREPORT_ID)
references AD_RPT_DS(ID);
alter table AD_RPT_DS_PARAM add constraint FK_AD_RPT_DS_PARAM_2 foreign key (REPORTPARAM_ID)
references AD_RPT_PRM(ID);
