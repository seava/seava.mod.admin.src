
  /* ==================== AD_ACL_DS ==================== */
  
  /* Table */
  
  create table if not exists AD_ACL_DS (
    ACCESSCONTROL_ID varchar(64) not null,
    DSNAME varchar(255) not null,
    QUERYALLOWED int(1) not null,
    INSERTALLOWED int(1) not null,
    UPDATEALLOWED int(1) not null,
    DELETEALLOWED int(1) not null,
    IMPORTALLOWED int(1) not null,
    EXPORTALLOWED int(1) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_ACL_DS add primary key(ID);
  alter table AD_ACL_DS add constraint UK_AD_ACL_DS_1 unique (CLIENTID,ACCESSCONTROL_ID,DSNAME);
