
  /* ==================== AD_VIEW_STATE ==================== */
  
  /* Table */
  
  create table if not exists AD_VIEW_STATE (
    CMP varchar(255) not null,
    CMPTYPE varchar(1) not null,
    VALUE varchar(4000), 
    NAME varchar(255) not null,
    DESCRIPTION varchar(4000), 
    ACTIVE int(1) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_VIEW_STATE add primary key(ID);
  alter table AD_VIEW_STATE add constraint UK_AD_VIEW_STATE_1 unique (CLIENTID,NAME);
