
  /* ==================== AD_RPT_DS_PARAM ==================== */
  
  /* Table */
  
  create table if not exists AD_RPT_DS_PARAM (
    DSREPORT_ID varchar(64) not null,
    REPORTPARAM_ID varchar(64) not null,
    DSFIELD varchar(255), 
    STATICVALUE varchar(4000), 
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_RPT_DS_PARAM add primary key(ID);
