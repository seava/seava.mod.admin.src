
  /* ==================== AD_USR ==================== */
  
  /* Table */
  
  create table if not exists AD_USR (
    DATEFORMAT_ID varchar(64), 
    LOGINNAME varchar(255) not null,
    PASSWORD varchar(255) not null,
    EMAIL varchar(128), 
    LOCKED int(1) not null,
    DECIMALSEPARATOR varchar(1), 
    THOUSANDSEPARATOR varchar(1), 
    CODE varchar(64) not null,
    NAME varchar(255) not null,
    DESCRIPTION varchar(4000), 
    ACTIVE int(1) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_USR add primary key(ID);
  alter table AD_USR add constraint UK_AD_USR_3 unique (CLIENTID,LOGINNAME);
  alter table AD_USR add constraint UK_AD_USR_2 unique (CLIENTID,CODE);
  alter table AD_USR add constraint UK_AD_USR_1 unique (CLIENTID,NAME);
