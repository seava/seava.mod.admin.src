
  /* ==================== AD_MENU_ITEM ==================== */
  
  /* Table */
  
  create table if not exists AD_MENU_ITEM (
    MENUITEM_ID varchar(64), 
    MENU_ID varchar(64), 
    SEQUENCENO int(4), 
    TITLE varchar(255) not null,
    FRAME varchar(255), 
    ICONURL varchar(255), 
    SEPARATORBEFORE int(1), 
    SEPARATORAFTER int(1), 
    NAME varchar(255) not null,
    DESCRIPTION varchar(4000), 
    ACTIVE int(1) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_MENU_ITEM add primary key(ID);
  alter table AD_MENU_ITEM add constraint UK_AD_MENU_ITEM_1 unique (CLIENTID,NAME);
