
  /* ==================== AD_ROLE_MENU ==================== */
  
  /* Table */
  
  create table if not exists AD_ROLE_MENU (
    ROLE_ID varchar(64) not null,
    MENU_ID varchar(64) not null
  );
  
  /* Constraints */
  
  alter table AD_ROLE_MENU add primary key(ROLE_ID,MENU_ID);
