
  /* ==================== AD_USR_ROLE ==================== */
  
  /* Table */
  
  create table if not exists AD_USR_ROLE (
    USER_ID varchar(64) not null,
    ROLE_ID varchar(64) not null
  );
  
  /* Constraints */
  
  alter table AD_USR_ROLE add primary key(USER_ID,ROLE_ID);
