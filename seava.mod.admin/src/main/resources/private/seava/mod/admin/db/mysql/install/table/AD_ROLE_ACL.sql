
  /* ==================== AD_ROLE_ACL ==================== */
  
  /* Table */
  
  create table if not exists AD_ROLE_ACL (
    ROLES_ID varchar(64) not null,
    ACL_ID varchar(64) not null
  );
  
  /* Constraints */
  
  alter table AD_ROLE_ACL add primary key(ROLES_ID,ACL_ID);
