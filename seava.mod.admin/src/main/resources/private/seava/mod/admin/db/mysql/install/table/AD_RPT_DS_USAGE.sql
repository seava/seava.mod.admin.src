
  /* ==================== AD_RPT_DS_USAGE ==================== */
  
  /* Table */
  
  create table if not exists AD_RPT_DS_USAGE (
    DSREPORT_ID varchar(64) not null,
    FRAMENAME varchar(255), 
    TOOLBARKEY varchar(4000), 
    DCKEY varchar(4000), 
    SEQUENCENO int(4), 
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_RPT_DS_USAGE add primary key(ID);
