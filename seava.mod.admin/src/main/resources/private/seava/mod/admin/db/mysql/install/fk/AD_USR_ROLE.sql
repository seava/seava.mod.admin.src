alter table AD_USR_ROLE add constraint FK_AD_USR_ROLE_1 foreign key (USER_ID)
references AD_USR(ID);
alter table AD_USR_ROLE add constraint FK_AD_USR_ROLE_2 foreign key (ROLE_ID)
references AD_ROLE(ID);
