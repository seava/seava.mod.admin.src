
  /* ==================== AD_FRAME_EXT ==================== */
  
  /* Table */
  
  create table if not exists AD_FRAME_EXT (
    FRAME varchar(255) not null,
    SEQUENCENO int(4) not null,
    FILELOCATION varchar(255) not null,
    RELATIVEPATH int(1) not null,
    ACTIVE int(1) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_FRAME_EXT add primary key(ID);
  alter table AD_FRAME_EXT add constraint UK_AD_FRAME_EXT_1 unique (CLIENTID,FRAME,FILELOCATION);
