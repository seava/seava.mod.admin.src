
  /* ==================== AD_ATCH ==================== */
  
  /* Table */
  
  create table if not exists AD_ATCH (
    TYPE_ID varchar(64) not null,
    NAME varchar(255) not null,
    FILENAME varchar(255), 
    LOCATION varchar(4000), 
    CONTENTTYPE varchar(32), 
    TARGETREFID varchar(64) not null,
    TARGETALIAS varchar(64) not null,
    TARGETTYPE varchar(255), 
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_ATCH add primary key(ID);
