alter table AD_ROLE_MENU add constraint FK_AD_ROLE_MENU_1 foreign key (ROLE_ID)
references AD_ROLE(ID);
alter table AD_ROLE_MENU add constraint FK_AD_ROLE_MENU_2 foreign key (MENU_ID)
references AD_MENU(ID);
