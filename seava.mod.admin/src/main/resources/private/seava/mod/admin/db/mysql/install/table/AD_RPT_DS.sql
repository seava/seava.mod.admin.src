
  /* ==================== AD_RPT_DS ==================== */
  
  /* Table */
  
  create table if not exists AD_RPT_DS (
    REPORT_ID varchar(64) not null,
    DATASOURCE varchar(255) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_RPT_DS add primary key(ID);
  alter table AD_RPT_DS add constraint UK_AD_RPT_DS_1 unique (CLIENTID,REPORT_ID,DATASOURCE);
