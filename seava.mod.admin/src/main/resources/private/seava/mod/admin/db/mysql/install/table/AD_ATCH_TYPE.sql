
  /* ==================== AD_ATCH_TYPE ==================== */
  
  /* Table */
  
  create table if not exists AD_ATCH_TYPE (
    CATEGORY varchar(8) not null,
    UPLOADPATH varchar(4000), 
    BASEURL varchar(255) not null,
    NAME varchar(255) not null,
    DESCRIPTION varchar(4000), 
    ACTIVE int(1) not null,
    CREATED_AT datetime not null,
    CREATED_BY varchar(32) not null,
    MODIFIED_AT datetime not null,
    MODIFIED_BY varchar(32) not null,
    NOTES varchar(4000), 
    CLIENTID varchar(64) not null,
    ID varchar(64) not null,
    REFID varchar(64) not null,
    VERSION bigint(10) not null
  );
  
  /* Constraints */
  
  alter table AD_ATCH_TYPE add primary key(ID);
  alter table AD_ATCH_TYPE add constraint UK_AD_ATCH_TYPE_1 unique (CLIENTID,NAME);
